#ifndef BLACKHACK_MAIN_H
#define BLACKHACK_MAIN_H

#include <stdint.h>

#include "defines.h"

blackhack_EXPORT int diverted_main(int argc, char* argv[]);

#endif // BLACKHACK_MAIN_H
