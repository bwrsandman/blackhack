#ifndef BLACKHACK_LAUNCHER_H_
#define BLACKHACK_LAUNCHER_H_

#include <stdbool.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdint.h>

static const GUID blackhack_guid = {
  /* 11b79fb0-96c1-4262-aa01-71f94cd8a852 */
  0x11b79fb0,
  0x96c1,
  0x4262,
  { 0x71, 0xf9, 0x4c, 0xd8, 0xa8, 0x52 },
};

#define RUNBLACK_MAKE_VERSION(platform, major, minor, md5_byte) \
  ((platform) | (major) << 0x8u | (minor) << 0x10u | (md5_byte) << 0x18u)

static const char* runblack_version_win_1_41_17_sum = "\x17\x4b\x1a\x64\xe7\x4b\x23\x21\xf3\xc3\x8c\xcc\x8a\x51\x1e\x78";
#define runblack_version_win_1_41_17 RUNBLACK_MAKE_VERSION(0u, 1u, 41u, 0x17u)

struct blackhack_launcher_payload_t {
  bool vs_jit_attach;
  uint32_t log_level;
  uint32_t version;
};

#endif // BLACKHACK_LAUNCHER_H_
