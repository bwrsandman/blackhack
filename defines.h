#ifndef BLACKHACK_DEFINES_H
#define BLACKHACK_DEFINES_H

#ifdef __GNUC__
#define PACK(__Declaration__) __Declaration__ __attribute__((__packed__))
#endif

#ifdef _MSC_VER
#define PACK(__Declaration__)                                                  \
  __pragma(pack(push, 1)) __Declaration__ __pragma(pack(pop))
#endif

#if defined(_WIN32)
#if defined(blackhack_EXPORTS)
#define blackhack_EXPORT __declspec(dllexport)
#else
#define blackhack_EXPORT __declspec(dllimport)
#endif /* blackhack_EXPORTS */
#else  /* defined (_WIN32) */
#define blackhack_EXPORT
#endif

#endif // BLACKHACK_DEFINES_H
