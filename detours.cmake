include(FetchContent)
FetchContent_Declare(
  detours
    GIT_REPOSITORY https://github.com/microsoft/Detours.git
    GIT_TAG master
)
FetchContent_GetProperties(detours)
if(NOT detours_POPULATED)
    FetchContent_Populate(detours)
endif()

option(DETOUR_DEBUG "Compile with debug tracing messages" OFF)
option(DETOUR_SAMPLES "Include detour samples in build" ON)

set(DETOURS_SRC
    detours.cmake
    
    ${detours_SOURCE_DIR}/src/detours.h
    ${detours_SOURCE_DIR}/src/detver.h

    ${detours_SOURCE_DIR}/src/creatwth.cpp
    ${detours_SOURCE_DIR}/src/disasm.cpp
    ${detours_SOURCE_DIR}/src/disolarm64.cpp
    ${detours_SOURCE_DIR}/src/disolx64.cpp
    ${detours_SOURCE_DIR}/src/image.cpp
    ${detours_SOURCE_DIR}/src/modules.cpp
    ${detours_SOURCE_DIR}/src/detours.cpp
    ${detours_SOURCE_DIR}/src/disolarm.cpp
    ${detours_SOURCE_DIR}/src/disolia64.cpp
    ${detours_SOURCE_DIR}/src/disolx86.cpp
    # ${detours_SOURCE_DIR}/src/uimports.cpp
)
add_library(Detours STATIC ${DETOURS_SRC})
target_include_directories(Detours INTERFACE ${detours_SOURCE_DIR}/src)
if (CMAKE_GENERATOR_PLATFORM STREQUAL "Win32")
    target_compile_definitions(Detours PUBLIC _X86_)
elseif (CMAKE_GENERATOR_PLATFORM STREQUAL "x64")
    target_compile_definitions(Detours PUBLIC _AMD64_)
elseif (VCPKG_TARGET_TRIPLET STREQUAL "x86-windows")
    target_compile_definitions(Detours PUBLIC _X86_)
elseif (VCPKG_TARGET_TRIPLET STREQUAL "x64-windows")
    target_compile_definitions(Detours PUBLIC _AMD64_)
else ()
    message(WARNING "Architecture ${CMAKE_GENERATOR_PLATFORM} not covered by cmake file")
endif()
if (DETOUR_DEBUG)
    target_compile_definitions(Detours PRIVATE DETOUR_DEBUG=1)
endif()
set_target_properties(Detours PROPERTIES FOLDER detours)

set(DETOURS_SAMPLES
    comeasy
    commem
    cping
    disas
    dtest
    dumpe
    dumpi
    dynamic_alloc
    echo
    einst
    excep
    findfunc
    impmunge
    member
    opengl
    region
    setdll
    simple
    slept
    syelog
    talloc
    traceapi
    tracebld
    tracelnk
    tracemem
    tracereg
    traceser
    tracessl
    tracetcp
    tryman
    withdll
)

if (DETOUR_SAMPLES)
    foreach(SAMPLE ${DETOURS_SAMPLES})
        file(GLOB SAMPLE_SRC ${detours_SOURCE_DIR}/samples/${SAMPLE}/*.h ${detours_SOURCE_DIR}/samples/${SAMPLE}/*.cpp)
        add_executable(${SAMPLE} ${SAMPLE_SRC})
        target_link_libraries(${SAMPLE} PRIVATE Detours)
        set_target_properties(${SAMPLE} PROPERTIES FOLDER detours/samples)
    endforeach()
endif()
