#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h>

#include <detours.h>
#include <logger.h>

#include "function_table.h"
#include "launcher.h"
#include "main.h"

static int (*original_main)(int, char**) = NULL;

bool launchDebugger(void) {
  // Get System directory, typically c:\windows\system32
  char system_dir[MAX_PATH] = { 0 };
  uint32_t num_chars = GetSystemDirectoryA(system_dir, sizeof(system_dir));
  if (num_chars == 0 || num_chars > MAX_PATH - 2) {
    printf("Unable to get system directory, numchars=%d\n", num_chars);
    return false; // failed to get system directory
  }
  system_dir[num_chars] = '\0';

  // Get process ID and create the command line
  DWORD pid = GetCurrentProcessId();
  char cmd_line[MAX_PATH] = { 0 };
  sprintf_s(cmd_line, sizeof(cmd_line), "%s\\vsjitdebugger.exe -p %d",
      system_dir, pid);

  // Start debugger process
  STARTUPINFOA si = { 0 };
  si.cb = sizeof(si);
  PROCESS_INFORMATION pi = { 0 };

  printf("Attaching JIT debugger\n");
  printf("CreateProcess: %s\n", cmd_line);
  if (!CreateProcessA(
          NULL, cmd_line, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
    return false;
  }

  // Close debugger process handles to eliminate resource leak
  CloseHandle(pi.hThread);
  CloseHandle(pi.hProcess);

  // Wait for the debugger to attach
  while (!IsDebuggerPresent()) {
    Sleep(100);
  }

  return true;
}

bool detour_entry_point(void) {
  if (((uint8_t*)original_main)[0] == 0xCC) {
    MessageBoxA(NULL,
        "First byte of entry is 0xCC. A debugger is inserting interrupts.",
        "main", 0);
    return false;
  }

  DetourAttach((void*)&original_main, diverted_main);
  return true;
}

bool detour_function_table(uint32_t version) {
  uint32_t count = 0;
  struct function_table_t* table = NULL;
  switch (version) {
  case runblack_version_win_1_41_17:
    table = runblack_win_1_41_17_function_table;
    count = _countof(runblack_win_1_41_17_function_table);
    break;
  default:
    LOG_ERROR(
        "Unsupported version: %u.%u (platform %u, md5sum starts with %02x)",
        (version >> 0x8u) & 0xFFu, (version >> 0x10u) & 0xFFu, version & 0xFFu,
        version >> 0x18u);
    return false;
  }

  for (uint32_t i = 0; i < count; ++i) {
    DetourAttach(&table[i].original_address, table[i].detour_address);
    LOG_DEBUG("blackhack" DETOURS_STRINGIFY(DETOURS_BITS) ".dll: Detoured %s",
        table[i].name);
  }
  return true;
}

// This will run after app is unsuspended but before app can do anything
bool APIENTRY DllMain(
    HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
  if (ul_reason_for_call == DLL_PROCESS_ATTACH) {

    // Find entry point of target exe by looking at the detour restore payload
    DWORD cbData;
    DETOUR_EXE_RESTORE* der = (DETOUR_EXE_RESTORE*)DetourFindPayloadEx(
        &DETOUR_EXE_RESTORE_GUID, &cbData);
    if (cbData <= 0 || der == NULL) {
      MessageBoxA(NULL, "Error finding target module",
          "blackhack" DETOURS_STRINGIFY(DETOURS_BITS) ".dll", 0);
      return false;
    }

    // Find launcher payload
    cbData;
    struct blackhack_launcher_payload_t* options =
        (struct blackhack_launcher_payload_t*)DetourFindPayloadEx(
            &blackhack_guid, &cbData);
    if (cbData <= 0 || options == NULL) {
      MessageBoxA(NULL, "Error launcher payload",
          "blackhack" DETOURS_STRINGIFY(DETOURS_BITS) ".dll", 0);
      return false;
    }

    logger_initConsoleLogger(NULL);
    logger_initFileLogger(
        "blackhack" DETOURS_STRINGIFY(DETOURS_BITS) ".log", 0, 10);
    logger_setLevel((LogLevel)options->log_level);
    LOG_INFO("blackhack" DETOURS_STRINGIFY(DETOURS_BITS) ".dll attached.");

    // Attach to calling console
    bool attached = AttachConsole(ATTACH_PARENT_PROCESS);
    if (attached) {
      FILE* f_stdout;
      FILE* f_stdin;
      if (freopen_s(&f_stdout, "CONOUT$", "w", stdout) != 0 ||
          freopen_s(&f_stdin, "CONIN$", "r", stdin)) {
        MessageBoxA(
            NULL, "Failed to get streams from parent console...", "main", 0);
        return false;
      }
      LOG_INFO("Attached to parent console...");
    }
    else {
      MessageBoxA(NULL, "Failed to attach to parent console...", "main", 0);
      return false;
    }

    if (options->vs_jit_attach) {
      if (!launchDebugger()) {
        MessageBoxA(NULL, "Unable to launch JIT debugger.\nAttach debugger now",
            "main", 0);
      }
    }

#if BLACK_HACK_DETOUR_ENTRY_POINT
    original_main = DetourGetEntryPoint((HMODULE)der->pidh);
    if (original_main == NULL) {
      LOG_FATAL("Error finding original entry point of target.");
      return false;
    }
#endif // BLACK_HACK_DETOUR_ENTRY_POINT

    DetourRestoreAfterWith();
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());

#if BLACK_HACK_DETOUR_ENTRY_POINT
    if (!detour_entry_point()) {
      LOG_FATAL("Error detouring main()");
      return false;
    }
    else {
      LOG_INFO("Detoured main().");
    }
#endif // BLACK_HACK_DETOUR_ENTRY_POINT

    if (!detour_function_table(options->version)) {
      LOG_FATAL("Error detouring function table().");
      return false;
    }
    else {
      LOG_INFO("Detoured function table().");
    }

    long error = DetourTransactionCommit();
    if (error == NO_ERROR) {
      LOG_INFO("Detour committed.");
    }
    else {
      LOG_FATAL("Error committing detour: %d", error);
      return false;
    }
  }
  else if (ul_reason_for_call == DLL_PROCESS_DETACH) {

    LOG_INFO("blackhack" DETOURS_STRINGIFY(DETOURS_BITS) ".dll detached.");
    logger_flush();

    if (original_main != NULL) {
      DetourTransactionBegin();
      DetourUpdateThread(GetCurrentThread());
      DetourDetach((void*)&original_main, diverted_main);
      LOG_INFO("Removed detour main() (result=%d)", DetourTransactionCommit());
    }
  }
  return true;
}
