# blackhack

Launcher for and decompilation of runblack.exe

## Contents
* Detouring launcher for runblack.exe
* Almost full manually decompiled implementation of the SetupControl classes representing the UI widgets of runblack.exe

## Modifications from vanilla
Modify the set fullscreen function to always be windowed

## Supported versions

Since the detours require apriori knowledge of function and data addresses, addresses to reference and replace, these addresses must be entered in the following tables:
* `runblack_*_globals_table` in `globals.c`
* `runblack_*_function_table` in `function_table.h`


| Supported Version    | md5sum                           |
| -------------------- | -------------------------------- |
| 1.41 with nocd patch | 174b1a64e74b2321f3c38ccc8a511e78 |

Adding a new version is a long but straightforward process of adding a new table for global and function addresses.
First, for validation, the version description and md5 sum must be added to launcher.h.
Additionally, a case must be added in `detour_function_table()` in `dllmain.c` and
in `global()` in `globals/globals.c`.
