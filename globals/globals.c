#include "globals.h"

#include <assert.h>

#include <detours.h>

#include "../launcher.h"

const struct globals_t runblack_win_1_41_17_globals_table = {
    .SetupThingDraw = (struct SetupThingDraw_t*)0x009c8078,
    .DrawAndClip_00c2ab00 = (struct DrawAndClip_t*)0x00c2ab00,
    .DAT_00c371d4 = (float*)0x00c371d4,
    .g_delta_time__8LH3DTech = (uint32_t*) 0x00c38134,
    .DAT_00c38714 = (uint32_t*)0x00c38714,
    .SetupThing = (void*)0x00c4cc80,
    .pp_game = (void*)0x00d0195c,
    .STRUCT_00d17ca8 = (void*)0x00d17ca8,
    .DAT_00e839e0 = (void*)0x00e839e0,
    .LHSys = (void*)0x00e85040,
    .DAT_00e8c0f4 = (void*)0x00e8c0f4,
    .UINT_ARRAY_00ec81f0 = (void*)0x00ec81f0,
    .UINT_ARRAY_00eca1f0 = (void*)0x00eca1f0,
    .DAT_00eca614 = (int*)0x00eca614,
    .PTR_00eca618 = (void*)0x00eca618,
    .Direct3DDevice7 = (IDirect3DDevice7 **)0x00eca638,
    .DAT_00eca64c = (void*)0x00eca64c,
    .GatheringText__gamefont = (void*)0x00eccd08,
    .DAT_00ed92dc = (void*)0x00ed92dc,
    .PTR_00edc368 = (void*)0x00edc368,
    .LH3DTexture = {
        .first = (void*)0x00edd460,
    },
    .PTR_00edd494 = (void*)0x00edd494,
};

const struct globals_t* globals() {
  static const struct globals_t* current_version = NULL;

  if (current_version == NULL) {
    // Find launcher payload
    DWORD cbData;
    struct blackhack_launcher_payload_t* options = (struct blackhack_launcher_payload_t*)DetourFindPayloadEx(
        &blackhack_guid, &cbData);
    assert(cbData > 0 && options != NULL);
    switch (options->version) {
    case runblack_version_win_1_41_17:
      current_version = &runblack_win_1_41_17_globals_table;
      break;
    default:
      assert(false);
    }

  }

  return current_version;
}
