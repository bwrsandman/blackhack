#ifndef BLACKHACK_GLOBALS_H
#define BLACKHACK_GLOBALS_H

#include <stdbool.h>
#include <stdint.h>

#include <d3d.h>

struct SetupThingDraw_t {
  uint8_t alpha;           // 0x009c8078
  uint32_t field_0x4;      // 0x009c807c
  D3DTLVERTEX vertices[4]; // 0x009c8080
  uint16_t indices[6];     // 0x009c8100
};

struct GlobalLH3DTextures {
  struct LH3DTexture** const first;
};

struct Node_00d17ca8_t {
  uint32_t field_0x0;
  uint32_t field_0x4;
  const wchar_t* str;
};

struct STRUCT_00d17ca8_t {
  struct Node_00d17ca8_t* array;
  uint32_t count;
};

struct SetRenderModeData {
  void (__fastcall* callback)(struct LH3DMaterial * material, int);
  void* field_0x4;
};

struct DrawAndClip_t {
  float clip_x_max; // 0x00c2ab00
  float clip_y_max; // 0x00c2ab04
};

struct globals_t {
  // 008a9000-009c5fff: Read-only data region
  // 009c6000-00fbfdff: Read-write region
  struct SetupThingDraw_t* SetupThingDraw; // 0x009c8078
  struct DrawAndClip_t* DrawAndClip_00c2ab00;
  float* DAT_00c371d4;
  uint32_t* g_delta_time__8LH3DTech;
  uint32_t* DAT_00c38714;
  struct SetupThing * SetupThing; // 0x00c4cc80
  struct GGame** pp_game; // 0x00d0195c
  struct STRUCT_00d17ca8_t* STRUCT_00d17ca8;
  float* DAT_00e839e0;
  struct LHSys* LHSys; // 0x00e85040
  void** DAT_00e8c0f4;
  uint32_t (*UINT_ARRAY_00ec81f0)[0x800];
  uint32_t (*UINT_ARRAY_00eca1f0)[D3DRENDERSTATE_CLIPPLANEENABLE + 1];
  int* DAT_00eca614;
  struct SetRenderModeData **PTR_00eca618;
  IDirect3DDevice7** Direct3DDevice7;    // 0x00eca638
  struct LH3DMaterial ** DAT_00eca64c;
  struct GatheringText** GatheringText__gamefont; // 0x00eccd08
  struct LH3DMaterial ** DAT_00ed92dc;
  struct LH3DMaterial ** PTR_00edc368;
  struct GlobalLH3DTextures LH3DTexture; // 0x00edd460
  struct LH3DMaterial ** PTR_00edd494;
};

const struct globals_t* globals();

#endif // BLACKHACK_GLOBALS_H
