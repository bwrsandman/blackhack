#ifndef BLACKHACK_FUNCTION_TABLE_H
#define BLACKHACK_FUNCTION_TABLE_H

#include <stdint.h>

#include <Game.h>
#include <LH3DRender.h>
#include <LHScreen.h>
#include <Setup.h>
#include <SetupBigButton.h>
#include <SetupBox.h>
#include <SetupButton.h>
#include <SetupCheckBox.h>
#include <SetupColourPicker.h>
#include <SetupControl.h>
#include <SetupEdit.h>
#include <SetupHLineGraph.h>
#include <SetupHSBarGraph.h>
#include <SetupList.h>
#include <SetupMP3Button.h>
#include <SetupMultiList.h>
#include <SetupPicture.h>
#include <SetupSlider.h>
#include <SetupStaticText.h>
#include <SetupStaticTextNoHit.h>
#include <SetupTabButton.h>
#include <SetupVBarGraph.h>

struct function_table_t {
  const char* name;
  uintptr_t original_address;
  uintptr_t detour_address;
  bool modified_implementation;
};

struct function_table_t runblack_win_1_41_17_function_table[] = {
  { "NeedsBiggerText(void)", 0x004079c0, (uintptr_t)NeedsBiggerText__Fv, false },
  { "GetMidTextSize(void)", 0x00407a00, (uintptr_t)GetMidTextSize__Fv, false },
  { "GetSmallTextSize(void)", 0x00407a10, (uintptr_t)GetSmallTextSize__Fv, false },
  { "SetupBox::GetCurrentActiveBox(void)", 0x00407ed0, (uintptr_t)GetCurrentActiveBox__8SetupBoxFv, false },
  { "SetupControl::SetFocus(bool)", 0x00409180, (uintptr_t)SetFocus__12SetupControlFb, false },
  { "SetupControl::SetupControl(int, int, int, int, int, wchar_t *)", 0x00409250, (uintptr_t)ct__12SetupControlFiiiiiPw, false },
  { "SetupControl::SetToolTip(unsigned long)", 0x00409210, (uintptr_t)SetToolTip__12SetupControlFUl, false },
  { "SetupControl::SetToolTip(wchar_t *)", 0x004092f0, (uintptr_t)SetToolTip__12SetupControlFPw, false },
  { "SetupControl::Hide(bool)", 0x00409300, (uintptr_t)Hide__12SetupControlFb, false },
  { "SetupControl::HitTest(int, int)", 0x00409310, (uintptr_t)HitTest__12SetupControlFii, false },
  { "SetupControl::Drag(int, int)", 0x00409340, (uintptr_t)Drag__12SetupControlFii, false },
  { "SetupControl::~SetupControl(void)", 0x004093c0, (uintptr_t)dt__12SetupControlFv, false },
  { "SetupButton::SetupButton(int, int, int, int, int, wchar_t *, int)", 0x004098b0, (uintptr_t)ct__10SetupButtonFiiiiiPwi, false },
  { "SetupButton::~SetupButton(void)", 0x00409940, (uintptr_t)dt__11SetupButtonFb, false },
  { "SetupSlider::SetupSlider(int, int, int, int, int, float, wchar_t *)", 0x00409bf0, (uintptr_t)ct__11SetupSliderFiiiiifPw, false },
  { "SetupSlider::~SetupSlider(void)", 0x00409c50, (uintptr_t)dt__11SetupSliderFb, false },
  { "SetupSlider::Drag(int, int)", 0x00409c70, (uintptr_t)Drag__11SetupSliderFii, false },
  { "SetupList::AutoScroll(bool)", 0x00409dd0, (uintptr_t)AutoScroll__9SetupListFb, false },
  { "SetupList::Drag(int, int)", 0x0040a110, (uintptr_t)Drag__9SetupListFii, false },
  { "SetupList::SetupList(int, int, int, int, int)", 0x0040a450, (uintptr_t)ct__9SetupListFiiiii, false },
  { "SetupList::~SetupList(void)", 0x0040a540, (uintptr_t)dt__11SetupSliderFb, false },
  { "SetupMultiList::SetupMultiList(int, int, int, int, int, int)", 0x0040b420, (uintptr_t)ct__14SetupMultiListFiiiiii, false },
  { "SetupMultiList::~SetupMultiList(void)", 0x0040b4a0, (uintptr_t)dt__14SetupMultiListFb, false },
  { "SetupEdit::CalcCharpos(int)", 0x0040c090, (uintptr_t)CalcCharpos__9SetupEditFi, false },
  { "SetupEdit::Drag(int, int)", 0x0040c150, (uintptr_t)Drag__9SetupEditFii, false },
  { "SetupEdit::SetupEdit(int, int, int, int, int, wchar_t *, int)", 0x0040c220, (uintptr_t)ct__9SetupEditFiiiiiPwi, false },
  { "SetupEdit::SetFocus(bool)", 0x0040c500, (uintptr_t)dt__9SetupListFb, false },
  { "SetupEdit::~SetupEdit(void)", 0x0040c560, (uintptr_t)dt__9SetupEditFb, false },
  { "SetupBigButton::SetupBigButton(int, int, int, wchar_t *, int, int, int)", 0x0040d260, (uintptr_t)ct__14SetupBigButtonFiiiPwiii, false },
  { "SetupBigButton::HitTest(int, int)", 0x0040d310, (uintptr_t)HitTest__14SetupBigButtonFii, false },
  { "SetupBigButton::~SetupBigButton(void)", 0x0040d360, (uintptr_t)dt__14SetupBigButtonFb, false },
  { "SetupHLineGraph::SetupHLineGraph(int, int, int, int, int, wchar_t *, bool)", 0x0040e510, (uintptr_t)ct__15SetupHLineGraphFiiiiiPwb, false },
  { "SetupHLineGraph::~SetupHLineGraph(void)", 0x0040e5c0, (uintptr_t)dt__15SetupHLineGraphFb, false },
  { "SetupVBarGraph::SetupVBarGraph(int, int, int, int, int, wchar_t *)", 0x0040ef00, (uintptr_t)ct__14SetupVBarGraphFiiiiiPw, false },
  { "SetupVBarGraph::~SetupVBarGraph(void)", 0x0040ef90, (uintptr_t)dt__14SetupVBarGraphFb, false },
  { "SetupTabButton::SetupTabButton(int, int, int, int, int, wchar_t *, int, int, int)", 0x0040f5e0, (uintptr_t)ct__14SetupTabButtonFiiiiiPwiii, false },
  { "SetupTabButton::~SetupTabButton(void)", 0x0040f690, (uintptr_t)dt__14SetupTabButtonFb, false },
  { "SetupPicture::Drag(int, int)", 0x0040fa10, (uintptr_t)Drag__12SetupPictureFii, false },
  { "SetupPicture::SetupPicture(int, int, int, LH3DMaterial *, int, int, bool, int, bool)", 0x004105d0, (uintptr_t)ct__12SetupPictureFiiiP12LH3DMaterialiibib, false },
  { "SetupPicture::~SetupPicture(void)", 0x00410720, (uintptr_t)dt__12SetupPictureFb, false },
  { "SetupPicture::SetFocus(bool)", 0x00410740, (uintptr_t)SetFocus__12SetupPictureFb, false },
  { "SetupPicture::Drag(int, int)", 0x00410810, (uintptr_t)Drag__17SetupColourPickerFii, false },
  { "SetupColourPicker::SetupColourPicker(int, int, int, int, int, int, LH3DMaterial *)", 0x00410ac0, (uintptr_t)ct__17SetupColourPickerFiiiiiiP12LH3DMaterial, false },
  { "SetupColourPicker::~SetupColourPicker(void)", 0x00410b60, (uintptr_t)dt__17SetupColourPickerFb, false },
  { "SetupCheckBox::SetupCheckBox(int, int, int, bool, int, wchar_t *, int)", 0x00410f10, (uintptr_t)ct__13SetupCheckBoxFiiibiPwi, false },
  { "SetupCheckBox::HitTest(int, int)", 0x00410f90, (uintptr_t)HitTest__13SetupCheckBoxFii, false },
  { "SetupCheckBox::~SetupCheckBox(void)", 0x00411070, (uintptr_t)dt__13SetupCheckBoxFb, false },
  { "SetupStaticText::~SetupStaticText(void)", 0x00411670, (uintptr_t)dt__15SetupStaticTextFb, false },
  { "SetupThing::GetTextWidth(wchar_t *, float, int, float)", 0x00411720, (uintptr_t)GetTextWidth__10SetupThingFPwfif, false },
  { "SetupThing::DrawTextWrap(int, int, int, int, int, bool, wchar_t *, int, LH3DColor *, bool, bool)", 0x00411750, (uintptr_t)DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb, false },
  { "SetupThing::DrawText(int, int, int, TEXTJUSTIFY, wchar_t *, int, LH3DColor *, int)", 0x004119b0, (uintptr_t)DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori, false },
  { "SetupThing::adjust(int &, int &)", 0x00411b40, (uintptr_t)adjust__10SetupThingFRiRi, false },
  { "SetupThing::unadjust(int &, int &)", 0x00411c30, (uintptr_t)unadjust__10SetupThingFRiRi, false },
  { "SetupThing::adjusty(int)", 0x00411e70, (uintptr_t)adjusty__10SetupThingFi, false },
  { "SetupThing::unadjustsize(float)", 0x00412030, (uintptr_t)unadjustsize__10SetupThingFf, false },
  { "SetupThing::DrawBigButton(int, int, bool, bool, int, BBSTYLE, bool, int, int)", 0x00412150, (uintptr_t)DrawBigButton__10SetupThingFiibbi7BBSTYLEbii, false },
  { "SetupThing::DrawLine(int, int, int, int, unsigned long, int, float, float)", 0x004125a0, (uintptr_t)DrawLine__10SetupThingFiiiiUliff, false },
  { "LH3DRender::SetRenderState(D3DRENDERSTATETYPE, unsigned long)", 0x00412940, (uintptr_t)SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl, false },
  { "SetupThing::DrawBox(int, int, int, int, float, float, float, float, LH3DMaterial *, LH3DColor *, int, int, int, bool, float)", 0x00412980, (uintptr_t)DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf, false },
  { "SetupThing::DrawQuad(int, int, int, int, int, int, int, int, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long)", 0x00412eb0, (uintptr_t)DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl, false },
  { "SetupThing::DrawBox(int, int, int, int, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long)", 0x004132c0, (uintptr_t)DrawBox__10SetupThingFiiiiUlUlUlUlUlUl, false },
  { "SetupThing::DrawBevBox(int, int, int, int, int, int, int, unsigned long)", 0x00413c20, (uintptr_t)DrawBevBox__10SetupThingFiiiiiiiUl, false },
  { "SetupHSBarGraph::~SetupHSBarGraph(void)", 0x0056d960, (uintptr_t)dt__15SetupHSBarGraphFb, false },
  { "SetupStaticTextNoHit::HitTest(int, int)", 0x00571f00, (uintptr_t)HitTest__20SetupStaticTextNoHitFii, false },
  { "SetupStaticTextNoHit::~SetupStaticTextNoHit(void)", 0x00571f10, (uintptr_t)dt__20SetupStaticTextNoHitFb, false },
  { "SetupMP3Button::~SetupMP3Button(void)", 0x00571f30, (uintptr_t)dt__14SetupMP3ButtonFb, false },
  { "LHScreen::SetFullscreenMode(int)", 0x007dd0d0, (uintptr_t)SetFullscreenMode__8LHScreenFi, true },
  { "DrawAndClip(D3DPRIMITIVETYPE, unsigned long, Vertex3D *, unsigned long, unsigned short *, unsigned long)", 0x0082a500, (uintptr_t)DrawAndClip__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl, true },
  { "DrawAndClip2D(D3DPRIMITIVETYPE, unsigned long, Vertex3D *, unsigned long, unsigned short *, unsigned long)", 0x0082a5b0, (uintptr_t)DrawAndClip2D__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl, true },
  { "LH3DRender::SetTextureStageState(unsigned long, D3DTEXTURESTAGESTATETYPE, unsigned long)", 0x0082b9c0, (uintptr_t)SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl, true },
  { "LH3DRender::SetD3DTillingOn(int)", 0x0082ff10, (uintptr_t)SetD3DTillingOn__10LH3DRenderFi, true },
  { "LH3DRender::SetD3DTillingOff(int)", 0x0082ff50, (uintptr_t)SetD3DTillingOff__10LH3DRenderFi, true },
};

#endif //BLACKHACK_FUNCTION_TABLE_H
