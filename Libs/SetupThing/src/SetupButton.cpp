#include "SetupButton.h"

#include <logger.h>

#include "Setup.h"
#include "SetupThing.h"

void __fastcall Draw__11SetupButtonFbb(
    struct SetupButton* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  DrawBevBox__10SetupThingFiiiiiiiUl(this->super.rect.p0.x,
      this->super.rect.p0.y, this->super.rect.p1.x, this->super.rect.p1.y,
      hovered ? 2 : 1, 0x10, 0xffffffff, white);

  int text_size = GetTextSize__12SetupControlFv(&this->super);
  for (int i = 0; i < text_size - 10; ++i) {
    float text_width = GetTextWidth__10SetupThingFPwfif(
        this->super.label, (float)(text_size - i), 0, 1.0f);
    if (text_width <= (float)(this->super.rect.p1.x - this->super.rect.p0.x)) {
      break;
    }
  }

  const struct LH3DColor* p_color;
  if (hovered) {
    p_color = &LH3DColor_ARRAY_00c4ccd8_3;
  }
  else if (selected) {
    p_color = &LH3DColor_00c4ccf8;
  }
  else {
    p_color = &LH3DColor_ARRAY_00c4ccd8_0;
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
      (this->super.rect.p0.x + this->super.rect.p1.x) / 2 + this->pressed * 2,
      ((this->super.rect.p0.y + this->super.rect.p1.y) / 2 - text_size / 2) +
          this->pressed * 2,
      this->super.rect.p1.x - this->super.rect.p0.x, TEXTJUSTIFY_CENTRE,
      this->super.label, text_size, p_color, 0);
}

struct SetupButton* __fastcall ct__10SetupButtonFiiiiiPwi(
    struct SetupButton* this, const void* edx, int id, int x, int y, int width,
    int height, const wchar_t* label, int param_8) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\", %d) from %p",
      this, id, x, y, width, height, label, param_8, puEBP[1]);

  ct__12SetupControlFiiiiiPw(&this->super, edx, id, x, y, width, height, label);

  this->super.vftptr = &vt__SetupButton;
  this->pressed = false;
  this->field_0x240 = param_8;

  return this;
}

void __fastcall MouseDown__10SetupButtonFiib(
    struct SetupButton* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  this->pressed = true;
}

void __fastcall MouseUp__10SetupButtonFiib(
    struct SetupButton* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  this->pressed = false;
}

void __fastcall KeyDown__11SetupButtonFii(struct SetupButton* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.setup_box != NULL) {
    LOG_DEBUG("%p", this->super.setup_box->vftptr->field_0x0);
    this->super.setup_box->vftptr->field_0x0(
        this->super.setup_box, edx, key, mod);
  }
}

struct SetupButton* __fastcall dt__11SetupButtonFb(
    struct SetupButton* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__12SetupControlFb(&this->super, edx, param_1);

  return this;
}
