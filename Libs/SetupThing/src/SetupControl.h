#ifndef BLACKHACK_SETUPCONTROL_H
#define BLACKHACK_SETUPCONTROL_H

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <wchar.h>

#include "LHKey.h"
#include "SetupBox.h"
#include "SetupRect.h"

struct SetupControl;

typedef void (__fastcall vt__SetupControl_SetToolTipUl_t)(struct SetupControl* this, const void* edx, uint32_t tooltip_id);
typedef void (__fastcall vt__SetupControl_SetToolTipPw_t)(struct SetupControl* this, const void* edx, const wchar_t* tooltip);
typedef void (__fastcall vt__SetupControl_Hide_t)(struct SetupControl* this, const void* edx, bool hidden);
typedef void (__fastcall vt__SetupControl_SetFocus_t)(struct SetupControl* this, const void* edx, bool focus);
typedef bool (__fastcall vt__SetupControl_HitTest_t)(struct SetupControl* this, const void* edx, int x, int y);
typedef void (__fastcall vt__SetupControl_Draw_t)(struct SetupControl* this, const void* edx, bool hovered, bool selected);
typedef void (__fastcall vt__SetupControl_Drag_t)(struct SetupControl* this, const void* edx, int x, int y);
typedef void (__fastcall vt__SetupControl_Mouse_t)(struct SetupControl* this, const void* edx, int x, int y, bool param_3);
typedef void (__fastcall vt__SetupControl_Click_t)(struct SetupControl* this, const void* edx, int x, int y);
typedef void (__fastcall vt__SetupControl_KeyDown_t)(struct SetupControl* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
typedef void (__fastcall vt__SetupControl_Char_t)(struct SetupControl* this, const void* edx, int character);
typedef struct SetupControl* (__fastcall vt__SetupControl_dtor_t)(struct SetupControl* this, const void* edx, bool param_1);

struct vt__SetupControl_t {
  vt__SetupControl_SetToolTipUl_t * const  SetToolTipUl;
  vt__SetupControl_SetToolTipPw_t * const  SetToolTipPw;
  vt__SetupControl_Hide_t * const  Hide;
  vt__SetupControl_SetFocus_t * const  SetFocus;
  vt__SetupControl_HitTest_t * const  HitTest;
  vt__SetupControl_Draw_t * const  Draw;
  vt__SetupControl_Drag_t * const  Drag;
  vt__SetupControl_Mouse_t * const  MouseDown;
  vt__SetupControl_Mouse_t * const  MouseUp;
  vt__SetupControl_Click_t * const  Click;
  vt__SetupControl_KeyDown_t * const  KeyDown;
  vt__SetupControl_Char_t * const  Char;
  vt__SetupControl_dtor_t * const  dtor;
};

struct SetupControl {
  const struct vt__SetupControl_t* vftptr;
  uint32_t field_0x4;
  struct SetupRect rect;
  int id;
  int field_0x1c;
  int text_size;
  wchar_t label[0x100];
  const wchar_t* tooltip;
  bool focus;
  bool hidden;
  bool field_0x22a;
  bool field_0x22b;
  uint32_t field_0x22c;
  struct SetupControl* next;
  struct SetupBox* setup_box;
  void* continue_button_callback;
};
static_assert(sizeof(struct SetupControl) == 0x23c, "struct size is wrong");

int __cdecl _purecall();

// inlined: not found in source but is useful with repeated code
int __fastcall GetTextSize__12SetupControlFv(const struct SetupControl* this);
// win1.41 00409250 mac 101668d0 SetupControl::SetupControl(int, int, int, int, int, wchar_t *)
struct SetupControl* __fastcall ct__12SetupControlFiiiiiPw(
    struct SetupControl* this, const void* edx, int id, int x, int y, int width,
    int height, const wchar_t* label);
// win1.41 00409180 mac 1032c7e0 SetupControl::SetFocus(bool)
void __fastcall SetFocus__12SetupControlFb(struct SetupControl *this, const void* edx, bool focus);
// win1.41 00409210 mac 1057a320 SetupControl::SetToolTip(unsigned long)
void __fastcall SetToolTip__12SetupControlFUl(struct SetupControl *this, const void* edx, uint32_t tooltip_id);
// win1.41 004092f0 mac 100c4fd0 SetupControl::SetToolTip(wchar_t *)
void __fastcall SetToolTip__12SetupControlFPw(struct SetupControl *this, const void* edx, const wchar_t *tooltip);
// win1.41 00409300 mac 105a3830 SetupControl::Hide(bool)
void __fastcall Hide__12SetupControlFb(struct SetupControl *this, const void* edx, bool hidden);
// win1.41 00409310 mac 10310540 SetupControl::HitTest(int, int)
bool __fastcall HitTest__12SetupControlFii(struct SetupControl *this, const void* edx, int x, int y);
// win1.41 00409340 mac inline SetupControl::Drag(int, int)
void __fastcall Drag__12SetupControlFii(struct SetupControl* this, const void* edx, int x, int y);
// win1.41 00409350 mac 103dbde0 SetupControl::MouseDown(int, int, bool)
void __fastcall MouseDown__12SetupControlFiib(struct SetupControl *this, const void* edx, int x, int y, bool param_3);
// win1.41 00409360 mac 104faf30 SetupControl::MouseUp(int, int, bool)
void __fastcall MouseUp__12SetupControlFiib(struct SetupControl *this, const void* edx, int x, int y, bool param_3);
// win1.41 00409370 mac 103e3120 SetupControl::Click(int, int)
void __fastcall Click__12SetupControlFii(struct SetupControl *this, const void* edx, int x, int y);
// win1.41 00409380 mac 100d4e40 SetupControl::KeyDown(int, int)
void __fastcall KeyDown__12SetupControlFii(struct SetupControl *this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 00409390 mac 105049b0 SetupControl::Char(int)
void __fastcall Char__12SetupControlFi(struct SetupControl *this, const void* edx, int character);
// win1.41 004093a0 mac 100c48e0 SetupControl::~SetupControl(bool)
struct SetupControl* __fastcall dt__12SetupControlFb(struct SetupControl *this, const void* edx, bool param_1);
// win1.41 004093c0 mac inlined SetupControl::~SetupControl(void)
void __fastcall dt__12SetupControlFv(struct SetupControl *this);

// win1.41 008ab27c mac none SetupControl virtual table
static struct vt__SetupControl_t vt__SetupControl = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)_purecall,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = MouseDown__12SetupControlFiib,
    .MouseUp = MouseUp__12SetupControlFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = KeyDown__12SetupControlFii,
    .Char = Char__12SetupControlFi,
    .dtor = dt__12SetupControlFb,
};

#endif // BLACKHACK_SETUPCONTROL_H
