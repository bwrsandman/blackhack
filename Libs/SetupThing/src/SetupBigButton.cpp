#include "SetupBigButton.h"

#include <logger.h>

#include "Setup.h"

void __fastcall Draw__14SetupBigButtonFbb(
    struct SetupBigButton* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  gSetupThingRect = this->super.super.rect;

  bool interacted =
      hovered || (selected && this->super.super.label[0] == L'\0');

  DrawBigButton__10SetupThingFiibbi7BBSTYLEbii(this->super.super.rect.p0.x,
      this->super.super.rect.p0.y, this->super.pressed, interacted,
      this->super.super.rect.p1.x - this->super.super.rect.p0.x, this->style,
      true, -0xa000, 0xa000);

  const int text_size = GetTextSize__12SetupControlFv(&this->super.super);

  const struct LH3DColor* p_color;
  if (hovered || (selected && this->super.super.label[0] == L'\0')) {
    p_color = &LH3DColor_ARRAY_00c4ccd8_3;
  }
  else {
    p_color = &LH3DColor_00c4ccf8;
  }

  enum TEXTJUSTIFY justify;
  struct LHCoord coord;
  switch (this->text_position) {
  case 0:
    justify = TEXTJUSTIFY_LEFT;
    coord.x = this->super.super.rect.p1.x;
    coord.y = (this->super.super.rect.p0.y + this->super.super.rect.p1.y) / 2 -
              text_size / 2;
    break;

  default:
    assert(false); // if this is hit, then the block is the same as 1
  case 1:
    justify = TEXTJUSTIFY_RIGHT;
    coord.x = this->super.super.rect.p0.x;
    coord.y = (this->super.super.rect.p0.y + this->super.super.rect.p1.y) / 2 -
              text_size / 2;
    break;

  case 2:
    justify = TEXTJUSTIFY_CENTRE;
    coord.x = (this->super.super.rect.p1.x + this->super.super.rect.p0.x) / 2;
    coord.y = this->super.super.rect.p1.y + 2;
    break;
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(coord.x + 2,
      coord.y + 2, 1000, justify, this->super.super.label, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, 0);
  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(coord.x, coord.y, 1000,
      justify, this->super.super.label, text_size, p_color, 0);

  this->inner_rect = gSetupThingRect;
}

struct SetupBigButton* __fastcall ct__14SetupBigButtonFiiiPwiii(
    struct SetupBigButton* this, const void* edx, int id, int x, int y,
    const wchar_t* label, int size, uint32_t text_position,
    enum BBSTYLE style) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, label=\"%S\", size=%d, "
                         "text_position=%u, style=%s) from %p",
      this, id, x, y, label, size, text_position,
      BBSTYLE_strs[style] == NULL ? "" : BBSTYLE_strs[style], puEBP[1]);

  assert(text_position >= 0);
  assert(text_position <= 3);

  ct__10SetupButtonFiiiiiPwi(&this->super, edx, id, x, y, size, size, label, 0);

  this->super.super.vftptr = &vt__SetupBigButton;
  if (text_position == 2) {
    this->super.super.text_size = GetMidTextSize__Fv();
  }
  this->super.pressed = false;
  this->text_position = text_position;
  this->style = style;
  this->inner_rect.p0.x = 0;
  this->inner_rect.p0.y = 0;
  this->inner_rect.p1.x = 0;
  this->inner_rect.p1.y = 0;

  return this;
}

void __fastcall KeyDown__14SetupBigButtonFii(struct SetupBigButton* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.super.setup_box != NULL) {
    LOG_DEBUG("%p", this->super.super.setup_box->vftptr->field_0x0);
    this->super.super.setup_box->vftptr->field_0x0(
        this->super.super.setup_box, edx, key, mod);
  }
}

bool __fastcall HitTest__14SetupBigButtonFii(
    struct SetupBigButton* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  bool result = HitTest__12SetupControlFii(&this->super.super, edx, x, y);

  if (!result) {
    result = x >= this->inner_rect.p0.x && y >= this->inner_rect.p0.y &&
             x < this->inner_rect.p1.x && y < this->inner_rect.p1.y;
  }

  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) -> %s from %p", this, x, y,
      result ? "true" : "false", puEBP[1]);

  return result;
}

struct SetupBigButton* __fastcall dt__14SetupBigButtonFb(
    struct SetupBigButton* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}
