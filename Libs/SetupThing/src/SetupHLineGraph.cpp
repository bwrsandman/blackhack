#include "SetupHLineGraph.h"

#include <stdlib.h>

#include <logger.h>

#include <LHSys.h>
#include <Memory.h>
#include <Utils.h>

#include "Setup.h"
#include "SetupThing.h"

struct HLineData* __fastcall ct__9HLineData(struct HLineData* this) {
  ct__9LH3DColorFUl(&this->color, NULL, 0x00000000);
  this->point_count = 0;
  this->points = NULL;
  return this;
}

struct HLineData* __fastcall as__9HLineDataFRC9HLineData(
    struct HLineData* this, const void* edx, const struct HLineData* other) {
  as__9LH3DColorFRC9LH3DColor(&this->color, edx, &other->color);
  this->point_count = other->point_count;
  this->points = other->points;
  return this;
}

void __fastcall SetNum__9HLineDataFi(
    struct HLineData* this, const void* edx, int num) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, num=%d) from %p", this, num, puEBP[1]);

  num = max(num, 0);
  float* points = __nw__FUl((size_t)num * sizeof(float), __FILE__, __LINE__);
  memset(points, 0, num * sizeof(float));
  memcpy(points, this->points, min(this->point_count, num) * sizeof(float));
  __dl__FPv(this->points);
  this->points = points;
  this->point_count = num;
}

struct HLineDataListNode* __fastcall ct__HLineDataListNode(
    struct HLineDataListNode* this, const void* edx, struct HLineData* payload,
    struct HLineDataListNode* next) {
  this->payload = payload;
  this->next = next;

  return this;
}

struct HLineDataListNode* __fastcall GetStart__HLineDataList(
    const struct HLineDataList* this) {
  return this->head;
}

struct HLineDataListNode* __fastcall GetNodeAtPosition__HLineDataList(
    const struct HLineDataList* this, const void* edx, int index) {
  if (this->count == 0 || this->head == NULL || this->count <= index) {
    return NULL;
  }

  if (index <= 0) {
    return this->head;
  }

  struct HLineDataListNode* walker = GetStart__HLineDataList(this);
  for (int i = 0; i < index; ++i) {
    walker = walker->next;
  }

  return walker;
}

struct HLineDataListNode* __fastcall GetLastNode__HLineDataList(
    const struct HLineDataList* this) {
  struct HLineDataListNode* result = NULL;

  for (struct HLineDataListNode* node = GetStart__HLineDataList(this);
       node != NULL; node = node->next) {
    result = node;
  }

  return result;
}

void __fastcall PushBack__HLineDataList(
    struct HLineDataList* this, const void* edx, struct HLineData* payload) {
  struct HLineDataListNode* node = malloc(sizeof(struct HLineDataListNode));
  assert(node);
  ct__HLineDataListNode(node, edx, payload, NULL);

  struct HLineDataListNode* tail = GetLastNode__HLineDataList(this);
  if (tail) {
    tail->next = node;
  }
  else {
    this->head = node;
  }

  ++this->count;
}

void __fastcall Draw__15SetupHLineGraphFbb(struct SetupHLineGraph* this,
    const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  int text_size = GetTextSize__12SetupControlFv(&this->super.super);

  float point_range = this->max_point - this->min_point;
  int height = this->super.super.rect.p1.y - this->super.super.rect.p0.y;

  DrawBevBox__10SetupThingFiiiiiiiUl(this->super.super.rect.p0.x,
      this->super.super.rect.p0.y, this->super.super.rect.p1.x,
      this->super.super.rect.p1.y, 1, 0x10, 0xffffffff, white);

  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
      this->super.super.rect.p0.x + 1, this->super.super.rect.p1.y + 1,
      this->super.super.rect.p1.x + 1,
      this->super.super.rect.p1.y + text_size + 1,
      this->super.super.rect.p1.y + 1, true, this->super.super.label, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, true, false);

  const struct LH3DColor* pLVar7 =
      hovered || (selected && this->super.super.label[0] == L'\0')
          ? &LH3DColor_ARRAY_00c4ccd8_3
          : &LH3DColor_00c4ccf8;

  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(this->super.super.rect.p0.x,
      this->super.super.rect.p1.y, this->super.super.rect.p1.x,
      text_size + this->super.super.rect.p1.y, this->super.super.rect.p1.y,
      true, this->super.super.label, text_size, pLVar7, true, false);

  if (!this->percent_mode) {
    struct HLineDataListNode* walker =
        GetStart__HLineDataList(&this->line_data_list);
    if (walker != NULL) {
      struct HLineData* payload = walker->payload;
      while (payload != NULL) {
        struct LH3DColor color = {
          .b = payload->color.b,
          .g = payload->color.g,
          .r = payload->color.r,
          .a = payload->color.a,
        };
        struct LH3DColor color_1 = {
          .b = payload->color.b, .g = payload->color.g, .r = payload->color.r
        };
        if (1 < payload->point_count) {
          float x_interval = (float)(this->super.super.rect.p1.x -
                                     this->super.super.rect.p0.x - 4) /
                             (float)(payload->point_count - 1);

          int min_y =
              (int)((float)(this->super.super.rect.p1.y - 2) -
                    ((payload->points[0] - this->min_point) / point_range) *
                        (float)(height - 4));
          int y_1 = this->super.super.rect.p0.y + 4;
          if (y_1 < min_y) {
            y_1 = min(min_y, this->super.super.rect.p1.y - 4);
          }

          float x_1 = (float)(this->super.super.rect.p0.x + 2);
          for (int i = 1; i < payload->point_count; ++i) {
            int y_2 =
                (int)((float)(this->super.super.rect.p1.y - 2) -
                      ((payload->points[i] - this->min_point) / point_range) *
                          (float)(height - 4));
            if (y_2 <= this->super.super.rect.p0.y + 4) {
              y_2 = this->super.super.rect.p0.y + 4;
            }
            else if (y_2 >= this->super.super.rect.p1.y - 4) {
              y_2 = this->super.super.rect.p1.y - 4;
            }
            float x_2 = x_1 + x_interval;
            DrawLine__10SetupThingFiiiiUliff(
                (int)x_1, y_1, (int)x_2, y_2, color, 1, 0.0f, 100.0f);
            x_1 = x_2;
            y_1 = y_2;
          }


          min_y = (int)((float)(this->super.super.rect.p1.y - 2) -
                        ((payload->points[0] - this->min_point) / point_range) *
                            (float)(this->super.super.rect.p1.y -
                                    this->super.super.rect.p0.y - 4));
          y_1 = this->super.super.rect.p0.y + 4;
          if (y_1 < min_y) {
            y_1 = min(min_y, this->super.super.rect.p1.y - 4);
          }
          x_1 = (float)(this->super.super.rect.p0.x + 2);
          for (int i = 1; i < payload->point_count; ++i) {
            int y_2 =
                (int)((float)(this->super.super.rect.p1.y - 2) -
                      ((payload->points[i] - this->min_point) / point_range) *
                          (float)(height - 4));

            if (y_2 <= this->super.super.rect.p0.y + 4) {
              y_2 = this->super.super.rect.p0.y + 4;
            }
            else if (y_2 >= this->super.super.rect.p1.y - 4) {
              y_2 = this->super.super.rect.p1.y - 4;
            }

            float x_2 = x_1 + x_interval;
            DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl((int)x_1, y_1 - 2,
                (int)x_2, y_2 - 2, (int)x_2, y_2, (int)x_1, y_1, color_1,
                color_1, color, color, 0, 1);
            DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl((int)x_1, y_1 + 2,
                (int)x_2, y_2 + 2, (int)x_2, y_2, (int)x_1, y_1, color_1,
                color_1, color, color, 0, 1);
            y_1 = y_2;
            x_1 = x_2;
          }
        }
        struct HLineData** ppHVar1;
        walker = this->line_data_list.head;
        do {
          if (walker == NULL) {
            break;
          }
          ppHVar1 = &walker->payload;
          walker = walker->next;
        } while (*ppHVar1 != payload);
        if (walker == NULL) {
          break;
        }
        payload = walker->payload;
      }
    }
  }
  else {
    if (this->line_data_list.head != NULL &&
        this->line_data_list.head->payload != NULL) {
      struct HLineDataListNode* walker = this->line_data_list.head;
      int local_230 = 0;
      while (walker != NULL && walker->payload != NULL) {
        if (local_230 > walker->payload->point_count || local_230 == 0) {
          local_230 = walker->payload->point_count;
        }

        struct HLineData* payload = walker->payload;
        walker = this->line_data_list.head;
        while (walker != NULL) {
          if (walker->payload == payload) {
            walker = walker->next;
            break;
          }
          walker = walker->next;
        }
      }

      if (local_230 > 1) {
        float current_x = (float)(this->super.super.rect.p0.x + 2);
        --local_230;
        float x_interval = (float)(this->super.super.rect.p1.x -
                                   this->super.super.rect.p0.x - 4) /
                           (float)local_230;
        for (int i = 0; i < local_230; ++i) {
          walker = this->line_data_list.head;
          float min_x_scaling = 0.0f;
          float max_x_scaling = 0.0f;
          if (walker != NULL && walker->payload != NULL) {
            while (walker != NULL && walker->payload != NULL) {
              struct HLineData* payload = walker->payload;
              min_x_scaling += payload->points[i];
              max_x_scaling += payload->points[i + 1];
              walker = this->line_data_list.head;
              while (walker != NULL) {
                if (walker->payload == payload) {
                  walker = walker->next;
                  break;
                }
                walker = walker->next;
              }
            }
            if (min_x_scaling != 0.0f) {
              min_x_scaling = 1.0f / min_x_scaling;
            }
            if (max_x_scaling != 0.0f) {
              max_x_scaling = 1.0f / max_x_scaling;
            }
          }
          int y_min = this->super.super.rect.p1.y - 2;
          walker = this->line_data_list.head;
          float local_20c = 0.0f;
          float local_210 = 0.0f;
          if (walker != NULL && walker->payload != NULL) {
            struct HLineData* payload = walker->payload;
            int x_max = (int)(current_x + x_interval);
            int x_min = (int)current_x;
            while (walker != NULL && walker->payload != NULL) {
              struct LH3DColor color_max = {
                .b = payload->color.b,
                .g = payload->color.g,
                .r = payload->color.r,
                .a = payload->color.a,
              };
              struct LH3DColor color_min = {
                .b = payload->color.b,
                .g = payload->color.g,
                .r = payload->color.r,
              };
              local_20c += min_x_scaling * payload->points[i];
              local_210 += max_x_scaling * payload->points[i + 1];
              int y_max = (int)((float)(this->super.super.rect.p1.y - 2) -
                                (float)(height - 4) * local_20c);

              DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(x_min, y_min, x_max, y_max,
                  color_min, color_min, color_max, color_max, 0, 1);

              payload = walker->payload;
              walker = GetStart__HLineDataList(&this->line_data_list);
              while (walker != NULL) {
                if (walker->payload == payload) {
                  walker = walker->next;
                  break;
                }
                walker = walker->next;
              }
            }
          }
          current_x += x_interval;
        }
      }
    }
  }

  // Get tick spacing
  if (!this->percent_mode) {
    int local_230 = (height < 0 ? height + 3 : height) / 4;
    float spacing = 0.25f;
    while (point_range / spacing > (float)local_230) {
      spacing *= 2.0f;
    }

    struct LH3DColor color = white;

    // Draw tick marks
    for (int i = (int)((float)(int)(this->min_point / spacing) / spacing);
         (float)i * spacing < this->max_point; ++i) {
      if (i != 0) {
        color.a = (uint8_t)((i % 4u) ? 0x10 : 0x50);
      }
      int y_min = (int)((float)(this->super.super.rect.p1.y - 2) -
                        (((float)i * spacing - this->min_point) / point_range) *
                            (float)(height - 4));
      if (y_min > this->super.super.rect.p0.y + 2 &&
          y_min < this->super.super.rect.p1.y - 2) {
        DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(this->super.super.rect.p0.x + 2,
            y_min, this->super.super.rect.p1.x - 2, y_min + 1, color, color,
            color, color, 0, 1);
      }
    }
  }

  if (!hovered) {
    return;
  }

  // Hovered value preview
  struct LHCoord mouse;
  ct__7LHCoordFRC7LHCoord(&mouse, edx, &Get__5LHSysFv()->mouse.coord_0xbc);
  unadjust__10SetupThingFRiRi(&mouse.x, &mouse.y);

  float value = saturatef(
      (float)(this->super.super.rect.p1.y - mouse.y - 2) / (float)(height - 4));
  if (!this->percent_mode) {
    value = value * point_range + this->min_point;
  }

  wchar_t value_preview[0x100];
  if (this->percent_mode) {
    swprintf_s(
        value_preview, _countof(value_preview), L"%d%%", (int)(value * 100.0f));
  }
  else if (this->max_point > 10.0f) {
    swprintf_s(value_preview, _countof(value_preview), L"%d", (int)value);
  }
  else {
    swprintf_s(value_preview, _countof(value_preview), u"%0.2f", value);
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
      this->super.super.rect.p0.x - 1, mouse.y - text_size / 2 + 1, 100,
      TEXTJUSTIFY_RIGHT, value_preview, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, 0);

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
      this->super.super.rect.p0.x - 2, mouse.y - text_size / 2, 100,
      TEXTJUSTIFY_RIGHT, value_preview, text_size, &LH3DColor_00c4ccf8,
      0);
}

struct SetupHLineGraph* __fastcall ct__15SetupHLineGraphFiiiiiPwb(
    struct SetupHLineGraph* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label, bool percent_mode) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\", percent_mode=%s) from %p",
      this, id, x, y, width, height, label, percent_mode ? "true" : "false",
      puEBP[1]);

  ct__10SetupButtonFiiiiiPwi(
      &this->super, edx, id, x, y, width, height, label, 0);

  this->super.super.vftptr = &vt__SetupHLineGraph.super;
  this->super.super.text_size = GetSmallTextSize__Fv();
  this->super.pressed = false;

  this->line_data_list.count = 0;
  this->line_data_list.head = NULL;
  this->percent_mode = percent_mode;
  Reset__15SetupHLineGraphFv(this);

  return this;
}

void __fastcall KeyDown__15SetupHLineGraphFii(struct SetupHLineGraph* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.super.setup_box != NULL) {
    LOG_DEBUG("%p", this->super.super.setup_box->vftptr->field_0x0);
    this->super.super.setup_box->vftptr->field_0x0(
        this->super.super.setup_box, edx, key, mod);
  }
}

void __fastcall MouseUp__15SetupHLineGraphFiib(
    struct SetupHLineGraph* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  if (param_3) {
    this->percent_mode = !this->percent_mode;
  }
}

struct SetupHLineGraph* __fastcall dt__15SetupHLineGraphFb(
    struct SetupHLineGraph* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}

struct HLineData* __fastcall dt__9HLineDataFv(
    struct HLineData* this, const void* edx, bool param_2) {
  __dl__FPv((void*)this->points);
  if (param_2) {
    __dl__FPv(this);
  }
  return this;
}

void __fastcall Reset__15SetupHLineGraphFv(struct SetupHLineGraph* this) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p) from 0x%p", this, puEBP[1]);

  struct HLineDataListNode* walker =
      GetStart__HLineDataList(&this->line_data_list);

  while (walker != NULL) {
    struct HLineDataListNode* next = walker->next;
    if (walker->payload) {
      dt__9HLineDataFv(walker->payload, NULL, true);
    }
    __dl__FPv(walker);
    walker = next;
  }

  this->line_data_list.head = NULL;
  this->line_data_list.count = 0;
}

void __fastcall SetScale__15SetupHLineGraphFffb(struct SetupHLineGraph* this,
    const void* edx, float max_point, float min_point, bool centered_at_zero) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %f, %f, %s) from 0x%p", this, max_point,
      min_point, centered_at_zero ? "true" : false, puEBP[1]);

  if (max_point > 0.0f) {
    if (min_point >= max_point || centered_at_zero) {
      min_point = 0.0f;
    }
  }
  else {
    max_point = -1.0e10f;
    min_point = 1.0e10f;

    struct HLineDataListNode* walker =
        GetStart__HLineDataList(&this->line_data_list);
    while (walker != NULL && walker->payload != NULL) {
      for (int i = 0; i < walker->payload->point_count; ++i) {
        max_point = max(max_point, walker->payload->points[i]);
        min_point = min(min_point, walker->payload->points[i]);
      }
      walker = GetStart__HLineDataList(&this->line_data_list);
      struct HLineData* payload = walker->payload;
      while (walker != NULL) {
        if (walker->payload == payload) {
          walker = walker->next;
          break;
        }
        walker = walker->next;
      }
    }

    if (min_point >= max_point || centered_at_zero) {
      min_point = 0.0f;
    }
  }

  if (max_point <= min_point) {
    max_point = min_point + 1.0f;
  }

  this->max_point = max_point;
  this->min_point = min_point;
}

void __fastcall AddLine__15SetupHLineGraphFR9HLineData(
    struct SetupHLineGraph* this, const void* edx,
    const struct HLineData* line) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, line={color=0x%08X, point_count=%d, points=%p}) "
                         "from 0x%p",
      this, *(uint32_t*)&line->color, line->point_count, line->points,
      puEBP[1]);

  struct HLineData* data =
      ct__9HLineData(__nw__FUl(sizeof(struct HLineData), __FILE__, __LINE__));
  as__9LH3DColorFRC9LH3DColor(&data->color, edx, &line->color);
  SetNum__9HLineDataFi(data, edx, line->point_count);
  memcpy(
      data->points, line->points, line->point_count * sizeof(line->points[0]));

  PushBack__HLineDataList(&this->line_data_list, edx, data);
}

void __fastcall SetLine__15SetupHLineGraphFiR9HLineData(
    struct SetupHLineGraph* this, const void* edx, int index,
    const struct HLineData* line) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, index=%d, line={0x%08X, %d, %p}) from 0x%p",
      this, index, *(uint32_t*)&line->color, line->point_count, line->points,
      puEBP[1]);

  if (index > -1 && index < this->line_data_list.count) {
    struct HLineDataListNode* node =
        GetNodeAtPosition__HLineDataList(&this->line_data_list, edx, index);
    if (node != NULL) {
      as__9HLineDataFRC9HLineData(node->payload, edx, line);
    }
  }
}

void __fastcall GetLine__15SetupHLineGraphFiR9HLineData(
    const struct SetupHLineGraph* this, const void* edx, int index,
    struct HLineData* result) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, index=%d, result=0x%p) from 0x%p", this, index,
      result, puEBP[1]);

  if (index > -1 && index < this->line_data_list.count) {
    struct HLineDataListNode* node =
        GetNodeAtPosition__HLineDataList(&this->line_data_list, edx, index);
    if (node != NULL) {
      as__9HLineDataFRC9HLineData(result, edx, node->payload);
    }
  }
}
