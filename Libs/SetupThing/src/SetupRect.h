#ifndef BLACKHACK_SETUPRECT_H
#define BLACKHACK_SETUPRECT_H

#include <LHCoord.h>

struct SetupRect {
  struct LHCoord p0;
  struct LHCoord p1;
};

#endif // BLACKHACK_SETUPRECT_H
