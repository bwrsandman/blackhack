#ifndef BLACKHACK_SETUPEDIT_H
#define BLACKHACK_SETUPEDIT_H

#include "SetupControl.h"

struct SetupEdit {
  struct SetupControl super;
  struct SetupList* field_0x23c;
  int field_0x240;
  int field_0x244;
  int field_0x248;
  int cursor_position;
  int select_start;
  int select_end;
  int field_0x258;
  int editable; ///< must be 32 bits
  bool masked_text;
  wchar_t text[0x100];
  uint32_t field_0x464;
};

static_assert(sizeof(struct SetupEdit) == 0x468, "struct size is wrong");

// inline: factoring common code
void __fastcall ClampCursor__9SetupEditFi(struct SetupEdit *this);
// inline: factoring common code
void __fastcall FixCursor__9SetupEditFi(struct SetupEdit *this);
// inline: factoring common code
void __fastcall FixSelect__9SetupEditFi(struct SetupEdit *this);
// win1.41 0040b5f0 mac 103e3630 SetupEdit::Char(int)
void __fastcall Char__9SetupEditFi(struct SetupEdit *this, const void* edx, int character);
// win1.41 0040baf0 mac 103813a0 SetupEdit::KeyDown(int)
void __fastcall KeyDown__9SetupEditFii(struct SetupEdit* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 0040c090 mac 103dc2c0 SetupEdit::CalcCharpos(int)
int __fastcall CalcCharpos__9SetupEditFi(struct SetupEdit *this, const void* edx, int pos);
// win1.41 0040c150 mac 103dc010 SetupEdit::Drag(int, int)
void __fastcall Drag__9SetupEditFii(struct SetupEdit *this, const void* edx, int x, int y);
// win1.41 0040c170 mac 10430180 SetupEdit::MouseDown(int, int, bool)
void __fastcall MouseDown__9SetupEditFiib(struct SetupEdit *this, const void* edx, int x, int y, bool param_3);
// win1.41 0040c1a0 mac 101178b0 SetupEdit::MouseUp(int, int, bool)
void __fastcall MouseUp__9SetupEditFiib(struct SetupEdit* this, const void* edx, int x, int y, bool param_3);
// win1.41 0040c220 mac 1014cca0 SetupEdit::SetupEdit(int, int, int, int, int, wchar_t *, int)
struct SetupEdit * __fastcall
ct__9SetupEditFiiiiiPwi(struct SetupEdit *this, const void* edx, int id, int x, int y, int width, int height, const wchar_t *label, bool editable);
// win1.41 0040c500 mac 100c1900 SetupEdit::SetFocus(bool)
void __fastcall SetFocus__9SetupEditFb(struct SetupEdit *this, const void* edx, bool focus);
// win1.41 0040c560 mac 1035a3a0 SetupEdit::~SetupEdit(void)
struct SetupEdit* __fastcall dt__9SetupEditFb(struct SetupEdit* this, const void* edx, bool param_1);
// win1.41 0040c580 mac 1055c840 SetupEdit::Draw(bool, bool)
void __fastcall Draw__9SetupEditFbb(struct SetupEdit* this, const void* edx, bool hovered, bool selected);

// win1.41 008ab3a0 mac none SetupEdit virtual table
static struct vt__SetupControl_t vt__SetupEdit = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = (vt__SetupControl_SetFocus_t*)SetFocus__9SetupEditFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__9SetupEditFbb,
    .Drag = (vt__SetupControl_Drag_t*)Drag__9SetupEditFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__9SetupEditFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__9SetupEditFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__9SetupEditFii,
    .Char = (vt__SetupControl_Char_t*)Char__9SetupEditFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__9SetupEditFb,
};

#endif // BLACKHACK_SETUPEDIT_H
