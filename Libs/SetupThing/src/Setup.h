#ifndef BLACKHACK_SETUP_H
#define BLACKHACK_SETUP_H

#include <stdbool.h>

#include "LH3DColor.h"

#include "SetupRect.h"

/// win1.41: Global(00c4ccb8) and static initialized (00407980)
/// but only used in Setup* functions
static struct SetupRect gSetupThingRect = {
    .p0 = { 0, 0 },
    .p1 = { 0, 0 },
};

/// win1.41: Global(008ab250) and compiler const defined
/// white (0xFFFFFFFF), white(0xFFFFFFFF), orange(0xFFFF8000), black(0xFF000000)
static const struct LH3DColor LH3DColor_ARRAY_008ab250[4] = {
    { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF},
    { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF},
    { .b = 0x00, .g = 0x80, .r = 0xFF, .a = 0xFF},
    { .b = 0x00, .g = 0x00, .r = 0x00, .a = 0xFF},
};
/// win1.41: Global(00c4ccd8) and static initialized(00407960)
/// Default text color
static const struct LH3DColor LH3DColor_ARRAY_00c4ccd8_0 = {
    .b = 0xd0, .g = 0xd0, .r = 0xd0, .a = 0xFF,
};
/// win1.41: Global(00c4ccdc) and static initialized(00407920)
static const struct LH3DColor LH3DColor_ARRAY_00c4ccd8_1 = {
    .b = 0x00, .g = 0x00, .r = 0x00, .a = 0xFF,
};
/// win1.41: Global(00c4cce0) and static initialized(00407900)
static const struct LH3DColor LH3DColor_ARRAY_00c4ccd8_2 = {
    .b = 0x00, .g = 0x00, .r = 0x00, .a = 0xFF,
};
/// win1.41: Global(00c4cce4) and static initialized(004078e0)
/// text hovered color
static const struct LH3DColor LH3DColor_ARRAY_00c4ccd8_3 = {
    .b = 0x20, .g = 0x80, .r = 0xc0, .a = 0xFF,
};
/// win1.41: Global(00c4cce8) and static initialized(004078c0)
/// text selected color
static const struct LH3DColor LH3DColor_ARRAY_00c4ccd8_4 = {
    .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF,
};
/// win1.41: Global(00c4ccf8) and static initialized(00407940)
static const struct LH3DColor LH3DColor_00c4ccf8 = {
    .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF,
};

// win1.41 004079c0 mac 10087550 NeedsBiggerText(void)
bool NeedsBiggerText__Fv(void);
// win1.41 00407a00 mac 1036d670 GetMidTextSize(void)
int GetMidTextSize__Fv(void);
// win1.41 00407a10 mac 10513380 GetSmallTextSize(void)
int GetSmallTextSize__Fv(void);

#endif // BLACKHACK_SETUP_H
