#include "SetupMP3Button.h"

#include <logger.h>

#include "Setup.h"
#include "SetupThing.h"

#include <globals.h>

struct SetupMP3Button* __fastcall ct__14SetupMP3ButtonFiiiiiPwiUi(
    struct SetupMP3Button* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label, int param_8,
    uint32_t param_9) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\", %d, %u) from %p",
      this, id, x, y, width, height, label, param_8, param_9, puEBP[1]);

  ct__10SetupButtonFiiiiiPwi(
      &this->super, edx, id, x, y, width, height, label, param_8);
  this->super.super.vftptr = &vt__SetupMP3Button;
  this->super.super.field_0x1c = 0;
  this->field_0x244 = param_9;
  this->field_0x248 = 1;
  this->color = LH3DColor_00c4ccf8;

  return this;
}

void __fastcall Draw__14SetupMP3ButtonFbb(
    struct SetupMP3Button* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  assert(false); // REMOVEME: raise signal to find where these widgets are used

  if (this->field_0x248 != 0) {
    Draw__11SetupButtonFbb(&this->super, edx, hovered, selected);
  }
  struct LHCoord coord = {
    .x = (this->super.super.rect.p1.x + this->super.super.rect.p0.x) / 2,
    .y = (this->super.super.rect.p1.y + this->super.super.rect.p0.y) / 2,
  };

  if (this->super.pressed || this->super.super.field_0x1c != 0) {
    coord.x -= 8;
    coord.y -= 8;
  }
  else {
    coord.x -= 9;
    coord.y -= 9;
  }

  float u_min = (float)(this->field_0x244 % 4) / 16.0f + 1.0f / 4.0f;
  float v_min = (float)((this->field_0x244 / 4) + 1) / 16.0f;

  const struct LH3DColor* p_color;
  if (hovered || this->super.super.field_0x1c) {
    p_color = &LH3DColor_ARRAY_00c4ccd8_3;
  }
  else {
    p_color = &this->color;
  }

  DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(coord.x,
      coord.y, coord.x + 16, coord.y + 16, u_min, v_min, u_min + 1.0f / 16.0f,
      v_min + 1.0f / 16.0f, *globals()->PTR_00edc368, p_color, 1, -0xa000, 0xa000,
      false, 100.0f);
}

struct SetupMP3Button* __fastcall dt__14SetupMP3ButtonFb(
    struct SetupMP3Button* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}
