#ifndef BLACKHACK_SETUPBOX_H
#define BLACKHACK_SETUPBOX_H

#include <assert.h>
#include <stdint.h>

#include <Zoomer.h>

struct SetupBox;
struct SetupControl;

typedef void (__fastcall vt__SetupBox_field_0x0_t)(struct SetupBox* this, const void* edx, int, int);

struct SetupBoxVtable {
  vt__SetupBox_field_0x0_t * const field_0x0;
};

struct SetupBox {
  struct SetupBoxVtable* vftptr;
  struct Zoomer zoomer_0x4;
  struct Zoomer zoomer_0x34;
  uint32_t field_0x64;
  struct SetupControl* widgets_0x68;
  struct SetupControl* widget_list;
  struct SetupControl* focused_widget;
  struct SetupControl* widget_0x74;
  uint8_t field_0x78;
  uint32_t field_0x7c;
  uint32_t field_0x80;
  float field_0x84;
  uint32_t field_0x88;
  uint32_t field_0x8c;
  uint32_t field_0x90;
  uint32_t field_0x94;
  uint32_t field_0x98;
  uint32_t field_0x9c;
  uint32_t field_0xa0;
  uint32_t field_0xa4;
  uint32_t field_0xa8;
  uint32_t default_text_size;
  void (__stdcall *field_0xb0)(int, struct SetupBox*, struct SetupControl*, int x, int y);
  uint32_t field_0xb4;
  uint32_t field_0xb8;
  struct SetupControl* field_0xbc;
  int field_0xc0;
  float field_0xc4;
  uint32_t field_0xc8;
};
static_assert(sizeof(struct SetupBox) == 0xcc, "Struct is of wrong size");

// win1.41 00407ed0 mac 10075ef0 SetupBox::GetCurrentActiveBox(void)
struct SetupBox* GetCurrentActiveBox__8SetupBoxFv();
// win1.41 408160 mac 1043c330 SetupBox::FindControl(int)
struct SetupControl * __fastcall FindControl__8SetupBoxFi(struct SetupBox *this, const void* edx, int id);
// win1.41 00409140 mac 10598ed0 SetupBox::SetFocusControl(SetupControl *)
void __fastcall SetFocusControl__8SetupBoxFP12SetupControl(struct SetupBox *this, const void* edx, struct SetupControl *widget);
// win1.41 00411090 mac 1047e3e0 SetupBox::SetFocusNext(SetupBox *)
void __fastcall SetFocusNext__8SetupBoxFv(struct SetupBox *this);
// win1.41 00411100 mac 10478e70 SetupBox::SetFocusPrev(SetupBox *)
void __fastcall SetFocusPrev__8SetupBoxFv(struct SetupBox *this);

#endif // BLACKHACK_SETUPBOX_H
