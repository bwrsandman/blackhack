#include "SetupList.h"
#include "SetupThing.h"

#include <logger.h>

#include <GatheringText.h>
#include <LHSys.h>
#include <Memory.h>
#include <Utils.h>

#include "../../../globals/globals.h"
#include "Setup.h"

void __fastcall AutoScroll__9SetupListFb(
    struct SetupList* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  if (param_1 != false || this->selected_index < 0) {
    this->scroll_position = this->field_0x278;
    return;
  }

  float fVar5 = 0.0f;
  int iVar1 = this->super.rect.p1.y - this->item_heights[this->selected_index] -
              this->super.rect.p0.y;
  int iVar2 = this->selected_index;
  for (int i = 0; i < this->num_items; ++i) {
    if (i == iVar2) {
      if (fVar5 < (float)this->scroll_position) {
        iVar2 = (int)fVar5;
        this->scroll_position = iVar2;
        if (iVar2 < 1) {
          iVar2 = 0;
        }
        else {
          if (this->field_0x278 <= iVar2) {
            this->scroll_position = this->field_0x278;
            return;
          }
        }
        this->scroll_position = iVar2;
        return;
      }
      float fVar6 = (float)this->scroll_position + (float)iVar1;
      if (fVar5 > fVar6) {
        fVar5 -= (float)iVar1;
        iVar2 = (int)(fVar5);
        this->scroll_position = iVar2;
        if (iVar2 < 1) {
          iVar2 = 0;
        }
        else {
          if (this->field_0x278 <= iVar2) {
            this->scroll_position = this->field_0x278;
            return;
          }
        }
        this->scroll_position = iVar2;
        return;
      }
    }
    fVar5 += (float)this->item_heights[i];
  }
}

void __fastcall KeyDown__9SetupListFii(struct SetupList* this, const void* edx,
    enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->field_0x284) {
    return;
  }

  if (!this->draw_highlight_box) {
    return;
  }

  switch (key) {
  case LHKEY_HOME:
    if (this->num_items < 1) {
      this->selected_index = -1;
    }
    else {
      this->selected_index = 0;
    }
    if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 != 0) {
      if (CandidateList_GetSelectIdx__Q24slim5TbIMEFv(
              Get__5LHSysFv()->field_0x70c4) == 0) {
        break;
      }
      CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
          Get__5LHSysFv()->field_0x70c4, edx, 0, this->num_items - 1, 0);
      AutoScroll__9SetupListFb(this, edx, false);
    }
    break;

  case LHKEY_UP:
    if (this->selected_index < 1) {
      int iVar3 = this->num_items - 1;
      if (iVar3 < 0 || iVar3 >= this->num_items) {
        this->selected_index = -1;
      }
      else {
        this->selected_index = iVar3;
      }
      if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 != 0 &&
          -1 < iVar3 &&
          CandidateList_GetSelectIdx__Q24slim5TbIMEFv(
              Get__5LHSysFv()->field_0x70c4) != iVar3) {
        CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
            Get__5LHSysFv()->field_0x70c4, edx, 0, this->num_items - 1, iVar3);
        AutoScroll__9SetupListFb(this, edx, false);
      }
    }
    else {
      int iVar3 = this->selected_index - 1;
      if ((iVar3 < 0) || (this->num_items <= iVar3)) {
        this->selected_index = -1;
      }
      else {
        this->selected_index = iVar3;
      }
      if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 != 0 &&
          -1 < iVar3 &&
          CandidateList_GetSelectIdx__Q24slim5TbIMEFv(
              Get__5LHSysFv()->field_0x70c4) != iVar3) {
        CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
            Get__5LHSysFv()->field_0x70c4, edx, 0, this->num_items - 1, iVar3);
        AutoScroll__9SetupListFb(this, edx, false);
      }
    }
    break;

  case LHKEY_END:
    if (this->num_items - 1 < 0 || this->num_items <= this->num_items - 1) {
      this->selected_index = -1;
    }
    else {
      this->selected_index = this->num_items - 1;
    }
    if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 != 0 &&
        -1 < this->num_items - 1 &&
        CandidateList_GetSelectIdx__Q24slim5TbIMEFv(
            Get__5LHSysFv()->field_0x70c4) != this->num_items - 1) {
      CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
          Get__5LHSysFv()->field_0x70c4, edx, 0, this->num_items - 1,
          this->num_items - 1);
      AutoScroll__9SetupListFb(this, edx, false);
    }
    break;

  case LHKEY_DOWN:
    if (this->selected_index < this->num_items - 1) {
      int iVar3 = this->selected_index + 1;
      if (iVar3 < 0 || this->num_items <= iVar3) {
        this->selected_index = -1;
      }
      else {
        this->selected_index = iVar3;
      }
      if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 != 0 &&
          -1 < iVar3 &&
          CandidateList_GetSelectIdx__Q24slim5TbIMEFv(
              Get__5LHSysFv()->field_0x70c4) != iVar3) {
        CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
            Get__5LHSysFv()->field_0x70c4, edx, 0, this->num_items - 1, iVar3);
        AutoScroll__9SetupListFb(this, edx, false);
      }
    }
    else {
      if (this->num_items > 0) {
        // Set to first index
        this->selected_index = 0;
      }
      else {
        // Set to unselected
        this->selected_index = -1;
      }
      if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 != 0) {
        if (CandidateList_GetSelectIdx__Q24slim5TbIMEFv(
                Get__5LHSysFv()->field_0x70c4) == 0) {
          break;
        }
        CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
            Get__5LHSysFv()->field_0x70c4, edx, 0, this->num_items - 1, 0);
        AutoScroll__9SetupListFb(this, edx, false);
      }
    }
    break;

  default:
    this->field_0x24c = this->selected_index;
    this->field_0x280 = this->scroll_position;
    return;
  }

  AutoScroll__9SetupListFb(this, edx, false);
  this->field_0x24c = this->selected_index;
  this->field_0x280 = this->scroll_position;
}

void __fastcall Drag__9SetupListFii(
    struct SetupList* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);

  if (this->super.field_0x4 != 0) {
    return;
  }

  if (!this->field_0x285 &&
      !this->super.vftptr->HitTest(&this->super, edx, x, y)) {
    return;
  }

  if (this->field_0x285) {
    int height = this->super.rect.p1.y - this->super.rect.p0.y;
    int iVar4 =
        (int)(((float)this->field_0x280 / (float)this->scroll_distance) *
                  (float)height +
              (float)this->super.rect.p0.y);
    if ((this->drag_start.y < iVar4) ||
        ((int)(((float)((this->super.rect.p1.y - this->super.rect.p0.y) - 8 +
                        this->field_0x280) /
                   (float)this->scroll_distance) *
                   (float)height +
               (float)this->super.rect.p0.y) <= this->drag_start.y)) {
      int width = this->super.rect.p1.x - this->super.rect.p0.x - 20;
      if (width < 0) {
        width = 0;
      }
      if (this->drag_start.y < iVar4) {
        width = -width;
      }
      this->scroll_position = this->field_0x280 + width;
    }
    else {
      this->scroll_position = (int)(((float)(y - this->drag_start.y) *
                                        (float)this->scroll_distance) /
                                        (float)height +
                                    (float)this->field_0x280);
    }
    if (this->scroll_position < 1) {
      this->scroll_position = 0;
    }
    else if (this->field_0x278 <= this->scroll_position) {
      this->scroll_position = this->field_0x278;
    }
  }
  else {
    int iVar7 = this->super.rect.p0.y - this->scroll_position;
    int i = 0;
    this->selected_index = -1;
    for (i = 0; i < this->num_items; ++i) {
      if (y >= iVar7 && y < this->item_heights[i] + iVar7)
        break;
      iVar7 += this->item_heights[i];
    }
    if (i < this->num_items) {
      if (i < 0) {
        this->selected_index = -1;
      }
      else {
        this->selected_index = i;
      }
      if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 &&
          i > -1 &&
          CandidateList_GetSelectIdx__Q24slim5TbIMEFv(
              Get__5LHSysFv()->field_0x70c4) != i) {
        CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
            Get__5LHSysFv()->field_0x70c4, edx, 0, this->num_items - 1, i);
        AutoScroll__9SetupListFb(this, edx, false);
      }
    }
  }
}

void __fastcall MouseDown__9SetupListFiib(
    struct SetupList* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  if (this->super.field_0x4 != 0 || !param_3) {
    return;
  }

  this->field_0x24c = this->selected_index;
  this->field_0x280 = this->scroll_position;
  this->drag_start.x = x;
  this->drag_start.y = y;
  if (this->super.rect.p1.x - this->scrollback_width < x &&
      this->show_scrollbar) {
    this->field_0x285 = true;
  }
  else {
    this->field_0x285 = false;
  }
  if (this->field_0x285) {
    this->selected_index = -1;
  }
  this->super.vftptr->Drag(&this->super, edx, x, y);
}

void __fastcall MouseUp__9SetupListFiib(
    struct SetupList* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  if (this->super.field_0x4 != 0) {
    return;
  }

  if (this->field_0x285) {
    if (param_3) {
      this->selected_index = this->field_0x24c;
    }
    this->field_0x285 = false;
    return;
  }
  else if (param_3) {
    this->field_0x24c = this->selected_index;
    this->field_0x280 = this->scroll_position;
  }
  else {
    this->selected_index = this->field_0x24c;
  }
}

void __fastcall Click__9SetupListFii(
    struct SetupList* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);
}

struct SetupList* __fastcall ct__9SetupListFiiiii(struct SetupList* this,
    const void* edx, int id, int x, int y, int width, int height) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d) from %p",
      this, id, x, y, width, height, puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  ct__12SetupControlFiiiiiPw(
      &this->super, edx, id, x, y, width, height, Get__10SetupThingFv()->WCHAR_00c4cd30);

  this->super.vftptr = &vt__SetupList.super;
  this->field_0x23c = false;
  this->scrollback_width = 24;
  this->field_0x244 = false;
  this->selected_index = -1;
  this->field_0x24c = -1;
  this->num_items = 0;
  this->field_0x254 = 0;
  this->item_labels = NULL;
  this->item_heights = NULL;
  this->field_0x260 = NULL;
  this->field_0x264 = NULL;
  this->field_0x268 = NULL;
  this->ListBoxDraw = NULL;
  this->scroll_distance = 0;
  this->show_scrollbar = false;
  this->field_0x278 = 0;
  this->scroll_position = 0;
  this->field_0x280 = 0;
  this->field_0x284 = false;
  this->use_color_background = false;
  this->draw_highlight_box = true;
  this->box_outline_color = white;
  this->selection_color = white;
  this->field_0x29c = 0;

  return this;
}

struct SetupList* __fastcall dt__9SetupListFb(
    struct SetupList* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  this->super.vftptr = &vt__SetupList.super;
  __dl__FPv(this->field_0x268);
  __dl__FPv(this->ListBoxDraw);
  __dl__FPv(this->field_0x264);
  __dl__FPv(this->item_heights);
  __dl__FPv(this->item_labels);
  __dl__FPv(this->field_0x260);

  dt__12SetupControlFb(&this->super, edx, param_1);

  return this;
}

bool __fastcall IsSelected__9SetupListFi(
    struct SetupList* this, const void* edx, int index) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  bool result = index == this->selected_index;

  LOG_TRACE(__FUNCTION__ "(%p, index=%d) -> %s from %p", this, index,
      result ? "true" : "false", puEBP[1]);

  return result;
}

void __fastcall Draw__9SetupListFbb(
    struct SetupList* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor clear_color = {
    .b = 0x00, .g = 0x00, .r = 0x00, .a = 0x00
  };
  const struct LH3DColor black = { .b = 0x00, .g = 0x00, .r = 0x00, .a = 0xFF };
  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  int x_max = this->super.rect.p1.x;
  if (this->show_scrollbar) {
    x_max -= 2 + (int)this->scrollback_width;
  }

  {
    uint32_t style = this->use_color_background ? 0 : 1;
    if (this->super.field_0x4 != 0 && Get__10SetupThingFv()->DAT_00c4cd00 != 0) {
      style += 0x10;
    }

    DrawBevBox__10SetupThingFiiiiiiiUl(this->super.rect.p0.x,
        this->super.rect.p0.y, x_max, this->super.rect.p1.y, style, 0x10,
        0xffffffff, this->box_outline_color);
  }

  this->selection_rect.p0 = this->super.rect.p0;
  this->selection_rect.p1 = this->super.rect.p0;

  int current_y = this->super.rect.p0.y - this->scroll_position;

  for (int i = 0; i < this->num_items; ++i) {
    int y_1 = current_y;
    int y_2 = current_y + this->item_heights[i];

    int local_18 = 1;
    if (!this->field_0x23c && this->ListBoxDraw[i] != NULL) {
      local_18 = this->ListBoxDraw[i](this, i, this->super.rect.p0.x,
          this->super.rect.p0.y, x_max, this->super.rect.p1.y, current_y, y_2);
    }

    bool is_selected =
        ((struct vt__SetupList_t*)this->super.vftptr)->IsSelected(this, edx, i);

    if (is_selected && this->draw_highlight_box) {
      y_1 = this->super.rect.p0.y;
      if (y_1 < current_y) {
        y_1 = current_y;
        if (y_1 > this->super.rect.p1.y) {
          y_1 = this->super.rect.p1.y;
        }
      }

      y_2 = clampi(y_2, this->super.rect.p0.y, this->super.rect.p1.y);

      this->selection_rect.p0.x = this->super.rect.p0.x;
      this->selection_rect.p0.y = y_1;

      this->selection_rect.p1.x = x_max;
      this->selection_rect.p1.y = y_2;

      if (y_1 != y_2) {
        DrawBevBox__10SetupThingFiiiiiiiUl(this->super.rect.p0.x, y_1, x_max,
            y_2, this->use_color_background ? 1 : 0, 0x10, 0xffffffff,
            this->selection_color);
      }
      if (this->super.rect.p0.y <= y_2) {
        if (y_1 <= this->super.rect.p1.y && local_18 != 0) {
          DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
              this->super.rect.p0.x + 4, this->super.rect.p0.y, x_max - 4,
              this->super.rect.p1.y, current_y + 2, this->field_0x244,
              this->item_labels[i], GetTextSize__12SetupControlFv(&this->super),
              &LH3DColor_00c4ccf8, false, false);
        }
      }
    }
    else {
      struct LHCoord mouse;
      ct__7LHCoordFRC7LHCoord(&mouse, edx, &Get__5LHSysFv()->mouse.coord_0xbc);
      unadjust__10SetupThingFRiRi(&mouse.x, &mouse.y);
      bool bVar1 = hovered && mouse.y >= current_y &&
                   mouse.y < (current_y + this->item_heights[i]);
      if (y_2 >= this->super.rect.p0.y && current_y <= this->super.rect.p1.y &&
          local_18 != 0) {

        struct LH3DColor color =
            i < this->num_items ? this->field_0x268[i] : clear_color;
        const struct LH3DColor* p_color;
        if (color.a == 0) {
          if (*(uint32_t*)&color != *(uint32_t*)&clear_color) {
            color.a = 0xFF;
          }
          if (bVar1) {
            p_color = &LH3DColor_ARRAY_00c4ccd8_3;
          }
          else if (*(uint32_t*)&color != *(uint32_t*)&clear_color) {
            p_color = &color;
          }
          else if (selected) {
            p_color = &LH3DColor_00c4ccf8;
          }
          else {
            p_color = &LH3DColor_ARRAY_00c4ccd8_0;
          }
        }
        else {
          color.a = 0xFF;
          if (current_y != y_2) {
            int uVar8 = this->super.rect.p0.y + 2;
            int uVar7 = this->super.rect.p1.y - 2;

            if (uVar7 >= y_2) {
              uVar7 = y_2;
            }
            if (uVar8 < current_y) {
              uVar8 = current_y;
            }
            DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(this->super.rect.p0.x + 2,
                uVar8, x_max - 2, uVar7, color, color, color, color, 1, 1);
          }
          p_color = &black;
        }
        DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
            this->super.rect.p0.x + 4, this->super.rect.p0.y, x_max - 4,
            this->super.rect.p1.y, current_y + 2, this->field_0x244,
            this->item_labels[i], GetTextSize__12SetupControlFv(&this->super),
            p_color, false, false);
      }
    }
    if (this->field_0x23c) {
      if (this->super.rect.p0.y <= y_2) {
        if (y_1 <= this->super.rect.p1.y && this->ListBoxDraw[i] != NULL) {
          this->ListBoxDraw[i](this, i, this->super.rect.p0.x,
              this->super.rect.p0.y, x_max, this->super.rect.p1.y, y_1, y_2);
        }
      }
    }
    current_y += this->item_heights[i];
  }

  if (this->show_scrollbar) {
    int height = this->super.rect.p1.y - this->super.rect.p0.y;
    float scale_factor = (float)(height - 6) / (float)this->scroll_distance;
    int scrollbar_height = height - 8;
    int scrollbar_min_y = (int)(scale_factor * (float)this->scroll_position);
    int scrollbar_max_y =
        (int)(scale_factor * (float)(this->scroll_position + scrollbar_height));

    x_max += 2;

    struct SetupRect rect = {
        .p0 = { .x = x_max, .y = this->super.rect.p0.y, },
        .p1 = { .x = x_max + this->scrollback_width, .y = this->super.rect.p1.y, },
    };

    DrawBevBox__10SetupThingFiiiiiiiUl(rect.p0.x, rect.p0.y, rect.p1.x,
        rect.p1.y, (uint32_t) !this->use_color_background, 0x10, 0xffffffff,
        white);

    DrawBevBox__10SetupThingFiiiiiiiUl(rect.p0.x + 3,
        rect.p0.y + 3 + scrollbar_min_y, rect.p1.x - 3,
        rect.p0.y + 3 + scrollbar_max_y, (uint32_t)this->use_color_background,
        0x10, 0xffffffff, white);
  }
}

void __fastcall UpdateHeights__9SetupListFv(struct SetupList* this) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p) from %p", this, puEBP[1]);

  this->show_scrollbar = false;
  this->field_0x278 = 0;
  this->scroll_distance = 0;

  int text_size = GetTextSize__12SetupControlFv(&this->super);
  for (int i = 0; i < this->num_items; ++i) {
    float fVar9 = DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
        Get__10SetupThingFv()->font, NULL, this->item_labels[i],
        (float)(this->super.rect.p0.x + 4), (float)this->super.rect.p0.y,
        (float)this->super.rect.p0.y, (float)(this->super.rect.p1.x - 4),
        (float)this->super.rect.p1.y, (float)this->super.rect.p1.y,
        (float)this->super.rect.p0.y, 5.0f, (float)text_size,
        &LH3DColor_ARRAY_00c4ccd8_2, 0, 0, 1);

    this->item_heights[i] = (int)(fVar9 + 6.0f);
    this->scroll_distance = this->scroll_distance + this->item_heights[i];
  }

  if (this->super.rect.p1.y - this->super.rect.p0.y - 8 <
      this->scroll_distance) {
    this->show_scrollbar = true;
    this->scroll_distance = 0;
    for (int i = 0; i < this->num_items; ++i) {
      float fVar9 = DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
          Get__10SetupThingFv()->font, NULL, this->item_labels[i],
          (float)((this->super).rect.p0.x + 4), (float)this->super.rect.p0.y,
          (float)this->super.rect.p0.y,
          (float)((this->super.rect.p1.x - this->scrollback_width) + -6),
          (float)this->super.rect.p1.y, (float)this->super.rect.p1.y,
          (float)this->super.rect.p0.y, 5.0f, (float)text_size,
          &LH3DColor_ARRAY_00c4ccd8_2, 0, 0, 1);
      this->item_heights[i] = (int)(fVar9 + 6.0f);
      this->scroll_distance = this->scroll_distance + this->item_heights[i];
    }
    this->field_0x278 = this->scroll_distance - this->super.rect.p1.y + 8 +
                        this->super.rect.p0.y;
  }
}

void __fastcall DeleteString__9SetupListFi(
    struct SetupList* this, const void* edx, int index) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, index=%d) from %p", this, index, puEBP[1]);

  if (index < 0 || index >= this->num_items)
    return;

  memmove(&this->item_labels[index], &this->item_labels[index + 1],
      (this->num_items - 1 - index) * sizeof(this->item_labels[0]));
  memmove(&this->item_heights[index], &this->item_heights[index + 1],
      (this->num_items - 1 - index) * sizeof(this->item_heights[0]));
  memmove(&this->field_0x264[index], &this->field_0x264[index + 1],
      (this->num_items - 1 - index) * sizeof(this->field_0x264[0]));
  memmove(&this->ListBoxDraw[index], &this->ListBoxDraw[index + 1],
      (this->num_items - 1 - index) * sizeof(this->ListBoxDraw[0]));
  memmove(&this->field_0x268[index], &this->field_0x268[index + 1],
      (this->num_items - 1 - index) * sizeof(this->field_0x268[0]));
  memmove(&this->field_0x260[index], &this->field_0x260[index + 1],
      (this->num_items - 1 - index) * sizeof(this->field_0x260[0]));

  SetNum__9SetupListFi(this, edx, this->num_items - 1);
}

void __fastcall SetNum__9SetupListFi(
    struct SetupList* this, const void* edx, int num) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, num=%d) from %p", this, num, puEBP[1]);

  if (num < 0) {
    num = 0;
  }
  if (num < this->field_0x254 / 2 || num > this->field_0x254) {
    this->field_0x254 = num + 16;

    wchar_t(*item_labels)[256] = __nw__FUl(
        this->field_0x254 * sizeof(this->item_labels[0]), __FILE__, __LINE__);
    int* item_heights = __nw__FUl(
        this->field_0x254 * sizeof(this->item_heights[0]), __FILE__, __LINE__);
    uint32_t* field_0x260 = __nw__FUl(
        this->field_0x254 * sizeof(this->field_0x260[0]), __FILE__, __LINE__);
    uint32_t* field_0x264 = __nw__FUl(
        this->field_0x254 * sizeof(this->field_0x264[0]), __FILE__, __LINE__);
    struct LH3DColor* field_0x268 = __nw__FUl(
        this->field_0x254 * sizeof(this->field_0x268[0]), __FILE__, __LINE__);
    SetupList__ListBoxDraw_t** ListBoxDraw = __nw__FUl(
        this->field_0x254 * sizeof(this->ListBoxDraw[0]), __FILE__, __LINE__);

    memset(item_labels, 0, this->field_0x254 * sizeof(this->item_labels[0]));
    memset(item_heights, 0, this->field_0x254 * sizeof(this->item_heights[0]));
    memset(field_0x260, 0, this->field_0x254 * sizeof(this->field_0x260[0]));
    memset(field_0x264, 0, this->field_0x254 * sizeof(this->field_0x264[0]));
    memset(field_0x268, 0, this->field_0x254 * sizeof(this->field_0x268[0]));
    memset(ListBoxDraw, 0, this->field_0x254 * sizeof(this->ListBoxDraw[0]));

    memcpy(item_labels, this->item_labels,
        min(this->num_items, num) * sizeof(this->item_labels[0]));
    memcpy(item_heights, this->item_heights,
        min(this->num_items, num) * sizeof(this->item_heights[0]));
    memcpy(field_0x264, this->field_0x264,
        min(this->num_items, num) * sizeof(this->field_0x264[0]));
    memcpy(ListBoxDraw, this->ListBoxDraw,
        min(this->num_items, num) * sizeof(this->ListBoxDraw[0]));
    memcpy(field_0x268, this->field_0x268,
        min(this->num_items, num) * sizeof(this->field_0x268[0]));
    memcpy(field_0x260, this->field_0x260,
        min(this->num_items, num) * sizeof(this->field_0x260[0]));

    __dl__FPv(this->item_heights);
    __dl__FPv(this->item_labels);
    __dl__FPv(this->field_0x264);
    __dl__FPv(this->ListBoxDraw);
    __dl__FPv(this->field_0x268);
    __dl__FPv(this->field_0x260);

    this->field_0x264 = field_0x264;
    this->ListBoxDraw = ListBoxDraw;
    this->field_0x268 = field_0x268;
    this->item_labels = item_labels;
    this->item_heights = item_heights;
    this->field_0x260 = field_0x260;
  }
  else if (this->num_items < num) {
    memset(
        &this->item_labels[this->num_items], 0, sizeof(this->item_labels[0]));
    memset(
        &this->item_heights[this->num_items], 0, sizeof(this->item_heights[0]));
    memset(
        &this->field_0x264[this->num_items], 0, sizeof(this->field_0x264[0]));
    memset(
        &this->ListBoxDraw[this->num_items], 0, sizeof(this->ListBoxDraw[0]));
    memset(
        &this->field_0x268[this->num_items], 0, sizeof(this->field_0x268[0]));
    memset(
        &this->field_0x260[this->num_items], 0, sizeof(this->field_0x260[0]));
  }

  this->num_items = num;
  if (this->num_items <= this->selected_index) {
    this->selected_index = -1;
  }
  UpdateHeights__9SetupListFv(this);
}
