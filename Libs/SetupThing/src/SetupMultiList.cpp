#include "SetupMultiList.h"

#include <logger.h>

#include <Memory.h>

struct SetupMultiList* __fastcall ct__14SetupMultiListFiiiiii(
    struct SetupMultiList* this, const void* edx, int id, int x, int y,
    int width, int height, int size) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "size=%d) from %p",
      this, id, x, y, width, height, size, puEBP[1]);

  ct__9SetupListFiiiii(&this->super, edx, id, x, y, width, height);

  this->super.super.vftptr = &vt__SetupMultiList.super;
  this->list = __nw__FUl((size_t)size, __FILE__, __LINE__);
  for (int i = 0; i < size; ++i) {
    this->list[i] = false;
  }
  this->field_0x2b4 = 0;
  this->size = size;

  return this;
}

struct SetupMultiList* __fastcall dt__14SetupMultiListFb(
    struct SetupMultiList* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  this->super.super.vftptr = &vt__SetupMultiList.super;
  __dl__FPv(this->list);

  dt__9SetupListFb(&this->super, edx, param_1);

  return this;
}

bool __fastcall IsSelected__14SetupMultiListFi(
    struct SetupMultiList* this, const void* edx, int index) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  bool result = index > 0xFF || index < 0;
  if (index > -1 && index <= this->size) {
    result |= this->list[index];
  }

  LOG_TRACE(__FUNCTION__ "(%p, index=%d) -> %s from %p", this, index,
      result ? "true" : "false", puEBP[1]);

  return result;
}

void __fastcall Click__14SetupMultiListFii(
    struct SetupMultiList* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);

  int iVar3 = this->super.super.rect.p0.y - this->super.scroll_position;

  if (this->super.field_0x285) {
    return;
  }

  int i = 0;
  for (i = 0; i < this->super.num_items; ++i) {
    if (y >= iVar3 && y < this->super.item_heights[i] + iVar3) {
      break;
    }
    iVar3 += this->super.item_heights[i];
  }

  if (i < this->super.num_items) {
    this->list[i] = !this->list[i];
    if (this->list[i]) {
      ++this->field_0x2b4;
    }
    else {
      --this->field_0x2b4;
    }
  }
}
