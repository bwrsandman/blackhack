#ifndef BLACKHACK_SETUPMP3BUTTON_H
#define BLACKHACK_SETUPMP3BUTTON_H

#include "SetupButton.h"

#include <LH3DColor.h>

/// Since the constructor is inlined, it is difficult to override the virtual
/// table. One solution is to override the original virtual table pointers

struct SetupMP3Button {
  struct SetupButton super;
  uint32_t field_0x244;
  uint32_t field_0x248;
  struct LH3DColor color;
};
static_assert(sizeof(struct SetupMP3Button) == 0x250, "Struct is of wrong size");

// inlined SetupMP3Button::SetupMP3Button(int, int, int, int, int, wchar_t *, int, unsigned int)
struct SetupMP3Button* __fastcall ct__14SetupMP3ButtonFiiiiiPwiUi(struct SetupMP3Button* this, const void* edx, int id, int x, int y, int width, int height, const wchar_t* label, int param_8, uint32_t param_9);
// win1.41 0040cda0 mac 10426080 SetupMP3Button::Draw(bool, bool)
void __fastcall Draw__14SetupMP3ButtonFbb(struct SetupMP3Button* this, const void* edx, bool hovered, bool selected);
// win1.41 00571f30 mac 103547d0 SetupMP3Button::~SetupMP3Button(void)
struct SetupMP3Button* __fastcall dt__14SetupMP3ButtonFb(struct SetupMP3Button* this, const void* edx, bool param_1);

// win1.41 00900060 mac none SetupMP3Button virtual table
static struct vt__SetupControl_t vt__SetupMP3Button = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__14SetupMP3ButtonFbb,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__10SetupButtonFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__11SetupButtonFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__14SetupMP3ButtonFb,
};

#endif // BLACKHACK_SETUPMP3BUTTON_H
