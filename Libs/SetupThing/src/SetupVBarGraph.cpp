#include "SetupVBarGraph.h"

#include <stdlib.h>
#include <math.h>

#include <logger.h>

#include <LHSys.h>
#include <LH3DTech.h>
#include <Memory.h>
#include <Utils.h>

#include "Setup.h"
#include "SetupThing.h"

struct VBarData* __fastcall ct__8VBarDataFRC8VBarData(
    struct VBarData* this, const void* edx, const struct VBarData* bar) {
  ct__9LH3DColorFRC9LH3DColor(&this->color, edx, &bar->color);
  this->value = bar->value;
  return this;
}

struct VBarData* __fastcall as__8VBarDataFRC8VBarData(
    struct VBarData* this, const void* edx, const struct VBarData* bar) {
  as__9LH3DColorFRC9LH3DColor(&this->color, edx, &bar->color);
  this->value = bar->value;
  return this;
}

struct VBarDataListNode* __fastcall ct__VBarDataListNode(
    struct VBarDataListNode* this, const void* edx, struct VBarData* payload,
    struct VBarDataListNode* next) {
  this->payload = payload;
  this->next = next;
  return this;
}

struct VBarDataListNode* __fastcall GetStart__VBarDataList(
    const struct VBarDataList* this) {
  return this->head;
}

struct VBarDataListNode* __fastcall GetNodeAtPosition__VBarDataList(
    const struct VBarDataList* this, const void* edx, int index) {
  if (this->count == 0 || this->head == NULL || this->count <= index) {
    return NULL;
  }

  if (index < 0) {
    return this->head;
  }

  struct VBarDataListNode* walker = GetStart__VBarDataList(this);
  for (int i = 0; i < index; ++i) {
    walker = walker->next;
  }

  return walker;
}

struct VBarDataListNode* __fastcall GetLastNode__VBarDataList(
    const struct VBarDataList* this) {
  struct VBarDataListNode* result = NULL;

  for (struct VBarDataListNode* node = GetStart__VBarDataList(this);
       node != NULL; node = node->next) {
    result = node;
  }

  return result;
}

void __fastcall PushBack__VBarDataList(
    struct VBarDataList* this, const void* edx, struct VBarData* payload) {
  struct VBarDataListNode* node = malloc(sizeof(struct VBarDataListNode));
  assert(node);
  ct__VBarDataListNode(node, edx, payload, NULL);

  struct VBarDataListNode* tail = GetLastNode__VBarDataList(this);
  if (tail) {
    tail->next = node;
  }
  else {
    this->head = node;
  }

  ++this->count;
}

void __fastcall Remove__VBarDataList(struct VBarDataList* this, const void* edx,
    struct VBarData* bar, bool only_one) {
  struct VBarDataListNode* walker = GetStart__VBarDataList(this);
  struct VBarDataListNode* node = NULL;

  bool run_once = false;

  while (!only_one || !run_once) {
    struct VBarDataListNode* prev;
    while (node == NULL || node->payload != bar) {
      prev = node;
      node = walker;
      if (node == NULL) {
        return;
      }
      walker = node->next;
    }
    if (node == this->head) {
      this->head = node->next;
    }
    else {
      prev->next = node->next;
    }
    --this->count;
    __dl__FPv(node);
    node = prev;
    run_once = true;
  }
}

void __fastcall Draw__14SetupVBarGraphFbb(
    struct SetupVBarGraph* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  int text_size = GetTextSize__12SetupControlFv(&this->super.super);
  float range = this->max_point - this->min_point;
  int height = this->super.super.rect.p1.y - this->super.super.rect.p0.y;

  DrawBevBox__10SetupThingFiiiiiiiUl(this->super.super.rect.p0.x,
      this->super.super.rect.p0.y, this->super.super.rect.p1.x,
      this->super.super.rect.p1.y, 1, 0x10, 0xffffffff, white);

  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
      this->super.super.rect.p0.x + 1, this->super.super.rect.p1.y + 1,
      this->super.super.rect.p1.x + 1,
      this->super.super.rect.p1.y + 1 + text_size,
      this->super.super.rect.p1.y + 1, true, this->super.super.label, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, true, false);

  const struct LH3DColor* p_color;
  if (hovered || (selected && this->super.super.label[0] == L'\0')) {
    p_color = &LH3DColor_ARRAY_00c4ccd8_3;
  }
  else {
    p_color = &LH3DColor_00c4ccf8;
  }
  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(this->super.super.rect.p0.x,
      this->super.super.rect.p1.y, this->super.super.rect.p1.x,
      this->super.super.rect.p1.y + text_size, this->super.super.rect.p1.y,
      true, this->super.super.label, text_size, p_color, true, false);

  this->field_0x244.m[5] += GetDeltaTime__8LH3DTechFv() / 1000.0f;

  if (this->field_0x244.m[5] < this->field_0x244.m[6]) {
    float t2_2 = this->field_0x244.m[5] * this->field_0x244.m[5] * 0.5f;
    float t3_6 = this->field_0x244.m[5] * t2_2 / 3.0f;
    float t4_24 = t2_2 * t2_2 / 6.0f;
    this->field_0x244.m[3] = this->field_0x244.m[5] * this->field_0x244.m[9] +
                             this->field_0x244.m[8] +
                             this->field_0x244.m[10] * t2_2 +
                             this->field_0x244.m[11] * t3_6;
    this->field_0x244.m[0] =
        this->field_0x244.m[5] * this->field_0x244.m[8] +
        this->field_0x244.m[7] + this->field_0x244.m[9] * t2_2 +
        this->field_0x244.m[10] * t3_6 + this->field_0x244.m[11] * t4_24;
  }
  else {
    this->field_0x244.m[0] = this->field_0x244.m[1];
    this->field_0x244.m[3] = this->field_0x244.m[2];
    this->field_0x244.m[4] = 0.0f;
    this->field_0x244.m[5] = this->field_0x244.m[6];
  }

  float x_interval =
      (float)(this->super.super.rect.p1.x - this->super.super.rect.p0.x - 4) /
      (float)this->bar_data_list.count;

  struct VBarDataListNode* walker =
      GetStart__VBarDataList(&this->bar_data_list);

  float current_x = (float)(this->super.super.rect.p0.x + 2);
  if (walker != NULL) {
    struct VBarData* payload = walker->payload;
    while (walker != NULL && payload != NULL) {

      struct LH3DColor color_max = {
        .b = payload->color.b,
        .g = payload->color.g,
        .r = payload->color.r,
        .a = 0xFF,
      };
      struct LH3DColor color_min = {
        .b = payload->color.b,
        .g = payload->color.g,
        .r = payload->color.r,
        .a = 0x3F,
      };

      int y_min =
          (int)((float)(this->super.super.rect.p1.y - 2) -
                (walker->payload->value - this->min_point) *
                    this->field_0x244.m[0] / range * (float)(height - 4));
      if (y_min <= this->super.super.rect.p0.y + 2) {
        y_min = this->super.super.rect.p0.y + 2;
      }
      else if (y_min >= this->super.super.rect.p1.y - 2) {
        y_min = this->super.super.rect.p1.y - 2;
      }


      int x_min = (int)(current_x + 2.0f);
      int x_max = (int)(current_x + x_interval - 2.0f);
      int y_max = (int)((float)(this->super.super.rect.p1.y - 2)) - 2;

      DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(x_min, y_min, x_max, y_max,
          color_max, color_max, color_min, color_min, 0, 1);

      current_x = (float)(this->super.super.rect.p1.y - 2);

      payload = walker->payload;
      walker = GetStart__VBarDataList(&this->bar_data_list);
      while (walker != NULL) {
        if (walker->payload == payload) {
          walker = walker->next;
          break;
        }
        walker = walker->next;
      }
      current_x += x_interval;
    }
  }

  // Get tick spacing
  float spacing = 0.25f;
  while (range / spacing > (float)(height < 0 ? height + 1 : height) / 4) {
    spacing *= 2.0f;
  }

  struct LH3DColor color = white;

  // Draw tick marks
  int min_ticks = (int)(this->min_point / spacing);
  float tick_position = (float)min_ticks / spacing;
  for (int i = 0; tick_position + i * (spacing / 2) < this->max_point; ++i) {
    color.a = (uint8_t)(((i + min_ticks % 2) % 8u) ? 0x10 : 0x50);
    int y_value = (int)((float)(this->super.super.rect.p1.y - 2) -
                        (tick_position + i * (spacing / 2) - this->min_point) /
                            range * (float)(height - 4));
    int y_min = clampi(y_value, this->super.super.rect.p0.y + 2,
        this->super.super.rect.p1.y - 2);
    DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(this->super.super.rect.p0.x + 2,
        y_min, this->super.super.rect.p1.x - 2, y_min + 1, color, color, color,
        color, 0, 1);
  }

  if (!hovered) {
    return;
  }

  // Hovered value preview
  struct LHCoord mouse;
  ct__7LHCoordFRC7LHCoord(&mouse, edx, &Get__5LHSysFv()->mouse.coord_0xbc);
  unadjust__10SetupThingFRiRi(&mouse.x, &mouse.y);

  float value = saturatef((float)(this->super.super.rect.p1.y - mouse.y - 2) /
                          (float)(height - 4)) *
                    range +
                this->min_point;

  wchar_t value_preview[0x100];
  if (fabsf(range) > 10.0f) {
    swprintf_s(value_preview, _countof(value_preview), L"%d", (int)value);
  }
  else {
    swprintf_s(value_preview, _countof(value_preview), L"%0.1f", value);
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
      this->super.super.rect.p0.x - 1, mouse.y - text_size / 2 + 1, 100,
      TEXTJUSTIFY_RIGHT, value_preview, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, 0);

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
      this->super.super.rect.p0.x - 2, mouse.y - text_size / 2, 100,
      TEXTJUSTIFY_RIGHT, value_preview, text_size, &LH3DColor_00c4ccf8,
      0);
}

struct SetupVBarGraph* __fastcall ct__14SetupVBarGraphFiiiiiPw(
    struct SetupVBarGraph* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\") from %p",
      this, id, x, y, width, height, label, puEBP[1]);

  ct__10SetupButtonFiiiiiPwi(
      &this->super, edx, id, x, y, width, height, label, 0);
  this->super.super.vftptr = &vt__SetupVBarGraph.super;
  this->super.super.text_size = GetSmallTextSize__Fv();
  this->super.pressed = false;

  this->bar_data_list.head = NULL;
  this->bar_data_list.count = 0;
  this->max_point = 0.0f;
  this->min_point = 0.0f;
  Reset__14SetupVBarGraphFv(this);

  return this;
}

void __fastcall KeyDown__14SetupVBarGraphFii(struct SetupVBarGraph* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.super.setup_box != NULL) {
    LOG_DEBUG("%p", this->super.super.setup_box->vftptr->field_0x0);
    this->super.super.setup_box->vftptr->field_0x0(
        this->super.super.setup_box, edx, key, mod);
  }
}

struct SetupVBarGraph* __fastcall dt__14SetupVBarGraphFb(
    struct SetupVBarGraph* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}

void __fastcall Reset__14SetupVBarGraphFv(struct SetupVBarGraph* this) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p) from 0x%p", this, puEBP[1]);

  bool run_once = false;
  while (!run_once) {
    struct VBarDataListNode* walker =
        GetStart__VBarDataList(&this->bar_data_list);
    if (walker == NULL) {
      break;
    }
    struct VBarData* payload = walker->payload;
    struct VBarDataListNode* prev = NULL;
    while (walker != NULL) {
      struct VBarDataListNode* next = walker->next;
      struct VBarDataListNode* node = walker;
      if (walker->payload == payload) {
        if (walker == this->bar_data_list.head) {
          this->bar_data_list.head = next;
        }
        else {
          prev->next = next;
        }
        --this->bar_data_list.count;
        __dl__FPv(walker);
        node = prev;
      }
      walker = next;
      prev = node;
    }
    __dl__FPv(payload);
  }

  this->field_0x244.m[0] = 0.0f;
  this->field_0x244.m[1] = 1.0f;
  this->field_0x244.m[2] = 0.0f;
  this->field_0x244.m[3] = 0.0f;
  this->field_0x244.m[4] = 0.0f;
  this->field_0x244.m[5] = 0.0f;
  this->field_0x244.m[6] = 0.5f;
  this->field_0x244.m[7] = 0.0f;
  this->field_0x244.m[8] = 0.0f;
  this->field_0x244.m[9] = 0.0f;
  this->field_0x244.m[10] = 0.0f;
  this->field_0x244.m[11] = 0.0f;

  struct LHMatrix local_60;
  local_60.m[0] = 1.0f / 384.0f;
  local_60.m[1] = 1.0f / 48.0f;
  local_60.m[2] = 1.0f / 8.0f;
  local_60.m[3] = 1.0f / 48.0f;
  local_60.m[4] = 1.0f / 8.0f;
  local_60.m[5] = 1.0f / 2.0f;
  local_60.m[6] = 1.0f / 8.0f;
  local_60.m[7] = 1.0f / 2.0f;
  local_60.m[8] = 1.0f;
  local_60.m[9] = 0.0f;
  local_60.m[10] = 0.0f;
  local_60.m[11] = 0.0f;

  struct LHMatrix local_30;
  SetInverse__8LHMatrixFRC8LHMatrix(&local_30, NULL, &local_60);

  float fVar1 = this->field_0x244.m[1] - this->field_0x244.m[7] -
                this->field_0x244.m[8] * this->field_0x244.m[6];
  float fVar2 = this->field_0x244.m[2] - this->field_0x244.m[8];

  this->field_0x244.m[9] =
      local_30.m[2] * fVar1 + local_30.m[5] * fVar2 + local_30.m[11];
  this->field_0x244.m[10] = this->field_0x244.m[2] * local_30.m[4] -
                            this->field_0x244.m[8] + local_30.m[1] * fVar1 +
                            local_30.m[10];
  this->field_0x244.m[11] =
      local_30.m[0] * fVar1 + local_30.m[3] * fVar2 + local_30.m[9];

  this->min_point = 0.0f;
  this->max_point = 0.0f;
}

void __fastcall SetScale__14SetupVBarGraphFf(
    struct SetupVBarGraph* this, const void* edx, float scale) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %f) from 0x%p", this, scale, puEBP[1]);

  if (scale <= 0.0f) {
    this->max_point = 0.0f;
    struct VBarDataListNode* node =
        GetStart__VBarDataList(&this->bar_data_list);
    while (node != NULL && node->payload != NULL) {
      this->max_point = max(this->max_point, node->payload->value);

      struct VBarData* payload = node->payload;
      for (node = GetStart__VBarDataList(&this->bar_data_list); node != NULL;
           node = node->next) {
        if (node->payload == payload) {
          node = node->next;
          break;
        }
      }
    }
    if (this->max_point <= 0.0f) {
      this->max_point = 1.0f;
    }
  }
  else {
    this->max_point = scale;
  }

  this->min_point = 0.0f;
  struct VBarDataListNode* node = GetStart__VBarDataList(&this->bar_data_list);
  while (node != NULL && node->payload != NULL) {
    this->min_point = min(this->min_point, node->payload->value);

    struct VBarData* payload = node->payload;
    for (node = GetStart__VBarDataList(&this->bar_data_list); node != NULL;
         node = node->next) {
      if (node->payload == payload) {
        node = node->next;
        break;
      }
    }
  }
}

void __fastcall AddBar__14SetupVBarGraphFRC8VBarData(
    struct SetupVBarGraph* this, const void* edx, const struct VBarData* bar) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, bar={color=0x%08X, value=%f}) from 0x%p", this,
      *(uint32_t*)&bar->color, bar->value, puEBP[1]);

  struct VBarData* data =
      __nw__FUl(sizeof(struct VBarData), __FILE__, __LINE__);
  assert(data);
  ct__8VBarDataFRC8VBarData(data, edx, bar);

  PushBack__VBarDataList(&this->bar_data_list, edx, data);
}

void __fastcall SetBar__14SetupVBarGraphFiRC8VBarData(
    struct SetupVBarGraph* this, const void* edx, int index,
    const struct VBarData* bar) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(
      __FUNCTION__ "(%p, index=%d, bar={color=0x%08X, value=%f}) from 0x%p",
      this, index, *(uint32_t*)&bar->color, bar->value, puEBP[1]);

  if (index > -1 && index < this->bar_data_list.count) {
    struct VBarDataListNode* node =
        GetNodeAtPosition__VBarDataList(&this->bar_data_list, edx, index);
    if (node != NULL) {
      as__8VBarDataFRC8VBarData(node->payload, edx, bar);
    }
  }
}

void __fastcall GetBar__14SetupVBarGraphFiR8VBarData(
    const struct SetupVBarGraph* this, const void* edx, int index,
    struct VBarData* result) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, index=%d, result=0x%p) from 0x%p", this, index,
      result, puEBP[1]);

  if (index > -1 && index < this->bar_data_list.count) {
    struct VBarDataListNode* node =
        GetNodeAtPosition__VBarDataList(&this->bar_data_list, edx, index);
    if (node != NULL) {
      as__8VBarDataFRC8VBarData(result, edx, node->payload);
    }
  }
}
