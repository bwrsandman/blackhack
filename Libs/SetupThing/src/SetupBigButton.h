#ifndef BLACKHACK_SETUPBIGBUTTON_H
#define BLACKHACK_SETUPBIGBUTTON_H

#include "SetupButton.h"

#include "SetupThing.h"

struct SetupBigButton {
  struct SetupButton super;
  uint32_t text_position;
  enum BBSTYLE style;
  // TODO: "inner" here is just a guess, definitely is a bounding box
  struct SetupRect inner_rect;
};
static_assert(sizeof(struct SetupBigButton) == 0x25c, "Struct is of wrong size");

// win1.41 0040ceb0 mac 103deac0 SetupBigButton::Draw(bool, bool)
void __fastcall Draw__14SetupBigButtonFbb(struct SetupBigButton* this, const void* edx, bool hovered, bool selected);
// win1.41 0040d260 mac 100fd210 SetupBigButton::SetupBigButton(int, int, int, wchar_t *, int, int, int)
struct SetupBigButton * __fastcall
ct__14SetupBigButtonFiiiPwiii(struct SetupBigButton *this, const void* edx, int id, int x, int y, const wchar_t *label, int size, uint32_t text_position, enum BBSTYLE style);
// win1.41 0040d2f0 mac 101689f0 SetupBigButton::KeyDown(int, int)
void __fastcall KeyDown__14SetupBigButtonFii(struct SetupBigButton* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 0040d310 mac 101670b0 SetupBigButton::HitTest(int, int)
bool __fastcall HitTest__14SetupBigButtonFii(struct SetupBigButton *this, const void* edx, int x, int y);
// win1.41 0040d360 mac 1010fca0 SetupBigButton::~SetupBigButton(void)
struct SetupBigButton* __fastcall dt__14SetupBigButtonFb(struct SetupBigButton* this, const void* edx, bool param_1);

// win1.41 008ab3e0 mac none SetupBigButton virtual table
static struct vt__SetupControl_t vt__SetupBigButton = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = (vt__SetupControl_HitTest_t*)HitTest__14SetupBigButtonFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__14SetupBigButtonFbb,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__10SetupButtonFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__14SetupBigButtonFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__14SetupBigButtonFb,
};

#endif // BLACKHACK_SETUPBIGBUTTON_H
