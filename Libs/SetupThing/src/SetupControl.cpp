#include "SetupControl.h"
#include "SetupThing.h"

#include <stdlib.h>

#include <logger.h>

#include <LHSys.h>
#include <Memory.h>

#include <globals.h>

int __fastcall GetTextSize__12SetupControlFv(const struct SetupControl* this) {
  if (this->text_size != 0) {
    return this->text_size;
  }
  else if (this->setup_box != NULL) {
    return this->setup_box->default_text_size;
  }
  else {
    return 10;
  }
}

struct SetupControl* __fastcall ct__12SetupControlFiiiiiPw(
    struct SetupControl* this, const void* edx, int id, int x, int y, int width,
    int height, const wchar_t* label) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\") from %p",
      this, id, x, y, width, height, label, puEBP[1]);

  this->vftptr = &vt__SetupControl;
  this->field_0x4 = 0;
  struct SetupRect rect = {
    .p0 = { .x = x, .y = y },
    .p1 = { .x = x + width, .y = y + height },
  };
  this->rect = rect;
  this->id = id;
  this->field_0x1c = 0;
  this->text_size = 0;
  wcscpy_s(this->label, _countof(this->label), label);
  this->tooltip = NULL;
  this->focus = false;
  this->hidden = false;
  this->field_0x22a = true;
  this->field_0x22b = false;
  this->next = Get__10SetupThingFv()->box_0->widget_list;
  Get__10SetupThingFv()->box_0->widget_list = this;
  this->setup_box =  Get__10SetupThingFv()->box_0;
  this->continue_button_callback = NULL;

  return this;
}

struct SetupControl* __fastcall dt__12SetupControlFb(
    struct SetupControl* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__12SetupControlFv(this);
  if ((param_1 & 1U) != 0) {
    __dl__FPv(this);
  }
  return this;
}

void __fastcall dt__12SetupControlFv(struct SetupControl* this) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p) from %p", this, puEBP[1]);

  this->vftptr = &vt__SetupControl;
  if (this->setup_box->focused_widget == this) {
    SetFocusControl__8SetupBoxFP12SetupControl(this->setup_box, NULL, NULL);
  }
  if (this->setup_box->widget_0x74 == this) {
    this->setup_box->widget_0x74 = NULL;
  }
  if (this->setup_box->widget_list == this) {
    this->setup_box->widget_list = this->next;
  }
  else {
    for (struct SetupControl* prev = this->setup_box->widget_list; prev != NULL;
         prev = prev->next) {
      if (prev->next == this) {
        prev->next = this->next;
        break;
      }
    }
  }
}

void __fastcall SetToolTip__12SetupControlFUl(
    struct SetupControl* this, const void* edx, uint32_t tooltip_id) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(
      __FUNCTION__ "(%p, tooltip_id=%u) from %p", this, tooltip_id, puEBP[1]);

  if (tooltip_id >= globals()->STRUCT_00d17ca8->count) {
    tooltip_id = 0;
  }

  this->tooltip = globals()->STRUCT_00d17ca8->array[tooltip_id].str;
}

void __fastcall SetToolTip__12SetupControlFPw(
    struct SetupControl* this, const void* edx, const wchar_t* tooltip) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(
      __FUNCTION__ "(%p, tooltip=\"%S\") from %p", this, tooltip, puEBP[1]);

  this->tooltip = tooltip;
}

void __fastcall Hide__12SetupControlFb(
    struct SetupControl* this, const void* edx, bool hidden) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hidden=\"%s\") from %p", this,
      hidden ? "true" : "false", puEBP[1]);

  this->hidden = hidden;
}

void __fastcall SetFocus__12SetupControlFb(
    struct SetupControl* this, const void* edx, bool focus) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, focus=\"%s\") from %p", this,
      focus ? "true" : "false", puEBP[1]);

  this->focus = focus;
  bool bVar1 = 0;
  if (focus && this->field_0x4 != 0 &&
      this->setup_box == GetCurrentActiveBox__8SetupBoxFv()) {
    bVar1 = 1;
  }
  if ((Get__10SetupThingFv()->DAT_00c4cd00 && !bVar1) ||
      (!Get__10SetupThingFv()->DAT_00c4cd00 && bVar1)) {
    if (bVar1 != 0) {
      Activate__Q24slim5TbIMEFPv(Get__5LHSysFv()->field_0x70c4, edx, Get__5LHSysFv()->screen.window);
      Get__10SetupThingFv()->DAT_00c4cd00 = bVar1;
    }
    else {
      UnActivate__Q24slim5TbIMEFv(Get__5LHSysFv()->field_0x70c4);
      Get__10SetupThingFv()->DAT_00c4cd00 = false;
    }
  }
}

bool __fastcall HitTest__12SetupControlFii(
    struct SetupControl* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  bool result = x >= this->rect.p0.x && y >= this->rect.p0.y &&
                x < this->rect.p1.x && y < this->rect.p1.y;

  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) -> %s from %p", this, x, y,
      result ? "true" : "false", puEBP[1]);

  return result;
}

void __fastcall Drag__12SetupControlFii(
    struct SetupControl* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);
}

void __fastcall MouseDown__12SetupControlFiib(
    struct SetupControl* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);
}

void __fastcall MouseUp__12SetupControlFiib(
    struct SetupControl* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);
}

void __fastcall Click__12SetupControlFii(
    struct SetupControl* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);
}

void __fastcall KeyDown__12SetupControlFii(struct SetupControl* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);
}

void __fastcall Char__12SetupControlFi(
    struct SetupControl* this, const void* edx, int character) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, 0x%04x) from %p", this, character, puEBP[1]);
}
