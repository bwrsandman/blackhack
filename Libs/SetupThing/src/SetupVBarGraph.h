#ifndef BLACKHACK_SETUPVBARGRAPH_H
#define BLACKHACK_SETUPVBARGRAPH_H

#include "SetupButton.h"

#include <LH3DColor.h>
#include <LHMatrix.h>

struct VBarData {
  struct LH3DColor color;
  float value;
};
static_assert(sizeof(struct VBarData) == 0x8, "Struct is of wrong size");

// inlined VBarData::VBarData(const VBarData &)
struct VBarData * __fastcall ct__8VBarDataFRC8VBarData(struct VBarData *this, const void* edx, const struct VBarData* bar);
// inlined VBarData::operator=(const VBarData &)
struct VBarData * __fastcall as__8VBarDataFRC8VBarData(struct VBarData *this, const void* edx, const struct VBarData* bar);

struct VBarDataListNode {
  struct VBarDataListNode* next;
  struct VBarData* payload;
};
static_assert(sizeof(struct VBarDataListNode) == 0x8, "Struct is of wrong size");
// win1.41 inlined mac 103ad510 LHLinkedNode<VBarData*>::LHLinkedNode<VBarData*>(VBarData*, LHLinkedNode<VBarData>*)
struct VBarDataListNode * __fastcall ct__VBarDataListNode(struct VBarDataListNode*this, const void* edx, struct VBarData* payload, struct VBarDataListNode* next);

struct VBarDataList {
  struct VBarDataListNode* head;
  int count;
};
// inlined LHLinkedList<VBarData>::GetStart() const
struct VBarDataListNode* __fastcall GetStart__VBarDataList(const struct VBarDataList* this);
// win1.41 inlined mac 10525970 LHLinkedList<VBarData*>::GetNodeAtPosition() const
struct VBarDataListNode* __fastcall GetNodeAtPosition__VBarDataList(const struct VBarDataList* this, const void* edx, int index);
// win1.41 inlined mac 10531c60 LHLinkedList<VBarData*>::GetLastNode()
struct VBarDataListNode* __fastcall GetLastNode__VBarDataList(const struct VBarDataList* this);
// inlined
void __fastcall PushBack__VBarDataList(struct VBarDataList*this, const void* edx, struct VBarData* payload);
// win1.41 inlined mac 10347470 LHLinkedList<VBarData*>Remove(VBarData, b)
void __fastcall Remove__VBarDataList(struct VBarDataList *this, const void* edx, struct VBarData * bar, bool only_one);

struct SetupVBarGraph {
  struct SetupButton super;
  struct LHMatrix field_0x244;
  struct VBarDataList bar_data_list;
  float max_point;
  float min_point;
};
static_assert(sizeof(struct SetupVBarGraph) == 0x284, "Struct is of wrong size");

// win1.41 0040e8b0 mac 10379480 SetupVBarGraph::Draw(bool, bool)
void __fastcall Draw__14SetupVBarGraphFbb(struct SetupVBarGraph* this, const void* edx, bool hovered, bool selected);
// win1.41 0040ef00 mac 10354bc0 SetupVBarGraph::SetupVBarGraph(int, int, int, int, int, wchar_t *)
struct SetupVBarGraph* __fastcall ct__14SetupVBarGraphFiiiiiPw(struct SetupVBarGraph *this, const void* edx, int id, int x, int y, int width, int height, const wchar_t *label);
// win1.41 0040ef70 mac 10350e50 SetupVBarGraph::KeyDown(int, int)
void __fastcall KeyDown__14SetupVBarGraphFii(struct SetupVBarGraph* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 0040ef90 mac 103de920 SetupVBarGraph::~SetupVBarGraph(void)
struct SetupVBarGraph* __fastcall dt__14SetupVBarGraphFb(struct SetupVBarGraph* this, const void* edx, bool param_1);
// win1.41 0040efb0 mac unknown SetupVBarGraph::Reset(vfoid)
void __fastcall Reset__14SetupVBarGraphFv(struct SetupVBarGraph *this);
// win1.41 0040f1b0 mac 10351240 SetupVBarGraph::SetScale(float)
void __fastcall SetScale__14SetupVBarGraphFf(struct SetupVBarGraph *this, const void* edx, float scale);
// win1.41 0040f280 mac 103fccd0 SetupVBarGraph::AddBar(const VBarData &)
void __fastcall AddBar__14SetupVBarGraphFRC8VBarData(struct SetupVBarGraph *this, const void* edx, const struct VBarData *bar);
// win1.41 0040f300 mac 10352240 SetupVBarGraph::SetBar(int, const VBarData &)
void __fastcall SetBar__14SetupVBarGraphFiRC8VBarData(struct SetupVBarGraph *this, const void* edx, int index, const struct VBarData *bar);
// win1.41 0040f350 mac 103f1500 SetupVBarGraph::SetBar(int, const VBarData &)
void __fastcall GetBar__14SetupVBarGraphFiR8VBarData(const struct SetupVBarGraph *this, const void* edx, int index, struct VBarData *result);

typedef void (__fastcall vt_SetupVBarGraph_Reset_t)(struct SetupVBarGraph *this);
typedef void (__fastcall vt_SetupVBarGraph_SetScale_t)(struct SetupVBarGraph *this, const void* edx, float scale);
typedef void (__fastcall vt_SetupVBarGraph_AddLine_t)(struct SetupVBarGraph *this, const void* edx, const struct VBarData *line);
typedef void (__fastcall vt_SetupVBarGraph_SetLine_t)(struct SetupVBarGraph *this, const void* edx, int index, const struct VBarData *line);
typedef void (__fastcall vt_SetupVBarGraph_GetLine_t)(const struct SetupVBarGraph *this, const void* edx, int index, struct VBarData *result);

struct vt__SetupVBarGraph_t {
  struct vt__SetupControl_t super;
  vt_SetupVBarGraph_Reset_t * const Reset;
  vt_SetupVBarGraph_SetScale_t * const SetScale;
  vt_SetupVBarGraph_AddLine_t * const AddBar;
  vt_SetupVBarGraph_SetLine_t * const SetBar;
  vt_SetupVBarGraph_GetLine_t * const GetBar;
};


// win1.41 008ab480 mac none SetupVBarGraph virtual table
static struct vt__SetupVBarGraph_t vt__SetupVBarGraph = {
    .super = {
        .SetToolTipUl = SetToolTip__12SetupControlFUl,
        .SetToolTipPw = SetToolTip__12SetupControlFPw,
        .Hide = Hide__12SetupControlFb,
        .SetFocus = SetFocus__12SetupControlFb,
        .HitTest = HitTest__12SetupControlFii,
        .Draw = (vt__SetupControl_Draw_t*)Draw__14SetupVBarGraphFbb,
        .Drag = Drag__12SetupControlFii,
        .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
        .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__10SetupButtonFiib,
        .Click = Click__12SetupControlFii,
        .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__14SetupVBarGraphFii,
        .Char = Char__12SetupControlFi,
        .dtor = (vt__SetupControl_dtor_t*)dt__14SetupVBarGraphFb,
    },
    .Reset = Reset__14SetupVBarGraphFv,
    .SetScale = SetScale__14SetupVBarGraphFf,
    .AddBar = AddBar__14SetupVBarGraphFRC8VBarData,
    .SetBar = SetBar__14SetupVBarGraphFiRC8VBarData,
    .GetBar = GetBar__14SetupVBarGraphFiR8VBarData,
};

#endif // BLACKHACK_SETUPVBARGRAPH_H
