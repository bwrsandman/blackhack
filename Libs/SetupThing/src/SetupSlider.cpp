#include "SetupSlider.h"

#include <logger.h>

#include <Utils.h>

#include "Setup.h"
#include "SetupThing.h"

void __fastcall KeyDown__11SetupSliderFii(struct SetupSlider* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  bool key_consumed = true;
  switch (key) {
  case LHKEY_HOME:
    this->value = 0.0f;
    break;
  case LHKEY_END:
    this->value = 1.0f;
  case LHKEY_LEFT:
    this->value -= 0.1f;
    break;
  case LHKEY_RIGHT:
    this->value += 0.1f;
    break;
  default:
    key_consumed = false;
    break;
  }

  this->value = saturatef(this->value);
  this->drag_start_value = this->value;

  if (key_consumed) {
    if (this->super.setup_box->field_0xb0 != NULL) {
      this->super.setup_box->field_0xb0(
          4, this->super.setup_box, &this->super, 0, 0);
    }
  }
}

void __fastcall Draw__11SetupSliderFbb(
    struct SetupSlider* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  DrawBevBox__10SetupThingFiiiiiiiUl(this->super.rect.p0.x,
      this->super.rect.p0.y, this->super.rect.p1.x, this->super.rect.p1.y, 1,
      0x10, 0xffffffff, white);

  int button_x = (int)((float)(this->super.rect.p1.x - this->super.rect.p0.x -
                               this->height) *
                       this->value) +
                 this->super.rect.p0.x;

  int x = (this->super.rect.p0.x + this->super.rect.p1.x) / 2;
  int width = this->super.rect.p1.x - this->super.rect.p0.x;
  const struct LH3DColor* text_color =
      selected ? &LH3DColor_00c4ccf8
               : &LH3DColor_ARRAY_00c4ccd8_0;

  int y;
  int text_size;
  if (this->super.field_0x1c & 0x40000000u) {
    int height = this->super.rect.p1.y - this->super.rect.p0.y;
    int box_size = height / 2;
    DrawBevBox__10SetupThingFiiiiiiiUl(button_x,
        this->super.rect.p0.y + box_size, this->height + button_x,
        this->super.rect.p0.y - 2 + height, 0, 0x10, 0xffffffff, white);
    text_size = box_size + 2;
    y = this->super.rect.p0.y + 2;
  }
  else {
    DrawBigButton__10SetupThingFiibbi7BBSTYLEbii(button_x + 3,
        this->super.rect.p0.y + 3, true, hovered || selected, this->height - 6,
        BBSTYLE_CHECK_BOX_OFF, false, -0xa000, 0xa000);
    text_size = GetTextSize__12SetupControlFv(&this->super);
    y = (this->super.rect.p0.y + this->super.rect.p1.y) / 2 - text_size / 2;
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(x, y, width,
      TEXTJUSTIFY_CENTRE, this->super.label, text_size, text_color, 0);
}

struct SetupSlider* __fastcall ct__11SetupSliderFiiiiifPw(
    struct SetupSlider* this, void* edx, int id, int x, int y, int width,
    int height, float value, wchar_t* label) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "%f, label=\"%S\") from %p",
      this, id, x, y, width, height, value, label, puEBP[1]);

  ct__12SetupControlFiiiiiPw(&this->super, edx, id, x, y, width, height, label);

  this->super.vftptr = &vt__SetupSlider;
  this->value = value;
  this->drag_start_value = value;
  this->height = this->super.rect.p1.y - y;

  return this;
}

struct SetupSlider* __fastcall dt__11SetupSliderFb(
    struct SetupSlider* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__12SetupControlFb(&this->super, edx, param_1);

  return this;
}

void __fastcall Drag__11SetupSliderFii(
    struct SetupSlider* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);

  int range = (this->super.rect.p1.x - this->super.rect.p0.x) - this->height;
  int new_x =
      (int)((float)range * this->drag_start_value) + this->super.rect.p0.x;
  if (this->drag_start.x < new_x) {
    this->value = this->drag_start_value - 0.1f;
    if (this->drag_start.x >= this->height + new_x) {
      this->value = this->drag_start_value + 0.1f;
    }
  }
  else {
    if (this->height + new_x <= this->drag_start.x) {
      if (this->drag_start.x < new_x) {
        this->value = this->drag_start_value - 0.1f;
      }
      if (this->drag_start.x >= this->height + new_x) {
        this->value = this->drag_start_value + 0.1f;
      }
    }
    else {
      this->value = (float)(x - this->drag_start.x) / (float)range +
                    this->drag_start_value;
    }
  }

  this->value = saturatef(this->value);
}

void __fastcall MouseDown__11SetupSliderFiib(
    struct SetupSlider* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  if (param_3 == false) {
    return;
  }

  this->drag_start.x = x;
  this->drag_start.y = y;
  this->drag_start_value = this->value;
}

void __fastcall MouseUp__11SetupSliderFiib(
    struct SetupSlider* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  if (this->super.setup_box->field_0xb0 != NULL) {
    LOG_DEBUG("this->super.setup_box->field_0xb0 callback @ %p",
        this->super.setup_box->field_0xb0);
    this->super.setup_box->field_0xb0(
        1, this->super.setup_box, &this->super, x, y);
  }
  this->super.vftptr->Click(&this->super, edx, x, y);
}
