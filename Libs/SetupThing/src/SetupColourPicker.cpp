#include "SetupColourPicker.h"

#include <logger.h>

#include <LHSys.h>
#include <Utils.h>

#include "SetupThing.h"

void __fastcall MouseDown__17SetupColourPickerFiib(
    struct SetupColourPicker* this, const void* edx, int x, int y,
    bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  MouseDown__10SetupButtonFiib(&this->super, edx, x, y, param_3);
}

void __fastcall MouseUp__17SetupColourPickerFiib(struct SetupColourPicker* this,
    const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  MouseUp__10SetupButtonFiib(&this->super, edx, x, y, param_3);
}

void __fastcall Drag__17SetupColourPickerFii(
    struct SetupColourPicker* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);

  this->slider_position = saturatef(
      (float)(y - this->super.super.rect.p0.y) /
      (float)(this->super.super.rect.p1.y - this->super.super.rect.p0.y));
}

void __fastcall Draw__17SetupColourPickerFbb(struct SetupColourPicker* this,
    const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };
  const struct LH3DColor clear_color = { .b = 0, .g = 0, .r = 0, .a = 0 };

  struct LHCoord mouse;
  ct__7LHCoordFRC7LHCoord(&mouse, edx, &Get__5LHSysFv()->mouse.coord_0xbc);
  unadjust__10SetupThingFRiRi(&mouse.x, &mouse.y);

  struct SetupRect rect = {
    .p0 = { this->super.super.rect.p0.x + 16, this->super.super.rect.p0.y },
    .p1 = { this->super.super.rect.p1.x - 16, this->super.super.rect.p1.y },
  };

  DrawBevBox__10SetupThingFiiiiiiiUl(rect.p0.x - 2, rect.p0.y - 2,
      rect.p1.x + 2, rect.p1.y + 2, hovered ? 2 : 1, 0x10, 0xffffffff, white);

  this->slider_position = saturatef(this->slider_position);

  int height = rect.p1.y - rect.p0.y;

  struct LHCoord button_coord = {
    .y = (int)((float)rect.p0.y + this->slider_position * (float)height) - 8,
  };

  enum BBSTYLE style;
  bool interacted;
  if (this->brightness_slider) {
    int middle = (rect.p0.y + rect.p1.y) / 2;

    DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(rect.p0.x, rect.p0.y, rect.p1.x,
        middle, clear_color, clear_color, this->color, this->color, true, true);
    DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(rect.p0.x, middle, rect.p1.x,
        rect.p1.y, this->color, this->color, white, white, true, true);

    style = BBSTYLE_LEFT_ARROW;
    interacted = hovered && mouse.x > rect.p1.x;
    button_coord.x = rect.p1.x;
  }
  else {
    // Hue slider
    if (this->material != NULL) {
      DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(rect.p0.x,
          rect.p0.y, rect.p1.x, rect.p1.y, 1.0f / 512.0f,
          1.0f / 8.0f + 1.0f / 512.0f, 1.0f / 8.0f - 1.0f / 512.0f,
          1.0f - 1.0f / 512.0f, this->material, &white, true, -0xa000, 0xa000,
          false, 100.0f);
    }

    style = BBSTYLE_RIGHT_ARROW;
    interacted = hovered && mouse.x < rect.p0.x;
    button_coord.x = rect.p0.x - 16;
  }
  DrawBigButton__10SetupThingFiibbi7BBSTYLEbii(button_coord.x, button_coord.y,
      true, interacted, 0x10, style, false, -0xa000, 0xa000);
}

struct
    SetupColourPicker* __fastcall ct__17SetupColourPickerFiiiiiiP12LH3DMaterial(
        struct SetupColourPicker* this, const void* edx, int id, int x, int y,
        int width, int height, bool brightness_slider,
        struct LH3DMaterial* material) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "brightness_slider=%s, %p) from %p",
      this, id, x, y, width, height, brightness_slider ? "true" : "false",
      material, puEBP[1]);

  const struct LH3DColor black = { .b = 0x00, .g = 0x00, .r = 0x00, .a = 0xFF };
  const struct LH3DColor gray = { .b = 0x80, .g = 0x80, .r = 0x80, .a = 0x00 };

  ct__10SetupButtonFiiiiiPwi(&this->super, edx, id, x, y, width, height,
      Get__10SetupThingFv()->WCHAR_00c4cd30, 0);

  this->material = material;
  this->super.super.vftptr = &vt__SetupColourPicker;
  this->color_0x244 = black;
  this->brightness_slider = brightness_slider;
  this->slider_position = 0.5f;
  this->color = gray;

  return this;
}

void __fastcall KeyDown__17SetupColourPickerFii(struct SetupColourPicker* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.super.setup_box != NULL) {
    this->super.super.setup_box->vftptr->field_0x0(
        this->super.super.setup_box, edx, key, mod);
  }
}

void __fastcall Click__17SetupColourPickerFii(
    struct SetupColourPicker* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);
}

struct SetupColourPicker* __fastcall dt__17SetupColourPickerFb(
    struct SetupColourPicker* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}
