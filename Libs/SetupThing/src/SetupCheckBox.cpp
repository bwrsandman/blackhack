#include "SetupCheckBox.h"

#include <logger.h>

#include "Setup.h"
#include "SetupThing.h"

void __fastcall Draw__13SetupCheckBoxFbb(
    struct SetupCheckBox* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  gSetupThingRect = this->super.super.rect;

  DrawBigButton__10SetupThingFiibbi7BBSTYLEbii(this->super.super.rect.p0.x,
      this->super.super.rect.p0.y, this->super.pressed, hovered,
      this->super.super.rect.p1.x - this->super.super.rect.p0.x, this->style,
      true, -0xa000, 0xa000);

  const int text_size = GetTextSize__12SetupControlFv(&this->super.super);

  const struct LH3DColor* p_color;
  if (hovered || (selected && this->super.super.label[0] == L'\0')) {
    p_color = &LH3DColor_ARRAY_00c4ccd8_3;
  }
  else {
    p_color = &LH3DColor_00c4ccf8;
  }

  enum TEXTJUSTIFY justify;
  struct LHCoord coord;
  switch (this->text_position) {
  case 0:
    justify = TEXTJUSTIFY_LEFT;
    coord.x = this->super.super.rect.p1.x + 4;
    coord.y = (this->super.super.rect.p0.y + this->super.super.rect.p1.y) / 2 -
              text_size / 2;
    break;

  default:
    assert(false); // if this is hit, then the block is the same as 1
  case 1:
    justify = TEXTJUSTIFY_RIGHT;
    coord.x = this->super.super.rect.p0.x - 4;
    coord.y = (this->super.super.rect.p0.y + this->super.super.rect.p1.y) / 2 -
              text_size / 2;
    break;

  case 2:
    justify = TEXTJUSTIFY_CENTRE;
    coord.x = (this->super.super.rect.p1.x + this->super.super.rect.p0.x) / 2;
    coord.y = this->super.super.rect.p1.y + 2;
    break;
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(coord.x + 2,
      coord.y + 2, 1000, justify, this->super.super.label, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, 0);
  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(coord.x, coord.y, 1000,
      justify, this->super.super.label, text_size, p_color, 0);

  this->inner_rect = gSetupThingRect;
}

struct SetupCheckBox* __fastcall ct__13SetupCheckBoxFiiibiPwi(
    struct SetupCheckBox* this, const void* edx, int id, int x, int y,
    bool checked, enum BBSTYLE style, const wchar_t* label, int size) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=0x%08x, x=%d, y=%d, checked=%s, style=%s, "
                         "label=\"%S\", size=%d) from %p",
      this, id, x, y, checked ? "true" : "false",
      BBSTYLE_strs[style] == NULL ? "" : BBSTYLE_strs[style], label, size,
      puEBP[1]);

  ct__10SetupButtonFiiiiiPwi(&this->super, edx, id, x, y, size, size, label, 0);

  this->super.super.vftptr = &vt__SetupCheckBox;
  this->super.super.text_size = GetMidTextSize__Fv();
  this->super.pressed = false;
  // 0: text is to the right
  // 1: text is to the left
  // 2: text is below the box
  this->text_position = 2;
  this->style = style;
  this->checked = checked;
  this->inner_rect.p0.x = 0;
  this->inner_rect.p0.y = 0;
  this->inner_rect.p1.x = 0;
  this->inner_rect.p1.y = 0;

  return this;
}

void __fastcall Click__13SetupCheckBoxFii(
    struct SetupCheckBox* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);

  if (this->checked) {
    this->style = BBSTYLE_CHECK_BOX_ON;
  }
  else {
    this->style = (this->style == BBSTYLE_CHECK_BOX_OFF)
                      ? BBSTYLE_CHECK_BOX_ON
                      : BBSTYLE_CHECK_BOX_OFF;
  }
}

void __fastcall KeyDown__13SetupCheckBoxFii(struct SetupCheckBox* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.super.setup_box != NULL) {
    LOG_DEBUG("%p", this->super.super.setup_box->vftptr->field_0x0);
    this->super.super.setup_box->vftptr->field_0x0(
        this->super.super.setup_box, edx, key, mod);
  }
}

struct SetupCheckBox* __fastcall dt__13SetupCheckBoxFb(
    struct SetupCheckBox* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}

bool __fastcall HitTest__13SetupCheckBoxFii(
    struct SetupCheckBox* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  int x_rel =
      x - (this->super.super.rect.p1.x + this->super.super.rect.p0.x) / 2;
  int y_rel =
      y - (this->super.super.rect.p1.x + this->super.super.rect.p1.y) / 2;
  int radius = (this->super.super.rect.p1.x - this->super.super.rect.p0.x) / 2;

  bool result = radius * radius > x_rel * x_rel + y_rel * y_rel;

  if (!result) {
    result = x >= this->inner_rect.p0.x && y >= this->inner_rect.p0.y &&
             x < this->inner_rect.p1.x && y < this->inner_rect.p1.y;
  }

  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) -> %s from 0x%p", this, x, y,
      result ? "true" : "false", puEBP[1]);

  return result;
}
