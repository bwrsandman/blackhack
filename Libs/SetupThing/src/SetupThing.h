#ifndef BLACKHACK_SETUPTHING_H
#define BLACKHACK_SETUPTHING_H

#include <stdbool.h>
#include <stdint.h>
#include <wchar.h>

#include <LH3DColor.h>

#include "SetupBox.h"
#include "SetupRect.h"
#include "TextJustify.h"

enum BBSTYLE {
  BBSTYLE_CHECK_BOX_OFF = 0x00,
  BBSTYLE_CHECK_BOX_ON = 0x01,
  BBSTYLE_LEFT_ARROW = 0x02,
  BBSTYLE_RIGHT_ARROW = 0x03,
  BBSTYLE_ROTATION = 0x04,
  BBSTYLE_SPEECH = 0x05,
  BBSTYLE_NO_SPEECH = 0x06,
  BBSTYLE_SPEECH_ARROW = 0x07,
  BBSTYLE_EXCLAIM_ARROW = 0x08,
  BBSTYLE_ENVELOPE_ARROW = 0x09,
  BBSTYLE_ENVELOPE = 0x0a,
  BBSTYLE_WEATHER_SUNNY = 0x0b,
  BBSTYLE_WEATHER_PARTIALLY_CLOUDY = 0x0c,
  BBSTYLE_WEATHER_CLOUDY = 0x0d,
  BBSTYLE_WEATHER_SCATTERED_SHOWERS = 0x0e,
  BBSTYLE_WEATHER_SHOWERS = 0x0f,
  BBSTYLE_WEATHER_SCATTERED_FLURRIES = 0x10,
  BBSTYLE_WEATHER_FLURRIES = 0x11,
  BBSTYLE_WEATHER_HEAVY_FLURRIES = 0x12,
  BBSTYLE_WEATHER_THUNDER_STORMS = 0x13,
  BBSTYLE_0x14 = 0x14, ///< Possibly catch texture to indicate bad indexing?

  BBSTYLE_COUNT,
  BBSTYLE_FORCE_32_BIT = 0x7FFFFFFF,
};

static const char* BBSTYLE_strs[BBSTYLE_COUNT] = {
    [BBSTYLE_CHECK_BOX_OFF] = "BBSTYLE_CHECK_BOX_OFF",
    [BBSTYLE_CHECK_BOX_ON] = "BBSTYLE_CHECK_BOX_ON",
    [BBSTYLE_LEFT_ARROW] = "BBSTYLE_LEFT_ARROW",
    [BBSTYLE_RIGHT_ARROW] = "BBSTYLE_RIGHT_ARROW",
    [BBSTYLE_ROTATION] = "BBSTYLE_ROTATION",
    [BBSTYLE_SPEECH] = "BBSTYLE_SPEECH",
    [BBSTYLE_NO_SPEECH] = "BBSTYLE_NO_SPEECH",
    [BBSTYLE_SPEECH_ARROW] = "BBSTYLE_SPEECH_ARROW",
    [BBSTYLE_EXCLAIM_ARROW] = "BBSTYLE_EXCLAIM_ARROW",
    [BBSTYLE_ENVELOPE_ARROW] = "BBSTYLE_ENVELOPE_ARROW",
    [BBSTYLE_ENVELOPE] = "BBSTYLE_ENVELOPE",
    [BBSTYLE_WEATHER_SUNNY] = "BBSTYLE_WEATHER_SUNNY",
    [BBSTYLE_WEATHER_PARTIALLY_CLOUDY] = "BBSTYLE_WEATHER_PARTIALLY_CLOUDY",
    [BBSTYLE_WEATHER_CLOUDY] = "BBSTYLE_WEATHER_CLOUDY",
    [BBSTYLE_WEATHER_SCATTERED_SHOWERS] = "BBSTYLE_WEATHER_SCATTERED_SHOWERS",
    [BBSTYLE_WEATHER_SHOWERS] = "BBSTYLE_WEATHER_SHOWERS",
    [BBSTYLE_WEATHER_SCATTERED_FLURRIES] = "BBSTYLE_WEATHER_SCATTERED_FLURRIES",
    [BBSTYLE_WEATHER_FLURRIES] = "BBSTYLE_WEATHER_FLURRIES",
    [BBSTYLE_WEATHER_HEAVY_FLURRIES] = "BBSTYLE_WEATHER_HEAVY_FLURRIES",
    [BBSTYLE_WEATHER_THUNDER_STORMS] = "BBSTYLE_WEATHER_THUNDER_STORMS",
    [BBSTYLE_0x14] = "BBSTYLE_0x14",
};

struct LH3DMaterial;

/// All uses are inlined so this might just be a collection of globals
struct SetupThing {
  struct LH3DMaterial * ui_shadow_material;
  uint32_t field_0x4;
  struct Zoomer zoomer;
  struct SetupRect SetupRect_00c4ccb8;
  uint8_t field_0x48;
  uint32_t field_0x4c;
  uint32_t field_0x50;
  uint32_t field_0x54;
  struct LH3DColor LH3DColor_ARRAY_00c4ccd8[5];
  uint8_t field_0x6c;
  uint8_t field_0x6d;
  uint8_t field_0x6e;
  struct LH3DMesh * mesh;
  uintptr_t field_0x74;
  struct LH3DColor LH3DColor_00c4ccf8;
  uint32_t field_0x7c;
  bool DAT_00c4cd00;
  int field_0x84;
  int field_0x88;
  int DAT_00c4cd0c;
  int field_0x90;
  int field_0x94;
  int field_0x98;
  struct SetupBox* box_0; // 0x00c4cd1c
  struct SetupBox* current_active_box;
  struct SetupBox* field_0xa4;
  bool initialized;
  struct GatheringText* font;
  wchar_t WCHAR_00c4cd30[4];
};

// Inlined SetupThing::GetDrawAlpha(void)
uint8_t* GetDrawAlpha__10SetupThingFv(void);
// Inlined SetupThing::Get(void)
struct SetupThing* Get__10SetupThingFv(void);
// win1.41 00411690 mac 10135530 SetupThing::GetTextHeight(int, int, int, int, int, bool, wchar_t *, int)
float __cdecl GetTextHeight__10SetupThingFiiiiibPwi(int param_1, int param_2, int param_3, int param_4, int param_5, bool param_6, const wchar_t* text, int param_8);
// win1.41 00411720 mac 1016ab60 SetupThing::GetTextWidth(wchar_t *, float, int, float)
float __cdecl GetTextWidth__10SetupThingFPwfif(const wchar_t *text, float size, int param_3, float param_4);
// win1.41 00411750 mac 1016d490 SetupThing::DrawTextWrap(int, int, int, int, int, bool, wchar_t *, int, LH3DColor *, bool, bool)
float __cdecl DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(int x_min, int y_min, int x_max, int y_max, int param_5, bool param_6, const wchar_t *text, int param_8, const struct LH3DColor *color, bool param_10, bool param_11);
// win1.41 004119b0 mac 103e1a40 SetupThing::DrawText(int, int, int, TEXTJUSTIFY, wchar_t *, int, LH3DColor *, int)
float __cdecl DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(int x, int y, int width, enum TEXTJUSTIFY justify, const wchar_t *text, int size, const struct LH3DColor* p_color, int param_8);
// win1.41 00411b40 mac 103e4a50 SetupThing::adjust(int &, int &)
float __cdecl adjust__10SetupThingFRiRi(int *x, int *y);
// win1.41 00411c30 mac 104f62a0 SetupThing::unadjust(int &, int &)
float __cdecl unadjust__10SetupThingFRiRi(int *x, int *y);
// win1.41 00411e70 mac 104f2b70 SetupThing::adjusty(int)
int __cdecl adjusty__10SetupThingFi(int y);
// win1.41 00412030 mac 10174f00 SetupThing::unadjustsize(float)
float __cdecl unadjustsize__10SetupThingFf(float size);
// win1.41 00412150 mac 105882d0 SetupThing::DrawBigButton(int, int, bool, bool, int, BBSTYLE, bool, int, int)
void __cdecl DrawBigButton__10SetupThingFiibbi7BBSTYLEbii(int x, int y, bool centered, bool interacted, int size, enum BBSTYLE style, bool shadowed, int clip_y_start, int clip_y_end);
// win1.41 004125a0 mac 101a2290 SetupThing::DrawLine(int, int, int, int, unsigned long, int, float, float)
void __cdecl DrawLine__10SetupThingFiiiiUliff(int param_1, int param_2, int param_3, int param_4, struct LH3DColor color, int adjust, float param_7, float inv_w);
// win1.41 00412980 mac 10048980 SetupThing::DrawBox(int, int, int, int, float, float, float, float, LH3DMaterial *, LH3DColor *, int, int, int, bool, float)
void __cdecl DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
    int x_min, int y_min, int x_max, int y_max, float u_min, float v_min, float u_max,
    float v_max, struct LH3DMaterial *material, const struct LH3DColor *color, int adjust, int clip_y_start,
    int clip_y_end, bool depth_test, float inv_w);
// win1.41 00412eb0 mac 1010f3e0 SetupThing::DrawQuad(int, int, int, int, int, int, int, int, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long)
void __cdecl DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl(
    int x_1, int y_1, int x_2, int y_2, int x_3, int y_3, int x_4, int y_4,
    struct LH3DColor color_1, struct LH3DColor color_2, struct LH3DColor color_3, struct LH3DColor color_4,
    uint32_t use_alpha, uint32_t adjust);
// win1.41 004132c0 mac 1035b610 SetupThing::DrawBox(int, int, int, int, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long, unsigned long)
void __cdecl DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(int x_min, int y_min, int x_max, int y_max, struct LH3DColor color_1, struct LH3DColor color_2, struct LH3DColor color_3, struct LH3DColor color_4, uint32_t use_alpha, uint32_t adjust);
// win1.41 00413360 mac 1013c530 SetupThing::DrawTab(int, int, int, int, int, int, int, wchar_t *, int, int)
void __cdecl DrawTab__10SetupThingFiiiiiiiPwii(int x_min, int y_min, int x_max, int y_max, bool selected, bool first_in_row, bool last_in_row, const wchar_t *label, struct LH3DColor color, bool no_blend);
// win1.41 00413c20 mac 10594590 SetupThing::DrawBevBox(int, int, int, int, int, int, int, unsigned long)
void __cdecl DrawBevBox__10SetupThingFiiiiiiiUl(int x_min, int y_min, int x_max, int y_max, uint32_t style, uint32_t outline_thickness, uint32_t horizontal_outline, struct LH3DColor color);

#endif // BLACKHACK_SETUPTHING_H
