#ifndef BLACKHACK_SETUPSTATICTEXTNOHIT_H
#define BLACKHACK_SETUPSTATICTEXTNOHIT_H

#include "SetupStaticText.h"

#include "TextJustify.h"

/// Since the constructor is inlined, it is difficult to override the virtual
/// table. One solution is to override the original virtual table pointers

struct SetupStaticTextNoHit {
  struct SetupStaticText super;
};
static_assert(sizeof(struct SetupStaticTextNoHit) == 0x244, "struct size is wrong");

// win1.41 inlined mac 10327f40 SetupStaticTextNoHit::SetupStaticTextNoHit(int, int, int, int, int, wchar_t *, TEXTJUSTIFY)
struct SetupStaticTextNoHit* __fastcall ct__20SetupStaticTextNoHitFiiiiiPw11TEXTJUSTIFY(
    struct SetupStaticTextNoHit* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label, enum TEXTJUSTIFY text_justify);
// win1.41 00571f00 mac 10328c60 SetupStaticTextNoHit::HitTest(int, int)
bool __fastcall HitTest__20SetupStaticTextNoHitFii(struct SetupStaticTextNoHit* this, const void* edx, int x, int y);
// win1.41 00571f10 mac 10328bc0 SetupStaticTextNoHit::~SetupStaticTextNoHit(void)
struct SetupStaticTextNoHit* __fastcall dt__20SetupStaticTextNoHitFb(struct SetupStaticTextNoHit* this, const void* edx, bool param_1);

// win1.41 00900098 mac none SetupStaticTextNoHit virtual table
static struct vt__SetupControl_t vt__SetupStaticTextNoHit = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = (vt__SetupControl_HitTest_t*)HitTest__20SetupStaticTextNoHitFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__15SetupStaticTextFbb,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = MouseDown__12SetupControlFiib,
    .MouseUp = MouseUp__12SetupControlFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = KeyDown__12SetupControlFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__20SetupStaticTextNoHitFb,
};

#endif // BLACKHACK_SETUPSTATICTEXTNOHIT_H
