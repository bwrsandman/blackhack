#include "SetupStaticText.h"

#include "logger.h"

#include "Setup.h"
#include "SetupThing.h"

struct SetupStaticText* __fastcall ct__15SetupStaticTextFiiiiiPw11TEXTJUSTIFY(
    struct SetupStaticText* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label,
    enum TEXTJUSTIFY text_justify) {

  ct__12SetupControlFiiiiiPw(&this->super, edx, id, x, y, width, height, label);

  this->super.vftptr = &vt__SetupStaticText;
  this->super.field_0x22a = false;
  this->text_justify = text_justify;
  this->display_text_size = 0;

  return this;
}

void __fastcall Draw__15SetupStaticTextFbb(struct SetupStaticText* this,
    const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  int text_size = GetTextSize__12SetupControlFv(&this->super);

  if (this->display_text_size < 10) {
    this->display_text_size = text_size;
  }

  int width = this->super.rect.p1.x - this->super.rect.p0.x;
  int height = this->super.rect.p1.y - this->super.rect.p0.y;

  if (this->display_text_size != text_size) {
    this->display_text_size = text_size;
    bool horizontal = this->text_justify != 3 && this->text_justify != 4;
    int desired_size = horizontal ? width : height;
    while (this->display_text_size > 10) {
      float candidate_size;
      if (horizontal) {
        candidate_size = GetTextWidth__10SetupThingFPwfif(
            this->super.label, (float)this->display_text_size, 0, 1.0f);
      }
      else {
        candidate_size = GetTextHeight__10SetupThingFiiiiibPwi(
            this->super.rect.p0.x, this->super.rect.p0.y, this->super.rect.p1.x,
            this->display_text_size + this->super.rect.p1.y,
            this->super.rect.p0.y, false, this->super.label,
            this->display_text_size);
      }
      if (candidate_size <= (float)desired_size) {
        break;
      }
      --this->display_text_size;
    }
  }

  assert(this->text_justify < TEXTJUSTIFY_COUNT);


  float drawn_height;
  float drawn_width;
  if (this->text_justify < TEXTJUSTIFY_LEFT_BREAK) {
    struct LHCoord point = {
      .y = (this->super.rect.p1.y + this->super.rect.p0.y) / 2 -
           this->display_text_size / 2,
    };

    switch (this->text_justify) {
    case TEXTJUSTIFY_LEFT:
      point.x = this->super.rect.p0.x;
      break;
    case TEXTJUSTIFY_CENTRE:
      point.x = (this->super.rect.p1.x + this->super.rect.p0.x) / 2;
      break;
    case TEXTJUSTIFY_RIGHT:
      point.x = this->super.rect.p1.x;
      break;
    default:
      assert(false);
      break;
    }
    // Shadow
    DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(point.x + 2,
        point.y + 2, width, this->text_justify, this->super.label,
        this->display_text_size, &LH3DColor_ARRAY_00c4ccd8_1, 0);
    drawn_height = DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
        point.x, point.y, width, this->text_justify, this->super.label,
        this->display_text_size, &LH3DColor_ARRAY_00c4ccd8_4, 0);
    drawn_width = GetTextWidth__10SetupThingFPwfif(
        this->super.label, (float)this->display_text_size, 0, 1.0f);
  }
  else {
    enum TEXTJUSTIFY justify = this->text_justify - TEXTJUSTIFY_LEFT_BREAK;
    // Shadow
    DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(this->super.rect.p0.x + 2,
        this->super.rect.p0.y + 2, this->super.rect.p1.x + 2,
        this->super.rect.p1.y + 2, this->super.rect.p0.y + 2, justify,
        this->super.label, this->display_text_size,
        &LH3DColor_ARRAY_00c4ccd8_1, this->super.field_0x1c != 0,
        false);
    drawn_height =
        DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(this->super.rect.p0.x,
            this->super.rect.p0.y, this->super.rect.p1.x, this->super.rect.p1.y,
            this->super.rect.p0.y, justify, this->super.label,
            this->display_text_size, &LH3DColor_ARRAY_00c4ccd8_4,
            this->super.field_0x1c != 0, false);
    drawn_width = (float)width;
  }

  if (drawn_width > (float)width || drawn_height > (float)height) {
    if (this->display_text_size > 10) {
      --this->display_text_size;
    }
  }
}

struct SetupStaticText* __fastcall dt__15SetupStaticTextFb(
    struct SetupStaticText* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__12SetupControlFb(&this->super, edx, param_1);

  return this;
}
