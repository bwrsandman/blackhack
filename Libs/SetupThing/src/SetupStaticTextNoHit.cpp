#include "SetupStaticTextNoHit.h"

#include <logger.h>

struct
    SetupStaticTextNoHit* __fastcall ct__20SetupStaticTextNoHitFiiiiiPw11TEXTJUSTIFY(
    struct SetupStaticTextNoHit* this, const void* edx, int id, int x,
    int y, int width, int height, const wchar_t* label,
    enum TEXTJUSTIFY text_justify) {

  ct__12SetupControlFiiiiiPw(
      &this->super.super, edx, id, x, y, width, height, label);

  this->super.super.vftptr = &vt__SetupStaticTextNoHit;
  this->super.super.field_0x22a = 0;
  this->super.text_justify = text_justify;
  this->super.display_text_size = 0;

  return this;
}

bool __fastcall HitTest__20SetupStaticTextNoHitFii(
    struct SetupStaticTextNoHit* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  assert(false);

  LOG_TRACE(
      __FUNCTION__ "(%p, x=%d, y=%d) -> false from %p", this, x, y, puEBP[1]);

  return false;
}

struct SetupStaticTextNoHit* __fastcall dt__20SetupStaticTextNoHitFb(
    struct SetupStaticTextNoHit* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
            puEBP[1]);

  dt__15SetupStaticTextFb(&this->super, edx, param_1);

  return this;
}
