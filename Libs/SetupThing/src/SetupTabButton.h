#ifndef BLACKHACK_SETUPTABBUTTON_H
#define BLACKHACK_SETUPTABBUTTON_H

#include "SetupButton.h"

#include "LH3DColor.h"

// win1.41 0040f3a0 mac 104081c0 SetupTabButton::Draw(bool, bool)
void __fastcall Draw__14SetupTabButtonFbb(struct SetupTabButton* this, const void* edx, bool hovered, bool selected);
// win1.41 0040f5e0 mac 101995b0 SetupTabButton::SetupTabButton(int, int, int, int, int, wchar_t *, int, int, int)
struct SetupTabButton * __fastcall
ct__14SetupTabButtonFiiiiiPwiii(struct SetupTabButton *this, const void* edx, int id, int x, int y,
                                int width, int height, const wchar_t *label, bool selected, bool first_in_row, bool last_in_row);
// win1.41 0040f670 mac 1037abd0 SetupTabButton::KeyDown(int, int)
void __fastcall KeyDown__14SetupTabButtonFii(struct SetupTabButton* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 0040f690 mac 10369440 SetupTabButton::~SetupTabButton(void)
struct SetupTabButton * __fastcall dt__14SetupTabButtonFb(struct SetupTabButton *this, const void* edx, bool param_1);

struct SetupTabButton {
  struct SetupButton super;
  uint32_t selected;     ///< Act as bool but aligned to 32 bits
  uint32_t first_in_row; ///< Act as bool but aligned to 32 bits
  uint32_t last_in_row;  ///< Act as bool but aligned to 32 bits
  struct LH3DColor color;
};
static_assert(sizeof(struct SetupTabButton) == 0x254, "Struct is of wrong size");

// win1.41 008ab4d0 mac none SetupTabButton virtual table
static struct vt__SetupControl_t vt__SetupTabButton = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__14SetupTabButtonFbb,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__10SetupButtonFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__14SetupTabButtonFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__14SetupTabButtonFb,
};

#endif // BLACKHACK_SETUPTABBUTTON_H
