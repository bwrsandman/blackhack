#include "Setup.h"

#include <logger.h>

#include "../../../src/Game.h"

bool NeedsBiggerText__Fv(void) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(void) from %p", puEBP[1]);

  struct GGame* game = Get__5GGameFv();
  return game != NULL && NeedsBiggerText__5GGameFv(game);
}

int GetMidTextSize__Fv(void) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(void) from %p", puEBP[1]);

  return (int)(NeedsBiggerText__Fv()) + 22;
}

int GetSmallTextSize__Fv(void) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(void) from %p", puEBP[1]);

  return (int)(NeedsBiggerText__Fv()) + 20;
}
