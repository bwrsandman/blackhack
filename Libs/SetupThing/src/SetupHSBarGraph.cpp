#include "SetupHSBarGraph.h"

#include <stdlib.h>
#include <math.h>

#include <logger.h>

#include <LHSys.h>
#include <LH3DTech.h>
#include <Utils.h>

#include "Setup.h"
#include "SetupThing.h"

struct SetupHSBarGraph* __fastcall ct__15SetupHSBarGraphFiiiiiPw(
    struct SetupHSBarGraph* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\") from %p",
      this, id, x, y, width, height, label, puEBP[1]);
  ct__14SetupVBarGraphFiiiiiPw(
      &this->super, edx, id, x, y, width, height, label);
  this->super.super.super.vftptr = &vt__SetupHSBarGraph.super;

  return this;
}

void __fastcall Draw__15SetupHSBarGraphFbb(struct SetupHSBarGraph* this,
    const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  int text_size = GetTextSize__12SetupControlFv(&this->super.super.super);
  assert(this->super.min_point == 0.0f);
  float range = this->super.max_point - this->super.min_point;
  int width =
      this->super.super.super.rect.p1.x - this->super.super.super.rect.p0.x;

  DrawBevBox__10SetupThingFiiiiiiiUl(this->super.super.super.rect.p0.x,
      this->super.super.super.rect.p0.y, this->super.super.super.rect.p1.x,
      this->super.super.super.rect.p1.y, 1, 0x10, 0xffffffff, white);

  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
      this->super.super.super.rect.p0.x + 1,
      this->super.super.super.rect.p1.y + 1,
      this->super.super.super.rect.p1.x + 1,
      this->super.super.super.rect.p1.y + 1 + text_size,
      this->super.super.super.rect.p1.y + 1, true,
      this->super.super.super.label, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, true, false);

  const struct LH3DColor* p_color;
  if (hovered || (selected && this->super.super.super.label[0] == L'\0')) {
    p_color = &LH3DColor_ARRAY_00c4ccd8_3;
  }
  else {
    p_color = &LH3DColor_00c4ccf8;
  }
  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
      this->super.super.super.rect.p0.x, this->super.super.super.rect.p1.y,
      this->super.super.super.rect.p1.x,
      this->super.super.super.rect.p1.y + text_size,
      this->super.super.super.rect.p1.y, true, this->super.super.super.label,
      text_size, p_color, true, false);

  this->super.field_0x244.m[5] += GetDeltaTime__8LH3DTechFv() / 1000.0f;

  if (this->super.field_0x244.m[5] < this->super.field_0x244.m[6]) {
    float t2_2 =
        this->super.field_0x244.m[5] * this->super.field_0x244.m[5] * 0.5f;
    float t3_6 = this->super.field_0x244.m[5] * t2_2 / 3.0f;
    float t4_24 = t2_2 * t2_2 / 6.0f;
    this->super.field_0x244.m[3] =
        this->super.field_0x244.m[5] * this->super.field_0x244.m[9] +
        this->super.field_0x244.m[8] + this->super.field_0x244.m[10] * t2_2 +
        this->super.field_0x244.m[11] * t3_6;
    this->super.field_0x244.m[0] =
        this->super.field_0x244.m[5] * this->super.field_0x244.m[8] +
        this->super.field_0x244.m[7] + this->super.field_0x244.m[9] * t2_2 +
        this->super.field_0x244.m[10] * t3_6 +
        this->super.field_0x244.m[11] * t4_24;
  }
  else {
    this->super.field_0x244.m[0] = this->super.field_0x244.m[1];
    this->super.field_0x244.m[3] = this->super.field_0x244.m[2];
    this->super.field_0x244.m[4] = 0.0f;
    this->super.field_0x244.m[5] = this->super.field_0x244.m[6];
  }

  struct VBarDataListNode* walker =
      GetStart__VBarDataList(&this->super.bar_data_list);

  float local_204 = 0.0f;
  if (walker != NULL) {
    struct VBarData* payload = walker->payload;
    int current_x = this->super.super.super.rect.p0.x + 2;
    while (walker != NULL && payload != NULL) {
      local_204 += fabsf(payload->value);


      int x_max = (int)(this->super.field_0x244.m[0] * local_204 / range *
                            (float)(width - 4) +
                        (float)(this->super.super.super.rect.p0.x + 2));

      if (x_max <= this->super.super.super.rect.p0.x + 2) {
        x_max = this->super.super.super.rect.p0.x + 2;
      }
      else if (x_max >= this->super.super.super.rect.p1.x - 2) {
        x_max = this->super.super.super.rect.p1.x - 2;
      }

      struct LH3DColor color_max = {
        .b = payload->color.b,
        .g = payload->color.g,
        .r = payload->color.r,
        .a = 0xFF,
      };
      struct LH3DColor color_min = {
        .b = payload->color.b,
        .g = payload->color.g,
        .r = payload->color.r,
        .a = 0x7F,
      };

      if (payload->value < 0.0f) {
        struct LH3DColor tmp = color_min;
        color_min = color_max;
        color_max = tmp;
      }

      int y_min = this->super.super.super.rect.p0.y + 2;
      int y_mid = (this->super.super.super.rect.p0.y +
                      this->super.super.super.rect.p1.y) /
                  2;
      int y_max = this->super.super.super.rect.p1.y - 2;

      DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(current_x, y_min, x_max, y_mid,
          color_min, color_min, color_max, color_max, 0, 1);
      DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(current_x, y_mid, x_max, y_max,
          color_max, color_max, color_min, color_min, 0, 1);

      payload = walker->payload;
      walker = GetStart__VBarDataList(&this->super.bar_data_list);
      while (walker != NULL) {
        if (walker->payload == payload) {
          walker = walker->next;
          break;
        }
        walker = walker->next;
      }
      current_x = x_max;
    }
  }

  // Get tick spacing
  float spacing = 0.25f;
  while (range / spacing > (float)(width < 0 ? width + 1 : width) / 4) {
    spacing *= 2.0f;
  }

  struct LH3DColor color = white;

  // Draw tick marks
  for (uint32_t i = 1; i * spacing < this->super.max_point; ++i) {
    color.a = (uint8_t)((i % 4u) ? 0x10 : 0x50);
    int x_min = (int)((float)i * spacing / range * (float)(width - 4) +
                      (float)(this->super.super.super.rect.p0.x + 2));
    DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(x_min,
        this->super.super.super.rect.p0.y + 2, x_min + 1,
        this->super.super.super.rect.p1.y - 2, color, color, color, color, 0,
        1);
  }

  // Hovered seems to always be false
  if (!hovered) {
    return;
  }

  // Hovered value preview
  struct LHCoord mouse;
  ct__7LHCoordFRC7LHCoord(&mouse, edx, &Get__5LHSysFv()->mouse.coord_0xbc);
  unadjust__10SetupThingFRiRi(&mouse.x, &mouse.y);

  float value =
      saturatef((float)(this->super.super.super.rect.p1.y - mouse.y - 2) /
                (float)(width - 4)) *
          range +
      this->super.min_point;

  wchar_t value_preview[0x100];
  if (fabsf(range) > 10.0f) {
    swprintf_s(value_preview, _countof(value_preview), L"%d", (int)value);
  }
  else {
    swprintf_s(value_preview, _countof(value_preview), L"%0.1f", value);
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(mouse.x + 1,
      this->super.super.super.rect.p0.y - text_size - 1, 100,
      TEXTJUSTIFY_CENTRE, value_preview, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, 0);

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(mouse.x,
      this->super.super.super.rect.p0.y - text_size - 2, 100,
      TEXTJUSTIFY_CENTRE, value_preview, text_size, &LH3DColor_00c4ccf8,
      0);
}

void __fastcall SetScale__15SetupHSBarGraphFf(
    struct SetupHSBarGraph* this, const void* edx, float scale) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %f) from 0x%p", this, scale, puEBP[1]);

  if (scale <= 0.0f) {
    scale = 0.0f;
    for (struct VBarDataListNode* walker =
             GetStart__VBarDataList(&this->super.bar_data_list);
         walker != NULL; walker = walker->next) {
      scale += fabsf(walker->payload->value);
    }
  }

  if (scale <= 0.0f) {
    scale = 1.0f;
  }

  this->super.max_point = scale;
}

struct SetupHSBarGraph* __fastcall dt__15SetupHSBarGraphFb(
    struct SetupHSBarGraph* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__14SetupVBarGraphFb(&this->super, edx, param_1);

  return this;
}
