#ifndef BLACKHACK_SETUPPICTURE_H
#define BLACKHACK_SETUPPICTURE_H

#include "SetupButton.h"

#include <LH3DColor.h>
#include <Zoomer.h>

struct LH3DMaterial;

struct SetupPicture {
  struct SetupButton super;
  int hovered_picture_index;
  struct Zoomer zoomer;
  struct LH3DMaterial* material;
  struct LH3DColor tint;
  bool draggable;
  bool dragging;
  int picture_index;
  int num_rows;
  int num_pictures;
  bool clickable;
};
static_assert(sizeof(struct SetupPicture) == 0x294, "Struct is of wrong size");

// win1.41 0040f6b0 mac ???????? SetupPicture::MouseDown(int, int, bool)
void __fastcall MouseDown__12SetupPictureFiib(struct SetupPicture* this, const void* edx, int x, int y, bool param_3);
// win1.41 0040f840 mac 1036e5b0 SetupPicture::MouseUp(int, int, bool)
void __fastcall MouseUp__12SetupPictureFiib(struct SetupPicture* this, const void* edx, int x, int y, bool param_3);
// win1.41 0040fa10 mac 100e47c0 SetupPicture::Drag(int, int)
void __fastcall Drag__12SetupPictureFii(struct SetupPicture* this, const void* edx, int x, int y);
// win1.41 0040fa20 mac 104081c0 SetupPicture::Draw(bool, bool)
void __fastcall Draw__12SetupPictureFbb(struct SetupPicture *this, const void* edx, bool hovered, bool selected);
// win1.41 004105d0 mac 101a6a00 SetupPicture::SetupPicture(int, int, int, LH3DMaterial *, int, int, bool, int, bool)
struct SetupPicture * __fastcall ct__12SetupPictureFiiiP12LH3DMaterialiibib(
    struct SetupPicture *this, const void* edx, int id, int x, int y,
    struct LH3DMaterial *material, int picture_index, int num_rows, bool clickable,
    int size, bool draggable);
// win1.41 004106f0 mac 100fe9d0 SetupPicture::KeyDown(int, int)
void __fastcall KeyDown__12SetupPictureFii(struct SetupPicture* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 00410710 mac 10351210 SetupPicture::Click(int, int)
void __fastcall Click__12SetupPictureFii(struct SetupPicture* this, const void* edx, int x, int y);
// win1.41 00410720 mac 1034f1b0 SetupPicture::~SetupPicture(void)
struct SetupPicture * __fastcall dt__12SetupPictureFb(struct SetupPicture *this, const void* edx, bool param_1);
// win1.41 00410740 mac 102410c0 SetupPicture::SetFocus(bool)
void __fastcall SetFocus__12SetupPictureFb(struct SetupPicture *this, const void* edx, bool focus);

// win1.41 008ab518 mac none SetupPicture virtual table
static struct vt__SetupControl_t vt__SetupPicture = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = (vt__SetupControl_SetFocus_t*)SetFocus__12SetupPictureFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__12SetupPictureFbb,
    .Drag = (vt__SetupControl_Drag_t*)Drag__12SetupPictureFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__12SetupPictureFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__12SetupPictureFiib,
    .Click = (vt__SetupControl_Click_t*)Click__12SetupPictureFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__12SetupPictureFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__12SetupPictureFb,
};


#endif // BLACKHACK_SETUPPICTURE_H
