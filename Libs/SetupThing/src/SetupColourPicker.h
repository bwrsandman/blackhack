#ifndef BLACKHACK_SETUPCOLOURPICKER_H
#define BLACKHACK_SETUPCOLOURPICKER_H

#include <SetupButton.h>

#include <LH3DColor.h>

struct LH3DMaterial;

struct SetupColourPicker {
  struct SetupButton super;
  struct LH3DColor color_0x244;
  struct LH3DMaterial* material;
  bool brightness_slider;
  float slider_position;
  struct LH3DColor color;
};

// win1.41 004107f0 mac 1030b070 SetupColourPicker::MouseDown(int, int, bool)
void __fastcall MouseDown__17SetupColourPickerFiib(struct SetupColourPicker* this, const void* edx, int x, int y, bool param_3);
// win1.41 00410800 mac 101119a0 SetupColourPicker::MouseUp(int, int, bool)
void __fastcall MouseUp__17SetupColourPickerFiib(struct SetupColourPicker* this, const void* edx, int x, int y, bool param_3);
// win1.41 00410810 mac 1023f300 SetupColourPicker::Drag(int, int)
void __fastcall Drag__17SetupColourPickerFii(struct SetupColourPicker* this, const void* edx, int x, int y);
// win1.41 00410880 mac 10103e40 SetupColourPicker::Draw(bool, bool)
void __fastcall Draw__17SetupColourPickerFbb(struct SetupColourPicker* this, const void* edx, bool hovered, bool selected);
// win1.41 00410ac0 mac 103c6130 SetupColourPicker::SetupColourPicker(int, int, int, int, int, int, LH3DMaterial *)
struct SetupColourPicker * __fastcall ct__17SetupColourPickerFiiiiiiP12LH3DMaterial(
    struct SetupColourPicker *this, const void* edx, int id,int x,int y,int width,int height,
    bool brightness_slider, struct LH3DMaterial *material);
// win1.41 00410b30 mac 1034f250 SetupColourPicker::KeyDown(int, int)
void __fastcall KeyDown__17SetupColourPickerFii(struct SetupColourPicker* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 00410b50 mac 100c8de0 SetupColourPicker::Click(int, int)
void __fastcall Click__17SetupColourPickerFii(struct SetupColourPicker* this, const void* edx, int x, int y);
// win1.41 00410b60 mac 10571d70 SetupColourPicker::~SetupColourPicker(void)
struct SetupColourPicker * __fastcall dt__17SetupColourPickerFb(struct SetupColourPicker *this, const void* edx, bool param_1);

// win1.41 008ab550 mac none SetupColourPicker virtual table
static struct vt__SetupControl_t vt__SetupColourPicker = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__17SetupColourPickerFbb,
    .Drag = (vt__SetupControl_Drag_t*)Drag__17SetupColourPickerFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__17SetupColourPickerFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__17SetupColourPickerFiib,
    .Click = (vt__SetupControl_Click_t*)Click__17SetupColourPickerFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__17SetupColourPickerFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__17SetupColourPickerFb,
};

#endif // BLACKHACK_SETUPCOLOURPICKER_H
