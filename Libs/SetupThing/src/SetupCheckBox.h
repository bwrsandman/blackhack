#ifndef BLACKHACK_SRC_SETUPCHECKBOX_H
#define BLACKHACK_SRC_SETUPCHECKBOX_H

#include "SetupButton.h"

struct SetupCheckBox {
  struct SetupButton super;
  uint32_t text_position;
  enum BBSTYLE style;
  bool checked;
  // TODO: "inner" here is just a guess, definitely is a bounding box
  struct SetupRect inner_rect;
};

// win1.41 00410b80 mac 103c4a20 SetupCheckBox::Click(int, int)
void __fastcall Draw__13SetupCheckBoxFbb(struct SetupCheckBox* this, const void* edx, bool hovered, bool selected);
// win1.41 00410f10 mac 1058b890 SetupCheckBox::SetupCheckBox(int, int, int, bool, int, wchar_t *, int)
struct SetupCheckBox * __fastcall ct__13SetupCheckBoxFiiibiPwi(
    struct SetupCheckBox *this, const void* edx, int id, int x, int y,
    bool checked, enum BBSTYLE style, const wchar_t *label, int size);
// win1.41 00410f90 mac 10112370 SetupCheckBox::HitTest(int, int)
bool __fastcall HitTest__13SetupCheckBoxFii(struct SetupCheckBox *this, const void* edx, int x, int y);
// win1.41 00411020 mac 103dbde0 SetupCheckBox::Click(int, int)
void __fastcall Click__13SetupCheckBoxFii(struct SetupCheckBox* this, const void* edx, int x, int y);
// win1.41 00411050 mac 10599580 SetupCheckBox::KeyDown(int, int)
void __fastcall KeyDown__13SetupCheckBoxFii(struct SetupCheckBox* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 00411070 mac 105893e0 SetupCheckBox::~SetupCheckBox(void)
struct SetupCheckBox* __fastcall dt__13SetupCheckBoxFb(struct SetupCheckBox *this, const void* edx, bool param_1);

// win1.41 008ab588 mac none SetupCheckBox virtual table
static struct vt__SetupControl_t vt__SetupCheckBox = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = (vt__SetupControl_HitTest_t*)HitTest__13SetupCheckBoxFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__13SetupCheckBoxFbb,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__10SetupButtonFiib,
    .Click = (vt__SetupControl_Click_t*)Click__13SetupCheckBoxFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__13SetupCheckBoxFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__13SetupCheckBoxFb,
};

#endif // BLACKHACK_SRC_SETUPCHECKBOX_H
