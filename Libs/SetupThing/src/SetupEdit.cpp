#include "SetupEdit.h"

#include <stdlib.h>

#include <logger.h>

#include <GatheringText.h>
#include <LHSys.h>
#include <Memory.h>
#include <Utils.h>

#include "Setup.h"
#include "SetupList.h"
#include "SetupThing.h"

void __fastcall ClampCursor__9SetupEditFi(struct SetupEdit* this) {
  size_t len = wcslen(this->super.label);

  this->select_start = clampi(this->select_start, 0, len);
  this->select_end = clampi(this->select_end, 0, len);
  this->cursor_position = clampi(this->cursor_position, 0, len);
  this->field_0x258 = clampi(this->field_0x258, 0, len);
}

void __fastcall FixCursor__9SetupEditFi(struct SetupEdit* this) {
  this->super.label[this->field_0x240] = L'\0';
  this->text[this->field_0x240] = L'\0';

  if (this->masked_text) {
    size_t len = wcslen(this->super.label);
    for (size_t i = 0; i < len; ++i) {
      this->text[i] = L'#';
    }
    this->text[len] = L'\0';
  }
  else {
    wcscpy_s(this->text, _countof(this->text), this->super.label);
  }

  this->field_0x244 = this->cursor_position;
  this->field_0x248 = this->cursor_position;

  if (Get__5LHSysFv()->field_0x70c4 != NULL && this->super.focus &&
      Get__10SetupThingFv()->DAT_00c4cd00 && this->cursor_position > -1) {
    if (this->cursor_position <= (int)wcslen(this->text)) {
      wchar_t* str =
          Composition_Get__Q24slim5TbIMEFv(Get__5LHSysFv()->field_0x70c4);
      if (str != NULL) {
        wchar_t buffer[0x100];
        wcscpy_s(buffer, _countof(buffer), &this->text[this->cursor_position]);
        wcscpy_s(&this->text[this->cursor_position],
            _countof(this->text) - this->cursor_position, str);
        wcscat_s(this->text, _countof(this->text), buffer);
        this->field_0x248 += (int)wcslen(str);
      }

      struct SetupRect rect = {
        .p0 = { .x = this->super.rect.p1.x - 150 },
        .p1 = { .x = this->super.rect.p1.x },
      };

      this->field_0x23c->super.text_size = GetSmallTextSize__Fv();
      this->field_0x23c->scrollback_width = 10;

      if (this->super.rect.p1.y < 400) {
        rect.p0.y = this->super.rect.p1.y;
        rect.p1.y = this->field_0x23c->super.rect.p0.y + 150;
      }
      else {
        rect.p0.y = this->field_0x23c->super.rect.p1.y - 150;
        rect.p1.y = this->super.rect.p0.y;
      }

      this->field_0x23c->super.rect = rect;
    }
  }
}

void __fastcall FixSelect__9SetupEditFi(struct SetupEdit* this) {
  if (this->select_end < this->select_start) {
    this->select_start = this->select_end;
    this->select_end = this->select_start;
  }

  if (this->field_0x258 > this->field_0x248) {
    this->field_0x258 = this->field_0x248;
  }
}

void __fastcall Char__9SetupEditFi(
    struct SetupEdit* this, const void* edx, int character) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, 0x%04x) from %p", this, character, puEBP[1]);

  if (!this->editable) {
    return;
  }

  if (character == 8) {
    if (this->select_start != this->select_end) {
      size_t len = wcslen(this->super.label);
      int select_start = clampi(this->select_start, 0, len);
      int select_end = clampi(this->select_end, 0, len);

      memmove(&this->super.label[select_start], &this->super.label[select_end],
          (select_end - len) * sizeof(this->super.label[0]));
      this->super.label[len - select_end + select_start] = L'\0';
      this->select_end = this->select_start;
      this->cursor_position = this->select_start;
    }
    else if (this->cursor_position > 0) {
      size_t len = wcslen(this->super.label);
      int cursor_position_m1 = clampi(this->cursor_position - 1, 0, len);
      int cursor_position = clampi(this->cursor_position, 0, len);

      memmove(&this->super.label[cursor_position_m1],
          &this->super.label[cursor_position],
          (cursor_position - len) * sizeof(this->super.label[0]));
      this->super.label[len - cursor_position + cursor_position_m1] = L'\0';

      --this->cursor_position;
      this->select_start = this->cursor_position;
      this->select_end = this->cursor_position;
    }
  }
  else if (character == 0xd) {
    if (this->super.setup_box != NULL &&
        this->super.setup_box->field_0xb0 != NULL) {
      this->super.setup_box->field_0xb0(
          1, this->super.setup_box, &this->super, 0, 0);
      SetFocusNext__8SetupBoxFv(this->super.setup_box);
    }
  }
  else if (character > 0x1f) {
    if (this->select_start != this->select_end) {
      size_t len = wcslen(this->super.label);
      int select_start = clampi(this->select_start, 0, len);
      int select_end = clampi(this->select_end, 0, len);

      memmove(&this->super.label[select_start], &this->super.label[select_end],
          (select_end - len) * sizeof(this->super.label[0]));
      this->super.label[len - select_end + select_start] = L'\0';
      this->select_end = this->select_start;
      this->cursor_position = this->select_start;
    }

    if (character != 0) {
      int len = (int)wcslen(this->super.label);

      if (this->field_0x240 > len) {
        int cursor_position = clampi(this->cursor_position, 0, len);

        memmove(&this->super.label[cursor_position + 1],
            &this->super.label[cursor_position],
            (cursor_position - len) * sizeof(this->super.label[0]));
        this->super.label[cursor_position] = (wchar_t)character;
        this->super.label[len + 1] = L'\0';
        this->super.label[this->field_0x240] = L'\0';
      }
      this->cursor_position++;
      this->select_end = this->cursor_position;
      this->select_start = this->cursor_position;
    }

    ClampCursor__9SetupEditFi(this);
    FixCursor__9SetupEditFi(this);
    FixSelect__9SetupEditFi(this);
  }
}


void __fastcall KeyDown__9SetupEditFii(struct SetupEdit* this, const void* edx,
    enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (!this->editable) {
    return;
  }

  switch (key) {
  case LHKEY_TAB:
    if (this->super.setup_box != NULL &&
        this->super.setup_box->field_0xb0 != NULL) {
      this->super.setup_box->field_0xb0(
          1, this->super.setup_box, &this->super, 0, 0);
      if ((mod & LHKEYMOD_SHIFT) == 0) {
        SetFocusNext__8SetupBoxFv(this->super.setup_box);
      }
      else {
        SetFocusPrev__8SetupBoxFv(this->super.setup_box);
      }
    }
    break;

  default:
    break;

  case LHKEY_HOME:
    this->cursor_position = 0;
    if ((mod & LHKEYMOD_SHIFT) == 0) {
      this->select_end = 0;
    }
    this->select_start = 0;
    break;

  case LHKEY_END:
    this->cursor_position = wcslen(this->super.label);
    this->select_end = this->cursor_position;
    if ((mod & LHKEYMOD_SHIFT) == 0) {
      this->select_start = this->cursor_position;
    }
    break;

  case LHKEY_LEFT:
    --this->cursor_position;
    if ((mod & LHKEYMOD_SHIFT) == 0) {
      this->select_start = this->cursor_position;
      this->select_end = this->cursor_position;
    }
    else if (this->select_start == this->select_end ||
             this->cursor_position == this->select_start - 1) {
      this->select_start = this->cursor_position;
    }
    else {
      this->select_end = this->cursor_position;
    }
    break;

  case LHKEY_RIGHT:
    ++this->cursor_position;
    if ((mod & LHKEYMOD_SHIFT) == 0) {
      this->select_start = this->cursor_position;
      this->select_end = this->cursor_position;
    }
    else if (this->select_start == this->select_end ||
             this->cursor_position != this->select_start + 1) {
      this->select_end = this->cursor_position;
    }
    else {
      this->select_start = this->cursor_position;
    }
    break;

  case LHKEY_DELETE: {
    int delete_start = this->select_start;
    int delete_end = this->select_end;

    if (delete_start == delete_end) {
      delete_start = this->cursor_position;
      delete_end = delete_start + 1;
    }

    size_t len = wcslen(this->super.label);
    delete_start = clampi(delete_start, 0, len);
    delete_end = clampi(delete_end, 0, len);

    memmove(&this->super.label[delete_start], &this->super.label[delete_end],
        (delete_end - len) * sizeof(this->super.label[0]));

    this->super.label[len - delete_start + delete_end] = L'\0';

    if (this->select_end == this->select_start) {
      this->select_start = this->cursor_position;
      this->select_end = this->cursor_position;
    }
    else {
      this->cursor_position = this->select_start;
      this->select_end = this->select_start;
    }
  } break;
  }

  ClampCursor__9SetupEditFi(this);
  FixCursor__9SetupEditFi(this);
  FixSelect__9SetupEditFi(this);
}

int __fastcall CalcCharpos__9SetupEditFi(
    struct SetupEdit* this, const void* edx, int pos) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %d) from %p", this, pos, puEBP[1]);

  int local_4 = (int)wcslen(this->super.label);

  for (int i = 1; i <= (int)wcslen(this->super.label) - this->field_0x258; ++i) {
    int text_size = GetTextSize__12SetupControlFv(&this->super);
    float width = GetStringWidth__13GatheringTextFPwif(Get__10SetupThingFv()->font,
        edx, this->super.label + this->field_0x258, i, (float)text_size);
    if (pos < this->super.rect.p0.x + 4 + (int)width) {
      local_4 = i - 1;
      break;
    }
  }

  return this->field_0x258 + local_4;
}

void __fastcall Drag__9SetupEditFii(
    struct SetupEdit* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);

  int pos = CalcCharpos__9SetupEditFi(this, edx, x);
  this->cursor_position = pos;
  this->select_start = pos;
}

void __fastcall MouseDown__9SetupEditFiib(
    struct SetupEdit* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  if (!param_3) {
    return;
  }

  int pos = CalcCharpos__9SetupEditFi(this, edx, x);
  this->cursor_position = pos;
  this->select_end = pos;
  this->select_start = pos;
}

void __fastcall MouseUp__9SetupEditFiib(
    struct SetupEdit* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  if (!param_3) {
    return;
  }

  int pos = CalcCharpos__9SetupEditFi(this, edx, x);

  this->cursor_position = pos;
  this->select_start = pos;

  if (this->select_end < pos) {
    this->select_start = this->select_end;
    this->select_end = pos;
  }

  if (this->select_start == this->select_end && this->field_0x464 != 0) {
    this->select_start = 0;
    size_t len = wcslen(this->super.label);
    this->cursor_position = len;
    this->select_end = len;
  }

  this->field_0x464 = 0;
}

struct SetupEdit* __fastcall ct__9SetupEditFiiiiiPwi(struct SetupEdit* this,
    const void* edx, int id, int x, int y, int width, int height,
    const wchar_t* label, bool editable) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\", %s) from %p",
      this, id, x, y, width, height, label, editable ? "true" : "false",
      puEBP[1]);

  ct__12SetupControlFiiiiiPw(&this->super, edx, id, x, y, width, height, label);

  this->super.vftptr = &vt__SetupEdit;
  this->field_0x240 = 0xfe;
  this->editable = editable;
  this->cursor_position = 0;
  this->select_start = 0;
  this->select_end = 0;
  this->masked_text = false;

  this->field_0x23c = (struct SetupList*)FindControl__8SetupBoxFi(
      this->super.setup_box, edx, 0x85b680);
  if (this->field_0x23c == NULL) {
    this->field_0x23c = ct__9SetupListFiiiii(
        __nw__FUl(sizeof(struct SetupList), __FILE__, __LINE__), edx, 0x85b680,
        0, 0, 50, 50);
    this->field_0x23c->super.vftptr->Hide(&this->field_0x23c->super, edx, true);
    this->field_0x23c->super.field_0x4 = 1;
    this->field_0x23c->field_0x284 = true;
  }
  else {
    for (int i = this->field_0x23c->num_items; i > 0;
         i = this->field_0x23c->num_items) {
      DeleteString__9SetupListFi(this->field_0x23c, edx, i - 1);
    }
    this->field_0x23c->super.vftptr->Hide(&this->field_0x23c->super, edx, true);
  }
  this->super.field_0x4 = 1;
  this->super.field_0x1c = 0;

  FixCursor__9SetupEditFi(this);
  FixSelect__9SetupEditFi(this);

  return this;
}

void __fastcall SetFocus__9SetupEditFb(
    struct SetupEdit* this, const void* edx, bool focus) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, focus=\"%s\") from %p", this,
      focus ? "true" : "false", puEBP[1]);

  if (focus && !this->super.focus) {
    this->field_0x464 = 1;
  }
  SetFocus__12SetupControlFb(&this->super, edx, focus);
  size_t length = wcslen(this->super.label);
  this->cursor_position = length;
  this->select_end = length;
  this->select_start = length;
  this->field_0x258 = 0;
  if (focus != false) {
    this->select_start = 0;
  }
}

struct SetupEdit* __fastcall dt__9SetupEditFb(
    struct SetupEdit* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__12SetupControlFb(&this->super, edx, param_1);

  return this;
}

void __fastcall Draw__9SetupEditFbb(
    struct SetupEdit* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };
  const struct LH3DColor cursor_color = {
    .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0x80
  };

  FixCursor__9SetupEditFi(this);

  if (!this->editable) {
    *GetDrawAlpha__10SetupThingFv() /= 2;
  }
  DrawBevBox__10SetupThingFiiiiiiiUl(this->super.rect.p0.x,
      this->super.rect.p0.y, this->super.rect.p1.x, this->super.rect.p1.y, 1,
      0x10, 0xffffffff, white);
  if (!this->editable) {
    *GetDrawAlpha__10SetupThingFv() *= 2;
  }

  int len = (int)wcslen(this->text);

  int current_char;
  for (current_char = 0; current_char < len; ++current_char) {
    int text_size = GetTextSize__12SetupControlFv(&this->super);
    float fVar13 = GetStringWidth__13GatheringTextFPwif(
        Get__10SetupThingFv()->font, edx, &this->text[current_char],
        (int)len - current_char, (float)text_size);
    if ((int)fVar13 < this->super.rect.p1.x - this->super.rect.p0.x - 9) {
      break;
    }
  }

  this->field_0x258 =
      min(clampi(this->field_0x258, 0, current_char), this->field_0x248);

  while (true) {
    int local_214 = GetTextSize__12SetupControlFv(&this->super);
    float fVar13 = GetStringWidth__13GatheringTextFPwif(Get__10SetupThingFv()->font,
        edx, &this->text[this->field_0x258],
        this->field_0x248 - this->field_0x258, (float)local_214);
    int position = (int)fVar13;
    int width = this->super.rect.p1.x - this->super.rect.p0.x;
    if (position < width - 9 || this->field_0x258 >= current_char) {
      break;
    }
    ++this->field_0x258;
  }

  DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
      this->super.rect.p0.x + 4,
      (this->super.rect.p0.y + this->super.rect.p1.y) / 2 -
          GetTextSize__12SetupControlFv(&this->super) / 2,
      this->super.rect.p1.x - this->super.rect.p0.x - 8, TEXTJUSTIFY_LEFT,
      this->text + this->field_0x258,
      GetTextSize__12SetupControlFv(&this->super), &LH3DColor_ARRAY_00c4ccd8_4,
      0);

  if (!selected) {
    return;
  }

  int size = max(this->select_end, this->field_0x258);
  int selection_end = max(this->select_start, this->field_0x258);
  int text_start = max(this->field_0x244, this->field_0x258);
  int text_end = max(this->field_0x248, this->field_0x258);

  int selection_box_min_x;
  {
    int local_218 = GetTextSize__12SetupControlFv(&this->super);
    float fVar13 = GetStringWidth__13GatheringTextFPwif(Get__10SetupThingFv()->font,
        edx, &this->text[this->field_0x258], size - this->field_0x258,
        (float)local_218);
    selection_box_min_x = (int)fVar13;
  }

  int iVar3;
  {
    int local_218 = GetTextSize__12SetupControlFv(&this->super);
    float fVar13 = GetStringWidth__13GatheringTextFPwif(Get__10SetupThingFv()->font,
        edx, &this->text[this->field_0x258], selection_end - this->field_0x258,
        (float)local_218);
    iVar3 = (int)fVar13;
  }

  int padding_x;
  {
    int local_218 = GetTextSize__12SetupControlFv(&this->super);
    float fVar13 = GetStringWidth__13GatheringTextFPwif(Get__10SetupThingFv()->font,
        edx, &this->text[this->field_0x258], text_start - this->field_0x258,
        (float)local_218);
    padding_x = (int)fVar13;
  }

  int cursor_x;
  {
    int local_218 = GetTextSize__12SetupControlFv(&this->super);
    float fVar13 = GetStringWidth__13GatheringTextFPwif(Get__10SetupThingFv()->font,
        edx, &this->text[this->field_0x258], text_end - this->field_0x258,
        (float)local_218);
    cursor_x = (int)fVar13;
  }

  if (iVar3 < selection_box_min_x) {
    // swap(iVar3, selection_box_min_x)
    int tmp = selection_box_min_x;
    selection_box_min_x = iVar3;
    iVar3 = tmp;
  }

  int width = this->super.rect.p1.x - this->super.rect.p0.x - 8;
  selection_box_min_x = min(selection_box_min_x, width);
  int selection_box_max_x = min(width, iVar3 + 1);

  int selection_start = size;
  if (selection_start > selection_end) {
    // swap(selection_start, selection_end)
    int tmp = selection_start;
    selection_start = selection_end;
    selection_end = tmp;
  }

  if (selection_box_min_x < selection_box_max_x && this->editable) {
    // Draw cursor
    uint32_t tick = GetTickCount();
    if (tick / 200u % 2 && cursor_x > -1) {
      if (this->super.rect.p1.x - this->super.rect.p0.x - 8 > cursor_x) {
        DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(
            this->super.rect.p0.x + 5 + cursor_x, (this->super).rect.p0.y + 2,
            this->super.rect.p0.x + 7 + cursor_x, (this->super).rect.p1.y - 2,
            cursor_color, cursor_color, cursor_color, cursor_color, 1, 1);
      }
    }

    // Draw selection
    if (selection_start != selection_end) {
      DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(
          this->super.rect.p0.x + 4 + selection_box_min_x,
          this->super.rect.p0.y + 2,
          this->super.rect.p0.x + 5 + selection_box_max_x,
          this->super.rect.p1.y - 2, cursor_color, cursor_color, cursor_color,
          cursor_color, 1, 1);

      size = GetTextSize__12SetupControlFv(&this->super);
      width = GetTextSize__12SetupControlFv(&this->super) / 2;

      DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
          this->super.rect.p0.x + 4 + selection_box_min_x,
          (this->super.rect.p0.y + this->super.rect.p1.y) / 2 - width,
          this->super.rect.p1.x - this->super.rect.p0.x - selection_box_min_x -
              8,
          TEXTJUSTIFY_LEFT, &this->text[selection_start], size,
          &LH3DColor_ARRAY_00c4ccd8_2, selection_end - selection_start);
    }
  }

  if (text_start < text_end) {
    size = GetTextSize__12SetupControlFv(&this->super);
    width = GetTextSize__12SetupControlFv(&this->super) / 2;

    DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(
        this->super.rect.p0.x + 4 + padding_x,
        (this->super.rect.p0.y + this->super.rect.p1.y) / 2 - width,
        this->super.rect.p1.x - this->super.rect.p0.x - padding_x - 8,
        TEXTJUSTIFY_LEFT, &this->text[text_start], size,
        &LH3DColor_ARRAY_00c4ccd8_3, text_end - text_start);

    if (this->super.setup_box != NULL) {
      this->field_0x23c = (struct SetupList*)FindControl__8SetupBoxFi(
          this->super.setup_box, edx, 0x85b680);
    }
    else {
      this->field_0x23c = NULL;
    }

    if (this->field_0x23c != NULL) {
      this->field_0x23c->super.rect.p1.x -= this->field_0x23c->super.rect.p0.x;
      this->field_0x23c->super.rect.p0.x =
          this->super.rect.p0.x + padding_x + 4;
      this->field_0x23c->super.rect.p1.x += this->field_0x23c->super.rect.p0.x;
    }
  }
}
