#ifndef BLACKHACK_SETUPSLIDER_H
#define BLACKHACK_SETUPSLIDER_H

#include "SetupControl.h"

#include <assert.h>

struct SetupSlider {
  struct SetupControl super;
  float value;
  float drag_start_value;
  struct LHCoord drag_start;
  int height;
};
static_assert(sizeof(struct SetupSlider) == 0x250, "struct size is wrong");

// win1.41 00409960 mac 10103d10 SetupSlider::KeyDown(int, int)
void __fastcall KeyDown__11SetupSliderFii(struct SetupSlider* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 00409a40 mac 10440200 SetupSlider::Draw(bool, bool)
void __fastcall Draw__11SetupSliderFbb(struct SetupSlider *this, const void* edx, bool hovered, bool selected);
// win1.41 00409bf0 mac 1043fbc0 SetupSlider::SetupSlider(int, int, int, int, int, float, wchar_t *)
struct SetupSlider* __fastcall ct__11SetupSliderFiiiiifPw(
    struct SetupSlider* this, void* edx, int id, int x, int y, int width, int height, float value, wchar_t *label);
// win1.41 00409c50 mac 101c8450 SetupSlider::~SetupSlider(void)
struct SetupSlider* __fastcall dt__11SetupSliderFb(struct SetupSlider* this, const void* edx, bool param_1);
// win1.41 00409c70 mac 1043ff90 SetupSlider::Drag(int, int)
void __fastcall Drag__11SetupSliderFii(struct SetupSlider* this, const void* edx, int x, int y);
// win1.41 00409d60 mac 1043c240 SetupSlider::MouseDown(int, int, bool)
void __fastcall MouseDown__11SetupSliderFiib(struct SetupSlider *this, const void* edx, int x, int y, bool param_3);
// win1.41 00409d90 mac 100b4690 SetupSlider::MouseUp(int, int, bool)
void __fastcall MouseUp__11SetupSliderFiib(struct SetupSlider* this, const void* edx, int x, int y, bool param_3);

// win1.41 008ab2ec mac none SetupSlider virtual table
static struct vt__SetupControl_t vt__SetupSlider = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__11SetupSliderFbb,
    .Drag = (vt__SetupControl_Drag_t*)Drag__11SetupSliderFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__11SetupSliderFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__11SetupSliderFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__11SetupSliderFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__11SetupSliderFb,
};

#endif // BLACKHACK_SETUPSLIDER_H
