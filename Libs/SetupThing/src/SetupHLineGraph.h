#ifndef BLACKHACK_SETUPHLINEGRAPH_H
#define BLACKHACK_SETUPHLINEGRAPH_H

#include "SetupButton.h"

#include <LH3DColor.h>

struct HLineData {
  struct LH3DColor color;
  int point_count;
  float* points;
};
static_assert(sizeof(struct HLineData) == 0xc, "Struct is of wrong size");

// inlined HLineData::HLineData(void)
struct HLineData * __fastcall ct__9HLineData(struct HLineData *this);
// inlined HLineData::operator=(const HLineData&)
struct HLineData * __fastcall as__9HLineDataFRC9HLineData(struct HLineData *this, const void* edx, const struct HLineData *other);
// win1.41 0040da30 mac 10501060 HLineData::SetNum()
void __fastcall SetNum__9HLineDataFi(struct HLineData *this, const void* edx, int num);

struct HLineDataListNode {
  struct HLineDataListNode* next;
  struct HLineData* payload;
};
static_assert(sizeof(struct HLineDataListNode) == 0x8, "Struct is of wrong size");
// win1.41 inlined mac 103ad510 LHLinkedNode<HLineData*>::LHLinkedNode<HLineData*>(HLineData*, LHLinkedNode<HLineData>*)
struct HLineDataListNode * __fastcall ct__HLineDataListNode(struct HLineDataListNode*this, const void* edx, struct HLineData* payload, struct HLineDataListNode* next);

struct HLineDataList {
  struct HLineDataListNode* head;
  int count;
};
// win1.41 inlined mac 102a79c0 LHLinkedList<HLineData*>::GetStart() const
struct HLineDataListNode* __fastcall GetStart__HLineDataList(const struct HLineDataList*this);
// win1.41 inlined mac 10377aa0 LHLinkedList<HLineData*>::GetNodeAtPosition() const
struct HLineDataListNode* __fastcall GetNodeAtPosition__HLineDataList(const struct HLineDataList*this, const void* edx, int index);
// win1.41 inlined mac 101bee70 LHLinkedList<HLineData*>::GetLastNode()
struct HLineDataListNode* __fastcall GetLastNode__HLineDataList(const struct HLineDataList*this);
// inlined
void __fastcall PushBack__HLineDataList(struct HLineDataList*this, const void* edx, struct HLineData* payload);

struct SetupHLineGraph {
  struct SetupButton super;
  struct HLineDataList line_data_list;
  float max_point;
  float min_point;
  bool percent_mode;
};
static_assert(sizeof(struct SetupHLineGraph) == 0x258, "Struct is of wrong size");

// win1.41 0040dab0 mac 101180e0 SetupHLineGraph::Draw(bool, bool)
void __fastcall Draw__15SetupHLineGraphFbb(struct SetupHLineGraph* this, const void* edx, bool hovered, bool selected);
// win1.41 0040e510 mac 103dcbb0 SetupHLineGraph::SetupHLineGraph(int, int, int, int, int, wchar_t *, bool)
struct SetupHLineGraph* __fastcall ct__15SetupHLineGraphFiiiiiPwb(struct SetupHLineGraph *this, const void* edx, int id, int x, int y, int width, int height, const wchar_t *label, bool percent_mode);
// win1.41 0040e580 mac 10518860 SetupHLineGraph::KeyDown(int, int)
void __fastcall KeyDown__15SetupHLineGraphFii(struct SetupHLineGraph* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 0040e5a0 mac 101585b0 SetupHLineGraph::MouseUp(int, int, bool)
void __fastcall MouseUp__15SetupHLineGraphFiib(struct SetupHLineGraph* this, const void* edx, int x, int y, bool param_3);
// win1.41 0040e5c0 mac 0040e5c0 SetupHLineGraph::~SetupHLineGraph(void)
struct SetupHLineGraph * __fastcall dt__15SetupHLineGraphFb(struct SetupHLineGraph *this, const void* edx, bool param_1);
// win1.41 0040e5e0 mac 102a7a10 SetupHLineGraph::Reset(void)
void __fastcall Reset__15SetupHLineGraphFv(struct SetupHLineGraph *this);
// win1.41 0040e650 mac 10211b80 SetupHLineGraph::SetScale(float, float, bool)
void __fastcall SetScale__15SetupHLineGraphFffb(struct SetupHLineGraph *this, const void* edx, float max_point, float min_point, bool centered_at_zero);
// win1.41 0040e730 mac 1010ccb0 SetupHLineGraph::SetScale(float, float, bool)
void __fastcall AddLine__15SetupHLineGraphFR9HLineData(struct SetupHLineGraph *this, const void* edx, const struct HLineData *line);
// win1.41 0040e7f0 mac 100c9eb0 SetupHLineGraph::SetLine(int, const HLineData &)
void __fastcall SetLine__15SetupHLineGraphFiR9HLineData(struct SetupHLineGraph *this, const void* edx, int index, const struct HLineData * line);
// win1.41 0040e850 mac 10372050 SetupHLineGraph::GetLine(int, HLineData &)
void __fastcall GetLine__15SetupHLineGraphFiR9HLineData(const struct SetupHLineGraph *this, const void* edx, int index, struct HLineData * result);

typedef void (__fastcall vt_SetupHLineGraph_Reset_t)(struct SetupHLineGraph *this);
typedef void (__fastcall vt_SetupHLineGraph_SetScale_t)(struct SetupHLineGraph *this, const void* edx, float max_point, float min_point, bool centered_at_zero);
typedef void (__fastcall vt_SetupHLineGraph_AddLine_t)(struct SetupHLineGraph *this, const void* edx, const struct HLineData *line);
typedef void (__fastcall vt_SetupHLineGraph_SetLine_t)(struct SetupHLineGraph *this, const void* edx, int index, const struct HLineData *line);
typedef void (__fastcall vt_SetupHLineGraph_GetLine_t)(const struct SetupHLineGraph *this, const void* edx, int index, struct HLineData *result);

struct vt__SetupHLineGraph_t {
  const struct vt__SetupControl_t super;
  vt_SetupHLineGraph_Reset_t * const Reset;
  vt_SetupHLineGraph_SetScale_t * const SetScale;
  vt_SetupHLineGraph_AddLine_t * const AddLine;
  vt_SetupHLineGraph_SetLine_t * const SetLine;
  vt_SetupHLineGraph_GetLine_t * const GetLine;
};

// win1.41 008ab424 mac none SetupHLineGraph virtual table
static struct vt__SetupHLineGraph_t vt__SetupHLineGraph = {
    .super = {
        .SetToolTipUl = SetToolTip__12SetupControlFUl,
        .SetToolTipPw = SetToolTip__12SetupControlFPw,
        .Hide = Hide__12SetupControlFb,
        .SetFocus = SetFocus__12SetupControlFb,
        .HitTest = HitTest__12SetupControlFii,
        .Draw = (vt__SetupControl_Draw_t*)Draw__15SetupHLineGraphFbb,
        .Drag = Drag__12SetupControlFii,
        .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
        .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__15SetupHLineGraphFiib,
        .Click = Click__12SetupControlFii,
        .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__15SetupHLineGraphFii,
        .Char = Char__12SetupControlFi,
        .dtor = (vt__SetupControl_dtor_t*)dt__15SetupHLineGraphFb,
    },
    .Reset = Reset__15SetupHLineGraphFv,
    .SetScale = SetScale__15SetupHLineGraphFffb,
    .AddLine = AddLine__15SetupHLineGraphFR9HLineData,
    .SetLine = SetLine__15SetupHLineGraphFiR9HLineData,
    .GetLine = GetLine__15SetupHLineGraphFiR9HLineData,
};

#endif // BLACKHACK_SETUPHLINEGRAPH_H
