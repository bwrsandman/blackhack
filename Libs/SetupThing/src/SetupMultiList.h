#ifndef BLACKHACK_SETUPMULTILIST_H
#define BLACKHACK_SETUPMULTILIST_H

#include "SetupList.h"

struct SetupMultiList {
  struct SetupList super;
  bool* list;
  int field_0x2b4;
  int size;
};
static_assert(sizeof(struct SetupList) == 0x2b0, "struct size is wrong");

// win1.41 0040b420 mac 1014cca0 SetupMultiList::SetupMultiList(int, int, int, int, int, int)
struct SetupMultiList * __fastcall
ct__14SetupMultiListFiiiiii(struct SetupMultiList *this, const void* edx, int id, int x, int y, int width, int height, int size);
// win1.41 0040b4a0 mac 103f18b0 SetupMultiList::~SetupMultiList(void)
struct SetupMultiList* __fastcall dt__14SetupMultiListFb(struct SetupMultiList* this, const void* edx, bool param_1);
// win1.41 0040b530 mac 1047e020 SetupMultiList::IsSelected(int)
bool __fastcall IsSelected__14SetupMultiListFi(struct SetupMultiList* this, const void* edx, int index);
// win1.41 0040b560 mac 103f18b0 SetupMultiList::Click(int, int)
void __fastcall Click__14SetupMultiListFii(struct SetupMultiList* this, const void* edx, int x, int y);

// win1.41 008ab364 mac none SetupMultiList virtual table
static struct vt__SetupList_t vt__SetupMultiList = {
    .super = {
        .SetToolTipUl = SetToolTip__12SetupControlFUl,
        .SetToolTipPw = SetToolTip__12SetupControlFPw,
        .Hide = Hide__12SetupControlFb,
        .SetFocus = SetFocus__12SetupControlFb,
        .HitTest = HitTest__12SetupControlFii,
        .Draw = (vt__SetupControl_Draw_t*)Draw__9SetupListFbb,
        .Drag = (vt__SetupControl_Drag_t*)Drag__9SetupListFii,
        .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__9SetupListFiib,
        .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__9SetupListFiib,
        .Click = (vt__SetupControl_Click_t*)Click__14SetupMultiListFii,
        .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__9SetupListFii,
        .Char = Char__12SetupControlFi,
        .dtor = (vt__SetupControl_dtor_t*)dt__14SetupMultiListFb,
    },
    .IsSelected = (vt__SetupList_IsSelected_t*)IsSelected__14SetupMultiListFi,
};

#endif // BLACKHACK_SETUPMULTILIST_H
