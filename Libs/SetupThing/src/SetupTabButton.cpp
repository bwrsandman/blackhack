#include "SetupTabButton.h"

#include <logger.h>

#include <GatheringText.h>

#include "Setup.h"
#include "SetupThing.h"

void __fastcall Draw__14SetupTabButtonFbb(
    struct SetupTabButton* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  DrawTab__10SetupThingFiiiiiiiPwii(this->super.super.rect.p0.x,
      this->super.super.rect.p0.y, this->super.super.rect.p1.x,
      this->super.super.rect.p1.y, (bool)this->selected,
      (bool)this->first_in_row, (bool)this->last_in_row,
      this->super.super.label, this->color, false);

  uint8_t prev_alpha = *GetDrawAlpha__10SetupThingFv();
  if (!this->selected) {
    *GetDrawAlpha__10SetupThingFv() =
        (uint8_t)((float)*GetDrawAlpha__10SetupThingFv() * 2.0f / 3.0f);
  }

  int text_size;
  for (text_size = GetTextSize__12SetupControlFv(&this->super.super);
       text_size > GetSmallTextSize__Fv() / 2; --text_size) {
    float fVar2 = (float)(this->super.super.rect.p0.y + 7);
    float local_10 = (float)(this->super.super.rect.p1.y + 100);
    struct LH3DColor local_18 = {
      .b = 0xFF,
      .g = 0xFF,
      .r = 0xFF,
      .a = 0xFF,
    };
    float fVar4 = DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
        GetGameFont__13GatheringTextFv(), edx, this->super.super.label,
        (float)(this->super.super.rect.p0.x + 9), fVar2, fVar2,
        (float)(this->super.super.rect.p1.x - 7), local_10, local_10, fVar2,
        20.0f, (float)text_size, &local_18, 1, 0, 0);
    if (fVar4 < (float)(this->super.super.rect.p1.y -
                        this->super.super.rect.p0.y - 6)) {
      break;
    }
  }

  // FIXME: The reason for the double call to DrawTextWrap is unknown, might be
  //        a vanilla bug.
  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
      this->super.super.rect.p0.x + 9, this->super.super.rect.p0.y + 8,
      this->super.super.rect.p1.x - 7, this->super.super.rect.p1.y + 2,
      this->super.super.rect.p0.y + 8, true, this->super.super.label, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, true, false);

  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
      this->super.super.rect.p0.x + 9, this->super.super.rect.p0.y + 8,
      this->super.super.rect.p1.x - 7, this->super.super.rect.p1.y + 2,
      this->super.super.rect.p0.y + 8, true, this->super.super.label, text_size,
      &LH3DColor_ARRAY_00c4ccd8_1, true, false);

  const struct LH3DColor* p_color;
  if (hovered || (selected && this->super.super.label[0] == L'\0')) {
    p_color = &LH3DColor_ARRAY_00c4ccd8_3;
  }
  else {
    p_color = &LH3DColor_00c4ccf8;
  }
  DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(
      this->super.super.rect.p0.x + 8, this->super.super.rect.p0.y + 7,
      this->super.super.rect.p1.x + -8, this->super.super.rect.p1.y + 1,
      this->super.super.rect.p0.y + 7, true, this->super.super.label, text_size,
      p_color, true, false);

  *GetDrawAlpha__10SetupThingFv() = prev_alpha;
}

struct SetupTabButton* __fastcall ct__14SetupTabButtonFiiiiiPwiii(
    struct SetupTabButton* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label, bool selected,
    bool first_in_row, bool last_in_row) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=%d, x=%d, y=%d, width=%d, height=%d, "
                         "label=\"%S\", selected=%s, first_in_row=%s, "
                         "last_in_row=%s) from %p",
      this, id, x, y, width, height, label, selected ? "true" : "false",
      first_in_row ? "true" : "false", last_in_row ? "true" : "false",
      puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  ct__10SetupButtonFiiiiiPwi(
      &this->super, edx, id, x, y, width, height, label, 0);
  this->super.super.vftptr = &vt__SetupTabButton;
  this->super.super.text_size = GetMidTextSize__Fv();
  if (this->super.super.setup_box != NULL) {
    this->super.super.setup_box->field_0x94 = 2;
  }
  this->super.pressed = false;

  this->selected = (uint32_t)selected;
  this->first_in_row = (uint32_t)first_in_row;
  this->last_in_row = (uint32_t)last_in_row;
  this->color = white;

  return this;
}

void __fastcall KeyDown__14SetupTabButtonFii(struct SetupTabButton* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.super.setup_box != NULL) {
    LOG_DEBUG("%p", this->super.super.setup_box->vftptr->field_0x0);
    this->super.super.setup_box->vftptr->field_0x0(
        this->super.super.setup_box, edx, key, mod);
  }
}

struct SetupTabButton* __fastcall dt__14SetupTabButtonFb(
    struct SetupTabButton* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}
