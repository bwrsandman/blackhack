#ifndef BLACKHACK_SETUPLIST_H
#define BLACKHACK_SETUPLIST_H

#include "SetupControl.h"

#include <LH3DColor.h>

struct SetupList;

typedef uint32_t __stdcall SetupList__ListBoxDraw_t(
    struct SetupList *widget, int value,
    int x_min, int y_min,
    int x_max, int y_max,
    int param_7, int style);

struct SetupList {
  struct SetupControl super;
  bool field_0x23c;
  int scrollback_width;
  bool field_0x244;
  int selected_index;
  int field_0x24c;
  int num_items;
  int field_0x254;
  wchar_t (*item_labels)[256];
  int* item_heights;
  uint32_t* field_0x260;
  uint32_t* field_0x264;
  struct LH3DColor* field_0x268;
  SetupList__ListBoxDraw_t** ListBoxDraw;
  int scroll_distance;
  bool show_scrollbar;
  int field_0x278;
  int scroll_position;
  int field_0x280;
  bool field_0x284;
  bool field_0x285;
  struct LHCoord drag_start;
  bool use_color_background;
  bool draw_highlight_box;
  uint8_t field_0x292;
  uint8_t field_0x293;
  struct LH3DColor box_outline_color;
  struct LH3DColor selection_color;
  uint8_t field_0x29c;
  struct SetupRect selection_rect;
};
static_assert(sizeof(struct SetupList) == 0x2b0, "struct size is wrong");

// win1.41 00409dd0 mac 10594000 SetupList::AutoScroll(bool)
void __fastcall AutoScroll__9SetupListFb(struct SetupList* this, const void* edx, bool param_1);
// win1.41 00409eb0 mac 103d24f0 SetupList::KeyDown(int, int)
void __fastcall KeyDown__9SetupListFii(struct SetupList* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 0040a110 mac 101c7fc0 SetupSlider::Drag(int, int)
void __fastcall Drag__9SetupListFii(struct SetupList* this, const void* edx, int x, int y);
// win1.41 0040a370 mac 10478900 SetupList::MouseDown(int, int, bool)
void __fastcall MouseDown__9SetupListFiib(struct SetupList *this, const void* edx, int x, int y, bool param_3);
// win1.41 0040a360 mac 100b7170 SetupList::Click(int, int)
void __fastcall Click__9SetupListFii(struct SetupList *this, const void* edx, int x, int y);
// win1.41 0040a3f0 mac 100b4690 SetupList::MouseUp(int, int, bool)
void __fastcall MouseUp__9SetupListFiib(struct SetupList* this, const void* edx, int x, int y, bool param_3);
// win1.41 0040a450 mac 10494bc0 SetupList::SetupList(int, int, int, int, int)
struct SetupList * __fastcall ct__9SetupListFiiiii(struct SetupList *this, const void* edx, int id, int x, int y, int width, int height);
// win1.41 0040a540 mac 1056c3d0 SetupList::~SetupList(void)
struct SetupList* __fastcall dt__9SetupListFb(struct SetupList* this, const void* edx, bool param_1);
// win1.41 0040a520 mac 104e2bb0 SetupList::IsSelected(int)
bool __fastcall IsSelected__9SetupListFi(struct SetupList* this, const void* edx, int index);
// win1.41 0040a5c0 mac 10388e60 SetupList::Draw(bool, bool)
void __fastcall Draw__9SetupListFbb(struct SetupList *this, const void* edx, bool hovered, bool selected);
// win1.41 0040aaf0 mac 1056d710 SetupList::UpdateHeights(void)
void __fastcall UpdateHeights__9SetupListFv(struct SetupList *this);
// win1.41 0040ad60 mac 10169200 SetupList::DeleteString(int)
void __fastcall DeleteString__9SetupListFi(struct SetupList *this, const void* edx, int index);
// win1.41 0040b050 mac 104ea7a0 SetupList::SetNum(int)
void __fastcall SetNum__9SetupListFi(struct SetupList *this, const void* edx, int num);

typedef bool (__fastcall vt__SetupList_IsSelected_t)(struct SetupList* this, const void* edx, int index);

struct vt__SetupList_t {
  struct vt__SetupControl_t super;
  vt__SetupList_IsSelected_t * const IsSelected;
};

// win1.41 008ab324 mac none SetupList virtual table
static struct vt__SetupList_t vt__SetupList = {
    .super = {
        .SetToolTipUl = SetToolTip__12SetupControlFUl,
        .SetToolTipPw = SetToolTip__12SetupControlFPw,
        .Hide = Hide__12SetupControlFb,
        .SetFocus = SetFocus__12SetupControlFb,
        .HitTest = HitTest__12SetupControlFii,
        .Draw = (vt__SetupControl_Draw_t*)Draw__9SetupListFbb,
        .Drag = (vt__SetupControl_Drag_t*)Drag__9SetupListFii,
        .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__9SetupListFiib,
        .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__9SetupListFiib,
        .Click = (vt__SetupControl_Click_t*)Click__9SetupListFii,
        .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__9SetupListFii,
        .Char = Char__12SetupControlFi,
        .dtor = (vt__SetupControl_dtor_t*)dt__9SetupListFb,
    },
    .IsSelected = IsSelected__9SetupListFi,
};


#endif // BLACKHACK_SETUPLIST_H
