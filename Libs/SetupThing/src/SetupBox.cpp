#include "SetupBox.h"

#include <logger.h>

#include "SetupControl.h"
#include "SetupThing.h"

struct SetupBox* GetCurrentActiveBox__8SetupBoxFv() {
  return Get__10SetupThingFv()->current_active_box;
}

struct SetupControl* __fastcall FindControl__8SetupBoxFi(
    struct SetupBox* this, const void* edx, int id) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, id=0x%08x) from %p", this, id, puEBP[1]);

  for (struct SetupControl* walker = this->widget_list; walker != NULL;
       walker = walker->next) {
    if (walker->id == id) {
      return walker;
    }
  }

  for (struct SetupControl* walker = this->widgets_0x68; walker != NULL;
       walker = walker->next) {
    if (walker->id == id) {
      return walker;
    }
  }

  return NULL;
}

void __fastcall SetFocusControl__8SetupBoxFP12SetupControl(
    struct SetupBox* this, const void* edx, struct SetupControl* widget) {
  if (this->focused_widget != widget) {
    if (this->focused_widget != NULL) {
      this->focused_widget->vftptr->SetFocus(this->focused_widget, edx, false);
    }
    this->focused_widget = widget;
    if (widget != NULL) {
      widget->vftptr->SetFocus(widget, edx, true);
    }
  }
}

void __fastcall SetFocusNext__8SetupBoxFv(struct SetupBox* this) {
  struct SetupControl* widget = this->focused_widget;

  while (true) {
    if (this->widget_list != NULL) {
      for (widget = this->widget_list; widget != NULL; widget = widget->next) {
        if (widget->next == this->focused_widget) {
          break;
        }
      }
    }

    if (widget == NULL) {
      widget = this->widget_list;
    }

    if (widget == this->focused_widget) {
      return;
    }

    SetFocusControl__8SetupBoxFP12SetupControl(this, NULL, widget);

    if (widget == this->focused_widget || widget == NULL ||
        (widget->field_0x22a && !widget->hidden)) {
      return;
    }
  }
}

void __fastcall SetFocusPrev__8SetupBoxFv(struct SetupBox* this) {
  struct SetupControl* widget = this->focused_widget;

  while (true) {
    if (widget == NULL) {
      widget = this->widget_list;
    }
    else {
      widget = widget->next;
      if (widget == NULL) {
        widget = this->widget_list;
      }
    }

    if (widget == this->focused_widget) {
      return;
    }

    SetFocusControl__8SetupBoxFP12SetupControl(this, NULL, widget);

    if (widget == this->focused_widget) {
      return;
    }

    if (widget == NULL) {
      break;
    }

    if (widget->field_0x22a && !widget->hidden) {
      return;
    }
  }
}
