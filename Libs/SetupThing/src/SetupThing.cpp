#include "SetupThing.h"
#include "Setup.h"

#include <logger.h>

#include <GatheringText.h>
#include <LH3DMaterial.h>
#include <LH3DRender.h>
#include <LHSys.h>

#include <globals.h>

uint8_t* GetDrawAlpha__10SetupThingFv(void) {
  return &globals()->SetupThingDraw->alpha;
}

struct SetupThing* Get__10SetupThingFv(void) {
  return globals()->SetupThing;
}

float __cdecl GetTextHeight__10SetupThingFiiiiibPwi(int param_1, int param_2,
    int param_3, int param_4, int param_5, bool param_6, const wchar_t* text,
    int param_8) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%d, %d, %d, %d, %d, %s, text=\"%S\", %d) from %p",
      param_1, param_2, param_3, param_4, param_5, param_6 ? "true" : "false",
      text, param_8, puEBP[1]);

  const struct LH3DColor white = { .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF };

  if (text[0] == L'\0') {
    return 0.0f;
  }

  return DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
      Get__10SetupThingFv()->font, NULL, text, (float)param_1, (float)param_2,
      (float)param_2, (float)param_3, (float)param_4, (float)param_4,
      (float)param_5, *globals()->DAT_00e839e0 * 1.5f, (float)param_8, &white,
      param_6, 0, 0);
}

float __cdecl GetTextWidth__10SetupThingFPwfif(
    const wchar_t* text, float size, int param_3, float param_4) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(text=\"%S\", size=%f, %d, %f) from %p", text, size,
      param_3, param_4, puEBP[1]);

  if (param_3 == 0) {
    param_3 = wcslen(text);
  }

  return param_4 * GetStringWidth__13GatheringTextFPwif(
                       Get__10SetupThingFv()->font, NULL, text, param_3, size);
}

float __cdecl DrawTextWrap__10SetupThingFiiiiibPwiP9LH3DColorbb(int x_min,
    int y_min, int x_max, int y_max, int param_5, bool param_6,
    const wchar_t* text, int param_8, const struct LH3DColor* p_color,
    bool param_10, bool param_11) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x_min=%d, y_min=%d, x_max=%d, y_max=%d, %d, %s, "
                         "text=\"%S\", %d, color=0x%08X, %s %s) from %p",
      x_min, y_min, x_max, y_max, param_5, param_6 ? "true" : "false", text,
      param_8, p_color, param_10 ? "true" : "false",
      param_11 ? "true" : "false", puEBP[1]);

  if (text[0] == L'\0') {
    return 0.0f;
  }

  const struct LH3DColor clear_color = {
    .b = 0x00, .g = 0x00, .r = 0x00, .a = 0x00
  };

  struct LH3DColor color = p_color == NULL ? clear_color : *p_color;

  float param_8_ = (float)param_8;
  y_max -= y_min;
  param_5 -= y_min;
  color.a = *GetDrawAlpha__10SetupThingFv();
  x_max -= x_min;
  float fVar1 = adjust__10SetupThingFRiRi(&x_min, &y_min);
  if (fVar1 != 0.0f) {
    param_8_ /= fVar1;
    param_5 = (int)((float)param_5 / fVar1);
    x_max = (int)((float)x_max / fVar1);
    y_max = (int)((float)y_max / fVar1);
  }
  param_5 += y_min;
  y_max += y_min;
  x_max += x_min;

  if (param_10) {
    fVar1 = DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
        GetGameFont__13GatheringTextFv(), NULL, text, (float)x_min, (float)y_min,
        (float)y_min, (float)x_max, (float)y_max, (float)y_max, (float)param_5,
        *globals()->DAT_00e839e0 * 1.5f, param_8_, &color, (uint32_t)param_6, 0,
        (uint32_t)param_11);

    if (fVar1 < (float)(y_max - y_min)) {
      param_5 =
          (int)(((float)(y_max - y_min) - fVar1) * (float)0.5f + (float)y_min);
    }
  }

  fVar1 = DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
      Get__10SetupThingFv()->font, NULL, text, (float)x_min, (float)y_min,
      (float)y_min, (float)x_max, (float)y_max, (float)y_max, (float)param_5,
      *globals()->DAT_00e839e0 * 1.5f, param_8_, &color, (uint32_t)param_6, 1,
      (uint32_t)param_11);

  gSetupThingRect.p0.x = x_min;
  gSetupThingRect.p0.y = y_min;
  gSetupThingRect.p1.x = x_max;
  gSetupThingRect.p1.y = y_max;

  unadjust__10SetupThingFRiRi(&gSetupThingRect.p0.x, &gSetupThingRect.p0.y);
  unadjust__10SetupThingFRiRi(&gSetupThingRect.p1.x, &gSetupThingRect.p1.y);
  return unadjustsize__10SetupThingFf(fVar1);
}

float __cdecl DrawText__10SetupThingFiii11TEXTJUSTIFYPwiP9LH3DColori(int x,
    int y, int width, enum TEXTJUSTIFY justify, const wchar_t* text, int size,
    const struct LH3DColor* p_color, int param_8) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x=%d, y=%d, width=%d, justify=%s, text=\"%S\", "
                         "size=%d, color=0x%08X, %d) from %p",
      x, y, width, TEXTJUSTIFY_strs[justify], text, size, p_color, param_8,
      puEBP[1]);

  if (text[0] == L'\0') {
    return 0.0f;
  }

  struct LH3DColor color = { .b = 0, .g = 0, .r = 0, .a = 0 };

  if (p_color != NULL) {
    color = *p_color;
  }

  color.a = *GetDrawAlpha__10SetupThingFv();

  float scale = adjust__10SetupThingFRiRi(&x, &y);
  float adjusted_size = (float)size;
  float adjusted_width = (float)width;
  if (scale != 0.0f) {
    adjusted_size = (float)size / scale;
    adjusted_width = (float)width / scale;
  }

  if (param_8 == 0) {
    param_8 = (int)wcslen(text);
  }

  int offset = 0;
  for (; param_8 != 0; --param_8) {
    scale = GetStringWidth__13GatheringTextFPwif(
        Get__10SetupThingFv()->font, NULL, text, param_8, adjusted_size);
    offset = (int)scale;
    if ((float)offset <= adjusted_width) {
      break;
    }
  }

  if (justify == TEXTJUSTIFY_RIGHT) {
    x -= offset;
  }
  else if (justify == TEXTJUSTIFY_CENTRE) {
    x -= offset / 2;
  }

  DrawTextRaw__13GatheringTextFPwiffffP9LH3DColoriP9LH3DColorff(
      Get__10SetupThingFv()->font, NULL, text, param_8, (float)x, (float)y,
      *globals()->DAT_00e839e0 * 1.5f, adjusted_size, &color, 0, NULL, 0.0f,
      4096.0f);

  gSetupThingRect.p0.x = x;
  gSetupThingRect.p0.y = y;
  gSetupThingRect.p1.x = offset + x;
  gSetupThingRect.p1.y = (int)((float)y + adjusted_size);

  unadjust__10SetupThingFRiRi(&gSetupThingRect.p0.x, &gSetupThingRect.p0.y);
  unadjust__10SetupThingFRiRi(&gSetupThingRect.p1.x, &gSetupThingRect.p1.y);

  return unadjustsize__10SetupThingFf(adjusted_size);
}

float __cdecl adjust__10SetupThingFRiRi(int* x, int* y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x=0x%p, y=0x%p) from %p", x, y, puEBP[1]);

  uint16_t sx = Get__5LHSysFv()->screen.field_0x8;
  uint16_t sy = Get__5LHSysFv()->screen.field_0xa;

  float scale;
  if (sx >= 800 && sy >= 600) {
    scale = 1.0f;
    *x += (sx - 800) / 2;
    *y += (sy - 600) / 2;
  }
  else {
    scale = max(850.0f / (float)sx, 650.0f / (float)sy);
    *x = (int)((float)*x / scale + ((float)sx - 800.0f / scale) / 2.0f);
    *y = (int)((float)*y / scale + ((float)sy - 600.0f / scale) / 2.0f);
  }

  return scale;
}

float __cdecl unadjust__10SetupThingFRiRi(int* x, int* y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x=0x%p, y=0x%p) from %p", x, y, puEBP[1]);

  uint16_t sx = Get__5LHSysFv()->screen.field_0x8;
  uint16_t sy = Get__5LHSysFv()->screen.field_0xa;

  float scale = 1.0f;
  if (sx >= 800 && sy >= 600) {
    scale = 1.0f;
    *x -= (sx - 800) / 2;
    *y -= (sy - 600) / 2;
  }
  else {
    scale = max(850.0f / (float)sx, 650.0f / (float)sy);
    *x = (int)((float)*x - ((float)sx - 800.0f / scale) / 2.0f);
    *y = (int)((float)*y - ((float)sy - 600.0f / scale) / 2.0f);
    *x = (int)((float)*x * scale);
    *y = (int)((float)*y * scale);
  }

  return scale;
}

int __cdecl adjusty__10SetupThingFi(int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(y=%f) from %p", y, puEBP[1]);

  uint16_t sx = Get__5LHSysFv()->screen.field_0x8;
  uint16_t sy = Get__5LHSysFv()->screen.field_0xa;

  if (sx >= 800 && sy >= 600) {
    return (sy - 600) / 2 + y;
  }

  float scale = 850.0f / (float)sx;
  if (scale <= 650.0f / (float)sy) {
    scale = 650.0f / scale;
  }

  return (int)((float)y / scale + (scale - 600.0f / scale) * 0.5f);
}

void __cdecl DrawBigButton__10SetupThingFiibbi7BBSTYLEbii(int x, int y,
    bool centered, bool interacted, int size, enum BBSTYLE style, bool shadowed,
    int clip_y_start, int clip_y_end) {

  assert(style < BBSTYLE_COUNT);

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x=%d, y=%d, centered=%s, interacted=%s, size=%d, "
                         "style=%s, shadowed=%s, clip_y_start=%d, "
                         "clip_y_end=%d) from %p",
      x, y, centered ? "true" : "false", interacted ? "true" : "false", size,
      BBSTYLE_strs[style] == NULL ? "" : BBSTYLE_strs[style],
      shadowed ? "true" : "false", clip_y_start, clip_y_end, puEBP[1]);


  uint32_t bev_style;
  bool use_weather_material;
  bool invert_uv;

  float u_min;
  float v_min;
  float uv_extent;

  float u_min_shadow;
  float v_min_shadow;

  switch (style) {
  default:
    assert(false);
  case BBSTYLE_CHECK_BOX_OFF:
  case BBSTYLE_CHECK_BOX_ON:
    bev_style = (style == BBSTYLE_CHECK_BOX_ON) ? 2 : 1;
    use_weather_material = false;
    invert_uv = false;

    uv_extent = 2.0f / 16.0f;
    u_min = 11.0f / 16.0f + (interacted ? uv_extent : 0.0f);
    v_min = (float)(style % 2) * uv_extent;

    u_min_shadow = 3.0f / 16.0f;
    v_min_shadow = 0.0f;
    break;

  case BBSTYLE_LEFT_ARROW:
  case BBSTYLE_RIGHT_ARROW:
    bev_style = 0;
    use_weather_material = false;
    invert_uv = style == BBSTYLE_RIGHT_ARROW;

    uv_extent = 2.0f / 16.0f;
    u_min = 5.0f / 16.0f + (interacted ? uv_extent : 0.0f);
    v_min = 0.0f;

    u_min_shadow = 9.0f / 16.0f;
    v_min_shadow = 0.0f;
    break;

  case BBSTYLE_ROTATION:
    bev_style = 0;
    use_weather_material = false;
    invert_uv = false;
    shadowed = false;
    centered = true;

    uv_extent = 6.0f / 16.0f;
    u_min = 4.0f / 16.0f + (interacted ? uv_extent : 0.0f);
    v_min = 10.0f / 16.0f;

    u_min_shadow = 0.0f;
    v_min_shadow = 0.0f;
    break;

  case BBSTYLE_SPEECH:
  case BBSTYLE_NO_SPEECH:
  case BBSTYLE_SPEECH_ARROW:
  case BBSTYLE_EXCLAIM_ARROW: {
    uint32_t index = style - BBSTYLE_SPEECH;

    bev_style = 0;
    use_weather_material = false;
    invert_uv = false;
    shadowed = false;

    uv_extent = 3.0f / 16.0f;
    u_min = 2.0f / 16.0f + (float)(index % 2) * uv_extent +
            2.0f * (interacted ? uv_extent : 0.0f);
    v_min = 4.0f / 16.0f + (float)(index / 2) * uv_extent;

    u_min_shadow = 0.0f;
    v_min_shadow = 0.0f;
  } break;

  case BBSTYLE_ENVELOPE_ARROW:
  case BBSTYLE_ENVELOPE: {
    uint32_t index = style - BBSTYLE_ENVELOPE_ARROW;

    bev_style = 0;
    use_weather_material = false;
    invert_uv = false;
    shadowed = false;

    uv_extent = 2.0f / 16.0f;
    u_min = 14.0f / 16.0f;
    v_min = 6.0f / 16.0f + (float)(index % 2) * uv_extent;

    u_min_shadow = 0.0f;
    v_min_shadow = 0.0f;
  } break;

  case BBSTYLE_WEATHER_SUNNY:
  case BBSTYLE_WEATHER_PARTIALLY_CLOUDY:
  case BBSTYLE_WEATHER_CLOUDY:
  case BBSTYLE_WEATHER_SCATTERED_SHOWERS:
  case BBSTYLE_WEATHER_SHOWERS:
  case BBSTYLE_WEATHER_SCATTERED_FLURRIES:
  case BBSTYLE_WEATHER_FLURRIES:
  case BBSTYLE_WEATHER_HEAVY_FLURRIES:
  case BBSTYLE_WEATHER_THUNDER_STORMS:
  case BBSTYLE_0x14: {
    uint32_t index = style - BBSTYLE_WEATHER_SUNNY;

    bev_style = 0;
    use_weather_material = true;
    invert_uv = false;
    shadowed = false;

    uv_extent = 1.0f / 4.0f;
    u_min = (float)(index % 4) * uv_extent;
    v_min = (float)(index / 4) * uv_extent;

    u_min += 2.0f / 512.0f;
    v_min += 2.0f / 512.0f + uv_extent;
    uv_extent += 2.0f / 512.0f;

    u_min_shadow = 0.0f;
    v_min_shadow = 0.0f;
  } break;
  }

  u_min += 1.0f / 512.0f;
  v_min += 1.0f / 512.0f;
  uv_extent -= 2.0f / 512.0f;
  float uv_extent_shadow = 2.0f / 16.0f;
  if (invert_uv) {
    u_min += uv_extent;
    v_min += uv_extent;
    uv_extent = -uv_extent;

    u_min_shadow += 2.0f / 16.0f;
    v_min_shadow = 2.0f / 16.0f;
    uv_extent_shadow = -uv_extent_shadow;
  }

  if (shadowed) {
    int padding = centered ? 2 : 4;
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(x + padding,
        y + padding, x + size + padding, y + size + padding, u_min_shadow,
        v_min_shadow, u_min_shadow + uv_extent_shadow,
        v_min_shadow + uv_extent_shadow, Get__10SetupThingFv()->ui_shadow_material, NULL, 1,
        clip_y_start, clip_y_end, false, 100.0f);
  }
  if (bev_style) {
    int padding = centered ? 1 : -1;
    const struct LH3DColor white = {
      .b = 0xFF, .g = 0xFF, .r = 0xFF, .a = 0xFF
    };
    DrawBevBox__10SetupThingFiiiiiiiUl(x + padding, y + padding,
        x + padding + size - 2, y + padding + size - 2,
        (bev_style == 2 ? 0x20 : 0x0) + (interacted ? 0x2 : 0x0) + 0xb, 8,
        0xffffffff, white);
  }
  else {
    int padding = centered ? 0 : -2;
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(x + padding,
        y + padding, x + padding + size, y + padding + size, u_min, v_min,
        u_min + uv_extent, v_min + uv_extent,
        use_weather_material ? *globals()->DAT_00ed92dc
                             : Get__10SetupThingFv()->ui_shadow_material,
        NULL, 1, clip_y_start, clip_y_end, false, 100.0f);
  }
}

float __cdecl unadjustsize__10SetupThingFf(float size) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(size=%f) from %p", size, puEBP[1]);

  if (Get__5LHSysFv()->screen.field_0x8 >= 800 &&
      Get__5LHSysFv()->screen.field_0xa >= 600) {
    return size;
  }

  float fVar5 = 850.0f / (float)Get__5LHSysFv()->screen.field_0x8;
  float fVar1 = 650.0f / (float)Get__5LHSysFv()->screen.field_0xa;

  if (fVar5 <= fVar1) {
    fVar5 = 650.0f / (float)Get__5LHSysFv()->screen.field_0xa;
  }

  return fVar5 * size;
}

void __cdecl DrawLine__10SetupThingFiiiiUliff(int x1, int y1, int x2, int y2,
    struct LH3DColor color, int adjust, float param_7, float inv_w) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x1=%d, y1=%d, x2=%d, y2=%d, color=0x%08X, "
                         "adjust=%d, %f, inv_w=%f) from %p",
      x1, y1, x2, y2, *(uint32_t*)&color, adjust, param_7, inv_w, puEBP[1]);

  if (adjust) {
    adjust__10SetupThingFRiRi(&x1, &y1);
    adjust__10SetupThingFRiRi(&x2, &y2);
  }

  globals()->SetupThingDraw->vertices[0].sx = (float)x1 + 0.5f;
  globals()->SetupThingDraw->vertices[0].color =
      ((*(uint32_t*)&color) & 0xffffffu) |
      ((uint32_t)*GetDrawAlpha__10SetupThingFv() << 0x18u);
  globals()->SetupThingDraw->vertices[0].tu = 0.0f;
  globals()->SetupThingDraw->vertices[0].sy = (float)y1 + 0.5f;
  globals()->SetupThingDraw->vertices[0].tv = 0.0f;
  globals()->SetupThingDraw->vertices[1].tu = 0.0f;
  globals()->SetupThingDraw->vertices[1].tv = 0.0f;
  globals()->SetupThingDraw->vertices[1].sx = (float)x2 + 0.5f;
  globals()->SetupThingDraw->vertices[1].sy = (float)y2 + 0.5f;
  globals()->SetupThingDraw->vertices[1].color =
      globals()->SetupThingDraw->vertices[0].color;

  if ((*globals()->PTR_00edd494) != NULL && (*globals()->DAT_00c38714) != 0) {
    *globals()->DAT_00eca64c = *globals()->PTR_00edd494;
    (*globals()->PTR_00eca618)[(*globals()->PTR_00edd494)->render_mode].callback(
        (*globals()->PTR_00edd494), 0);
    if ((*globals()->DAT_00eca614) == 0 &&
        ((*globals()->PTR_00edd494)->cull_mode & 4u) == 0) {
      SetD3DTillingOff__10LH3DRenderFi(0);
    }
    else {
      SetD3DTillingOn__10LH3DRenderFi(0);
    }
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(D3DRENDERSTATE_CULLMODE,
        ((*globals()->PTR_00edd494)->cull_mode % 2 == 0) ? D3DCULL_CCW
                                                      : D3DCULL_NONE);
  }

  uint32_t z_write_enabled;
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_ZWRITEENABLE, &z_write_enabled);
  D3DCMPFUNC z_func;
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_ZFUNC, (uint32_t*)&z_func);

  if (param_7 == 0.0f) {
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZFUNC, D3DCMP_ALWAYS);
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZWRITEENABLE, FALSE);
  }
  else {
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZWRITEENABLE, FALSE);
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZFUNC, D3DCMP_LESSEQUAL);
    globals()->SetupThingDraw->vertices[0].rhw = (*globals()->DAT_00e839e0) / inv_w;
    globals()->SetupThingDraw->vertices[0].sz =
        1.0f - globals()->SetupThingDraw->vertices[0].rhw;

    globals()->SetupThingDraw->vertices[1].sz =
        globals()->SetupThingDraw->vertices[0].sz;
    globals()->SetupThingDraw->vertices[1].rhw =
        globals()->SetupThingDraw->vertices[0].rhw;

    globals()->SetupThingDraw->vertices[2].sz =
        globals()->SetupThingDraw->vertices[0].sz;
    globals()->SetupThingDraw->vertices[2].rhw =
        globals()->SetupThingDraw->vertices[0].rhw;

    globals()->SetupThingDraw->vertices[3].sz =
        globals()->SetupThingDraw->vertices[0].sz;
    globals()->SetupThingDraw->vertices[3].rhw =
        globals()->SetupThingDraw->vertices[0].rhw;
  }

  uint32_t last_pixel;
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_LASTPIXEL, &last_pixel);
  if (last_pixel != 1) {
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_LASTPIXEL, 1);
  }

  uint16_t indices[2] = { 0, 1 };
  DrawAndClip__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl(D3DPT_LINELIST,
      D3DFVF_TLVERTEX, globals()->SetupThingDraw->vertices, 2, indices,
      _countof(indices));

  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_LASTPIXEL, last_pixel);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZFUNC, z_func);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZWRITEENABLE, z_write_enabled);

  globals()->SetupThingDraw->vertices[0].sz = 0.0f;
  globals()->SetupThingDraw->vertices[0].rhw = 1.0f;
  globals()->SetupThingDraw->vertices[1].sz = 0.0f;
  globals()->SetupThingDraw->vertices[1].rhw = 1.0f;
  globals()->SetupThingDraw->vertices[2].sz = 0.0f;
  globals()->SetupThingDraw->vertices[2].rhw = 1.0f;
  globals()->SetupThingDraw->vertices[3].sz = 0.0f;
  globals()->SetupThingDraw->vertices[3].rhw = 1.0f;
}

void __cdecl DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
    int x_min, int y_min, int x_max, int y_max, float u_min, float v_min,
    float u_max, float v_max, struct LH3DMaterial* material,
    const struct LH3DColor* p_color, int adjust, int clip_y_start,
    int clip_y_end, bool depth_test, float inv_w) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x_min=%d, y_min=%d, x_max=%d, y_max=%d, u_min=%f, "
                         "v_min=%f, u_max=%f, v_max=%f, material=0x%p, "
                         "p_color=0x%p, adjust=%d, clip_y_start=%d, "
                         "clip_y_end=%d, depth_test=%s, inv_w=%f) from %p",
      x_min, y_min, x_max, y_max, u_min, v_min, u_max, v_max, material, p_color,
      adjust, clip_y_start, clip_y_end, depth_test ? "true" : "false", inv_w,
      puEBP[1]);

  if (adjust) {
    adjust__10SetupThingFRiRi(&x_min, &y_min);
    adjust__10SetupThingFRiRi(&x_max, &y_max);
    clip_y_start = adjusty__10SetupThingFi(clip_y_start);
    clip_y_end = adjusty__10SetupThingFi(clip_y_end);
  }

  struct LH3DColor color = {
    .b = 0xff,
    .g = 0xff,
    .r = 0xff,
    .a = 0xff,
  };
  if (p_color != NULL) {
    color = *p_color;
  }
  color.a = *GetDrawAlpha__10SetupThingFv();

  if (y_max < y_min) {
    float tempf;
    int tempi;

    tempf = u_min;
    u_min = v_min;
    u_max = tempf;

    tempf = v_min;
    v_min = v_max;
    v_max = tempf;

    tempi = x_min;
    x_min = y_min;
    x_max = tempi;

    tempi = y_min;
    y_min = y_max;
    y_max = tempi;
  }

  if (x_max < x_min) {
    int tempi;

    u_max = u_min;
    u_min = v_max;

    tempi = y_max;
    x_max = x_min;
    x_min = tempi;
  }

  if (y_min >= clip_y_end || y_max <= clip_y_start) {
    return;
  }

  float scale = 0.0f;
  if (y_min < y_max) {
    scale = (v_max - v_min) / (float)(y_max - y_min);
  }
  if (y_min < clip_y_start) {
    v_min = (float)(clip_y_start - y_min) * scale + v_min;
    y_min = clip_y_start;
  }
  if (y_max > clip_y_end) {
    v_max = v_max - (float)(y_max - clip_y_end) * scale;
    y_max = clip_y_end;
  }
  if (y_max < y_min) {
    return;
  }

  globals()->SetupThingDraw->vertices[0].sx = (float)x_min + 0.5f;
  globals()->SetupThingDraw->vertices[0].sy = (float)y_min + 0.5f;
  globals()->SetupThingDraw->vertices[0].color = *(uint32_t*)&color;
  globals()->SetupThingDraw->vertices[0].tu = u_min;
  globals()->SetupThingDraw->vertices[0].tv = v_min;

  globals()->SetupThingDraw->vertices[1].sx = (float)x_max + 0.5f;
  globals()->SetupThingDraw->vertices[1].sy =
      globals()->SetupThingDraw->vertices[0].sy;
  globals()->SetupThingDraw->vertices[1].color =
      globals()->SetupThingDraw->vertices[0].color;
  globals()->SetupThingDraw->vertices[1].tu = u_max;
  globals()->SetupThingDraw->vertices[1].tv = v_min;

  globals()->SetupThingDraw->vertices[2].sx =
      globals()->SetupThingDraw->vertices[1].sx;
  globals()->SetupThingDraw->vertices[2].sy = (float)y_max + 0.5f;
  globals()->SetupThingDraw->vertices[2].color =
      globals()->SetupThingDraw->vertices[0].color;
  globals()->SetupThingDraw->vertices[2].tu = u_max;
  globals()->SetupThingDraw->vertices[2].tv = v_max;

  globals()->SetupThingDraw->vertices[3].sx =
      globals()->SetupThingDraw->vertices[0].sx;
  globals()->SetupThingDraw->vertices[3].sy =
      globals()->SetupThingDraw->vertices[2].sy;
  globals()->SetupThingDraw->vertices[3].color =
      globals()->SetupThingDraw->vertices[0].color;
  globals()->SetupThingDraw->vertices[3].tu = u_min;
  globals()->SetupThingDraw->vertices[3].tv = v_max;

  if ((*globals()->DAT_00c38714) != 0) {
    *globals()->DAT_00eca64c = material;
    if (material != NULL) {
      (*globals()->PTR_00eca618)[material->render_mode].callback(material, 0);
      if ((*globals()->DAT_00eca614) == 0 && (material->cull_mode & 4u) == 0) {
        SetD3DTillingOff__10LH3DRenderFi(0);
      }
      else {
        SetD3DTillingOn__10LH3DRenderFi(0);
      }
      SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
          D3DRENDERSTATE_CULLMODE,
          (material->cull_mode % 2 == 0) ? D3DCULL_CCW : D3DCULL_NONE);
    }
  }

  uint32_t z_write_enabled;
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_ZWRITEENABLE, &z_write_enabled);
  D3DCMPFUNC z_func;
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_ZFUNC, (uint32_t*)&z_func);

  if (depth_test) {
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZWRITEENABLE, FALSE);
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZFUNC, D3DCMP_LESSEQUAL);

    globals()->SetupThingDraw->vertices[0].rhw = (*globals()->DAT_00e839e0) / inv_w;
    globals()->SetupThingDraw->vertices[0].sz =
        1.0f - globals()->SetupThingDraw->vertices[0].rhw;

    globals()->SetupThingDraw->vertices[1].sz =
        globals()->SetupThingDraw->vertices[0].sz;
    globals()->SetupThingDraw->vertices[1].rhw =
        globals()->SetupThingDraw->vertices[0].rhw;

    globals()->SetupThingDraw->vertices[2].sz =
        globals()->SetupThingDraw->vertices[0].sz;
    globals()->SetupThingDraw->vertices[2].rhw =
        globals()->SetupThingDraw->vertices[0].rhw;

    globals()->SetupThingDraw->vertices[3].sz =
        globals()->SetupThingDraw->vertices[0].sz;
    globals()->SetupThingDraw->vertices[3].rhw =
        globals()->SetupThingDraw->vertices[0].rhw;
  }
  else {
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZFUNC, D3DCMP_ALWAYS);
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_ZWRITEENABLE, FALSE);
  }

  uint32_t last_pixel;
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_LASTPIXEL, &last_pixel);
  if (last_pixel != 0) {
    SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
        D3DRENDERSTATE_LASTPIXEL, 0);
  }

  DrawAndClip2D__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl(D3DPT_TRIANGLELIST,
      D3DFVF_TLVERTEX, globals()->SetupThingDraw->vertices,
      _countof(globals()->SetupThingDraw->vertices),
      globals()->SetupThingDraw->indices,
      _countof(globals()->SetupThingDraw->indices));

  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_LASTPIXEL, last_pixel);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZFUNC, z_func);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZWRITEENABLE, z_write_enabled);

  globals()->SetupThingDraw->vertices[0].sz = 0.0f;
  globals()->SetupThingDraw->vertices[0].rhw = 1.0f;

  globals()->SetupThingDraw->vertices[1].sz = 0.0f;
  globals()->SetupThingDraw->vertices[1].rhw = 1.0f;

  globals()->SetupThingDraw->vertices[2].sz = 0.0f;
  globals()->SetupThingDraw->vertices[2].rhw = 1.0f;

  globals()->SetupThingDraw->vertices[3].sz = 0.0f;
  globals()->SetupThingDraw->vertices[3].rhw = 1.0f;
}

void __cdecl DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl(int x_1, int y_1,
    int x_2, int y_2, int x_3, int y_3, int x_4, int y_4,
    struct LH3DColor color_1, struct LH3DColor color_2,
    struct LH3DColor color_3, struct LH3DColor color_4, uint32_t use_alpha,
    uint32_t adjust) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(
      __FUNCTION__ "(x_1=%d, y_1=%d, x_2=%d, y_2=%d, x_3=%d, y_3=%d, x_4=%d, "
                   "y_4=%d, color_1=0x%08X, color_2=0x%08X, color_3=0x%08X, "
                   "color_4=0x%08X, use_alpha=0x%08X, adjust=0x%08X) from %p",
      x_1, y_1, x_2, y_2, x_3, y_3, x_4, y_4, *(uint32_t*)&color_1,
      *(uint32_t*)&color_2, *(uint32_t*)&color_3, *(uint32_t*)&color_4,
      use_alpha, adjust, puEBP[1]);

  if (adjust) {
    adjust__10SetupThingFRiRi(&x_1, &y_1);
    adjust__10SetupThingFRiRi(&x_2, &y_2);
    adjust__10SetupThingFRiRi(&x_3, &y_3);
    adjust__10SetupThingFRiRi(&x_4, &y_4);
  }

  uint8_t alpha = *GetDrawAlpha__10SetupThingFv();
  if (use_alpha == 0) {
    color_1.a = (uint8_t)(((uint32_t)color_1.a * alpha) >> 8u);
    color_2.a = (uint8_t)(((uint32_t)color_2.a * alpha) >> 8u);
    color_3.a = (uint8_t)(((uint32_t)color_3.a * alpha) >> 8u);
    color_4.a = (uint8_t)(((uint32_t)color_4.a * alpha) >> 8u);
  }
  else {
    color_1.a = alpha;
    color_2.a = alpha;
    color_3.a = alpha;
    color_4.a = alpha;
  }

  globals()->SetupThingDraw->vertices[0].sx = (float)x_1 + 0.5f;
  globals()->SetupThingDraw->vertices[0].sy = (float)y_1 + 0.5f;
  globals()->SetupThingDraw->vertices[0].color = *(uint32_t*)&color_1;
  globals()->SetupThingDraw->vertices[0].tu = 0.0f;
  globals()->SetupThingDraw->vertices[0].tv = 0.0f;

  globals()->SetupThingDraw->vertices[1].sx = (float)x_2 + 0.5f;
  globals()->SetupThingDraw->vertices[1].sy = (float)y_2 + 0.5f;
  globals()->SetupThingDraw->vertices[1].color = *(uint32_t*)&color_2;
  globals()->SetupThingDraw->vertices[1].tu = 0.0f;
  globals()->SetupThingDraw->vertices[1].tv = 0.0f;

  globals()->SetupThingDraw->vertices[2].sx = (float)x_3 + 0.5f;
  globals()->SetupThingDraw->vertices[2].sy = (float)y_3 + 0.5f;
  globals()->SetupThingDraw->vertices[2].color = *(uint32_t*)&color_3;
  globals()->SetupThingDraw->vertices[2].tu = 0.0f;
  globals()->SetupThingDraw->vertices[2].tv = 0.0f;

  globals()->SetupThingDraw->vertices[3].sx = (float)x_4 + 0.5f;
  globals()->SetupThingDraw->vertices[3].sy = (float)y_4 + 0.5f;
  globals()->SetupThingDraw->vertices[3].color = *(uint32_t*)&color_4;
  globals()->SetupThingDraw->vertices[3].tu = 0.0f;
  globals()->SetupThingDraw->vertices[3].tv = 0.0f;

  if (*globals()->DAT_00c38714 != 0) {
    *globals()->DAT_00eca64c = *globals()->PTR_00edd494;
    if (*globals()->PTR_00edd494 != NULL) {
      (*globals()->PTR_00eca618)[(*globals()->PTR_00edd494)->render_mode].callback(
          *globals()->PTR_00edd494, 0);
      if (*globals()->DAT_00eca614 == 0 &&
          ((*globals()->PTR_00edd494)->cull_mode & 4u) == 0) {
        SetD3DTillingOff__10LH3DRenderFi(0);
      }
      else {
        SetD3DTillingOn__10LH3DRenderFi(0);
      }
      SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
          D3DRENDERSTATE_CULLMODE, ((*globals()->PTR_00edd494)->cull_mode % 2 == 0)
                                       ? D3DCULL_CCW
                                       : D3DCULL_NONE);
    }
  }

  uint32_t z_write_enable;
  uint32_t z_func;
  uint32_t last_pixel;
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_ZWRITEENABLE, &z_write_enable);
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_ZFUNC, &z_func);
  GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
      D3DRENDERSTATE_LASTPIXEL, &last_pixel);

  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZFUNC, D3DCMP_ALWAYS);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZWRITEENABLE, false);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_LASTPIXEL, 0);

  DrawAndClip2D__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl(D3DPT_TRIANGLELIST,
      D3DFVF_TLVERTEX, globals()->SetupThingDraw->vertices,
      _countof(globals()->SetupThingDraw->vertices),
      globals()->SetupThingDraw->indices,
      _countof(globals()->SetupThingDraw->indices));

  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZWRITEENABLE, z_write_enable);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_ZFUNC, z_func);
  SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
      D3DRENDERSTATE_LASTPIXEL, last_pixel);
}

void __cdecl DrawBox__10SetupThingFiiiiUlUlUlUlUlUl(int x_min, int y_min,
    int x_max, int y_max, struct LH3DColor color_1, struct LH3DColor color_2,
    struct LH3DColor color_3, struct LH3DColor color_4, uint32_t use_alpha,
    uint32_t adjust) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(
      __FUNCTION__ "(x_min=%d, y_min=%d, x_max=%d, y_max=%d, color_1=0x%08X, "
                   "color_2=0x%08X, color_3=0x%08X, color_4=0x%08X, "
                   "use_alpha=0x%08X, adjust=0x%08X) from %p",
      x_min, y_min, x_max, y_max, *(uint32_t*)&color_1, *(uint32_t*)&color_2,
      *(uint32_t*)&color_3, *(uint32_t*)&color_4, use_alpha, adjust, puEBP[1]);

  int temp;
  struct LH3DColor temp_color;

  if (x_max < x_min) {
    // swap(x_min, x_max)
    temp = x_min;
    x_min = x_max;
    x_max = temp;

    // swap(color_1, color_2)
    temp_color = color_1;
    color_1 = color_2;
    color_2 = temp_color;

    // swap(color_3, color_4)
    temp_color = color_3;
    color_3 = color_4;
    color_4 = temp_color;
  }
  if (y_max < y_min) {
    // swap(y_min, y_max)
    temp = y_min;
    y_min = y_max;
    y_max = temp;

    // swap(color_1, color_3)
    temp_color = color_1;
    color_1 = color_3;
    color_3 = temp_color;

    // swap(color_2, color_4)
    temp_color = color_2;
    color_2 = color_4;
    color_4 = temp_color;
  }
  DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl(x_min, y_min, x_max, y_min, x_max,
      y_max, x_min, y_max, color_1, color_2, color_3, color_4, use_alpha,
      adjust);
}

void __cdecl DrawTab__10SetupThingFiiiiiiiPwii(int x_min, int y_min, int x_max,
    int y_max, bool selected, bool first_in_row, bool last_in_row,
    const wchar_t* label, struct LH3DColor color, bool no_blend) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x_min=%d, y_min=%d, x_max=%d, y_max=%d, "
                         "selected=%s, first_in_row=%s, first_in_row=%s, "
                         "label=\"%S\", color=0x%08X, no_blend=%s) from %p",
      x_min, y_min, x_max, y_max, selected ? "true" : "false",
      first_in_row ? "true" : "false", last_in_row ? "true" : "false", label,
      *(uint32_t*)&color, no_blend ? "true" : "false", puEBP[1]);

  const struct LH3DColor clear_white = {
    .a = 0x00, .b = 0xFF, .g = 0xFF, .r = 0xFF
  };

  uint8_t prev_alpha = *GetDrawAlpha__10SetupThingFv();
  if (!no_blend) {
    *GetDrawAlpha__10SetupThingFv() =
        (uint8_t)((float)*GetDrawAlpha__10SetupThingFv() / 1.2f);
  }

  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MAGFILTER, D3DTFG_POINT);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MAGFILTER, D3DTFG_POINT);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MINFILTER, D3DTFG_POINT);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MINFILTER, D3DTFG_POINT);

  float v_min;

  struct SetupRect outer = {
    .p0 = { .x = x_min, .y = y_min },
    .p1 = { .x = x_max, .y = y_max },
  };

  struct SetupRect inner = {
    .p0 = { .x = outer.p0.x + 8, .y = outer.p0.y + 8 },
    .p1 = { .x = outer.p1.x - 8, .y = outer.p1.y - 8 },
  };

  // Two units shorter if first in row or last in row on left and right
  // respectively
  if (selected) {
    // Left side
    // For the tab: __________[###...###]__________
    // If first:      ________ 8 units wide at offset x_min + 2
    // If not:      __________ 10 units wide at offset x_min
    DrawLine__10SetupThingFiiiiUliff(outer.p0.x + (first_in_row ? 2 : 0),
        outer.p1.y + 2, inner.p0.x + 2, outer.p1.y + 2, clear_white, true, 0.0f,
        100.0f);

    // Right side
    // For the tab: __________[###...###]__________
    // If last:                          ________   8 units wide
    // If not:                           __________ 10 units wide
    // both offset at x_max - 10
    DrawLine__10SetupThingFiiiiUliff(inner.p1.x - 2, outer.p1.y + 2,
        outer.p1.x - (last_in_row ? 2 : 0), outer.p1.y + 2, clear_white, true,
        0.0f, 100.0f);
    v_min = 16.0f / 128.0f;
  }
  else {
    // For the tab: __________[___...___]__________
    // If first:      _____________________________
    // If last:     _____________________________
    // If both:       ___________________________
    // If neither:  _______________________________
    DrawLine__10SetupThingFiiiiUliff(outer.p0.x + (first_in_row ? 2 : 0),
        outer.p1.y + 2, outer.p1.x - (last_in_row ? 2 : 0), outer.p1.y + 2,
        clear_white, true, 0.0f, 100.0f);

    // Shadowed area above window on top of unselected tabs
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(x_min,
        inner.p1.y, outer.p1.x, outer.p1.y, 27.0f / 128.0f, 0.0f,
        37.0f / 128.0f, 3.0f / 128.0f, Get__10SetupThingFv()->ui_shadow_material, NULL, true,
        -0xa000, 0xa000, selected, 100.0f);
    *GetDrawAlpha__10SetupThingFv() =
        (uint8_t)((float)*GetDrawAlpha__10SetupThingFv() * 0.5f);

    v_min = 20.0f / 128.0f;
  }

  if (label[0] != L'\0') {

    // Shadowed areas around tabs
    float u0 = 24.0f / 128.0f;
    float u1 = 27.0f / 128.0f;
    float u2 = 37.0f / 128.0f;
    float u3 = 40.0f / 128.0f;
    float u4 = 120.0f / 128.0f;
    float u5 = 123.0f / 128.0f;
    float u6 = 125.0f / 128.0f;
    float u7 = 128.0f / 128.0f;
    float v0 = 0.0f / 128.0f;
    float v1 = 3.0f / 128.0f;
    float v2 = 13.0f / 128.0f;
    // Top
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(inner.p0.x,
        outer.p0.y, inner.p1.x, inner.p0.y, u1, v0, u2, v1,
        Get__10SetupThingFv()->ui_shadow_material, NULL, 1, -0xa000, 0xa000, false, 100.0f);
    // Left
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(outer.p0.x,
        inner.p0.y, inner.p0.x, inner.p1.y, u0, v1, u1, v2,
        Get__10SetupThingFv()->ui_shadow_material, NULL, 1, -0xa000, 0xa000, false, 100.0f);
    // Right
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(inner.p1.x,
        inner.p0.y, outer.p1.x, inner.p1.y, u2, v1, u3, v2,
        Get__10SetupThingFv()->ui_shadow_material, NULL, 1, -0xa000, 0xa000, false, 100.0f);
    // Top left corner
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(outer.p0.x,
        outer.p0.y, inner.p0.x, inner.p0.y, u0, v0, u1, v1,
        Get__10SetupThingFv()->ui_shadow_material, NULL, 1, -0xa000, 0xa000, false, 100.0f);
    // Top right corner
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(inner.p1.x,
        outer.p0.y, outer.p1.x, inner.p0.y, u2, v0, u3, v1,
        Get__10SetupThingFv()->ui_shadow_material, NULL, 1, -0xa000, 0xa000, false, 100.0f);
    // Bottom left corner
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(outer.p0.x,
        inner.p1.y, inner.p0.x, outer.p1.y, u4, v_min + v0, u5, v_min + v1,
        Get__10SetupThingFv()->ui_shadow_material, NULL, 1, -0xa000, 0xa000, false, 100.0f);
    // Bottom right corner
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(inner.p1.x,
        inner.p1.y, outer.p1.x, outer.p1.y, u6, v_min + v0, u7, v_min + v1,
        Get__10SetupThingFv()->ui_shadow_material, NULL, 1, -0xa000, 0xa000, false, 100.0f);

    // Top and side outline
    DrawBevBox__10SetupThingFiiiiiiiUl(inner.p0.x, inner.p0.y, inner.p1.x,
        outer.p1.y, no_blend ? 0x10 : 0x00, 0x10, 1, color);

    // FIXME: There is a vanilla bug here, the outline attachments should be
    //        drawn under the shadowed region. They appear a bit too bright
    //        because they are drawn on top of shadow
    // Right side outline attachment to the window
    DrawLine__10SetupThingFiiiiUliff(inner.p1.x - 2, outer.p1.y + 2,
        inner.p1.x - 2, outer.p1.y - 4, clear_white, true, 0.0f, 100.0f);
    // Left side outline attachment to the window
    DrawLine__10SetupThingFiiiiUliff(inner.p0.x + 2, outer.p1.y + 2,
        inner.p0.x + 2, outer.p1.y - 4, clear_white, true, 0.0f, 100.0f);
  }

  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MAGFILTER, D3DTFG_LINEAR);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MAGFILTER, D3DTFG_LINEAR);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MINFILTER, D3DTFG_LINEAR);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MINFILTER, D3DTFG_LINEAR);

  *GetDrawAlpha__10SetupThingFv() = prev_alpha;
}

void __cdecl DrawBevBox__10SetupThingFiiiiiiiUl(int x_min, int y_min, int x_max,
    int y_max, uint32_t style, uint32_t outline_thickness,
    uint32_t horizontal_outline, struct LH3DColor color) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(x_min=%d, y_min=%d, x_max=%d, y_max=%d, theme=%d, "
                         "outline_thickness=0x%08X, "
                         "horizontal_outline=0x%08X%s, color=0x%08X) from %p",
      x_min, y_min, x_max, y_max, style, outline_thickness, horizontal_outline,
      (horizontal_outline & 0b11) == 0b11
          ? "(top+bottom)"
          : ((horizontal_outline & 0b11) == 0b01
                    ? "(top)"
                    : ((horizontal_outline & 0b11) == 0b10 ? "(bottom)" : "")),
      *(uint32_t*)&color, puEBP[1]);

  float v_offset;
  float u_offset;
  float uv_extent;
  if (outline_thickness == 0x10) {
    u_offset = 1.0f / 32.0f;
    v_offset = 1.0f / 32.0f;
    uv_extent = 1.0f / 32.0f;
  }
  else {
    u_offset = 0.0f;
    v_offset = 0.0f;
    uv_extent = 1.0f / (float)outline_thickness;
  }

  float u_min = (float)(style % 16) / 16.0f;
  float v_min = (float)(style / 16) / 16.0f;

  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MAGFILTER, D3DTFG_POINT);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MAGFILTER, D3DTFG_POINT);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MINFILTER, D3DTFG_POINT);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MINFILTER, D3DTFG_POINT);

  DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(x_min, y_min,
      x_max, y_max, u_min + u_offset, v_min + v_offset, u_min + uv_extent,
      v_min + uv_extent, Get__10SetupThingFv()->ui_shadow_material, &color, 1, -0xa000,
      0xa000, false, 100.0f);

  bool is_theme =
      style == 0xd || style == 0xb || style == 0x2d || style == 0x2b;
  struct LH3DColor outline_color;
  if (outline_thickness == 0x10 || (outline_thickness == 8 && is_theme)) {
    bool is_thickness = outline_thickness == 8;
    outline_color = LH3DColor_ARRAY_008ab250[style % _countof(LH3DColor_ARRAY_008ab250)];
    if (is_thickness) {
      if ((style % 0x20) == 0xd) {
        outline_color = LH3DColor_ARRAY_008ab250[2];
      }
      else {
        outline_color = LH3DColor_ARRAY_008ab250[1];
      }
    }

    struct LH3DColor line_color = {
      .b = (uint8_t)(((uint32_t)outline_color.b * color.b) >> 8u),
      .g = (uint8_t)(((uint32_t)outline_color.g * color.g) >> 8u),
      .r = (uint8_t)(((uint32_t)outline_color.r * color.r) >> 8u),
      .a = color.a,
    };

    y_min += 2;
    if ((horizontal_outline & 1U) == 0) {
      DrawLine__10SetupThingFiiiiUliff(
          x_min + 2, y_min, x_min + 4, y_min, line_color, 1, 0.0f, 100.0f);
      DrawLine__10SetupThingFiiiiUliff(
          x_max - 4, y_min, x_max - 2, y_min, line_color, 1, 0.0f, 100.0f);
    }
    else {
      DrawLine__10SetupThingFiiiiUliff(
          x_min + 2, y_min, x_max - 2, y_min, line_color, 1, 0.0f, 100.0f);
    }
    x_min += 2;
    x_max -= 2;
    if ((horizontal_outline & 2U) != 0) {
      DrawLine__10SetupThingFiiiiUliff(
          x_max, y_max - 2, x_min, y_max - 2, line_color, 1, 0.0f, 100.0f);
    }
    DrawLine__10SetupThingFiiiiUliff(
        x_min, y_max - 2, x_min, y_min, line_color, 1, 0.0f, 100.0f);
    DrawLine__10SetupThingFiiiiUliff(
        x_max, y_min, x_max, y_max - 2, line_color, 1, 0.0f, 100.0f);
  }

  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MAGFILTER, D3DTFG_LINEAR);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MAGFILTER, D3DTFG_LINEAR);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      0, D3DTSS_MINFILTER, D3DTFG_LINEAR);
  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      1, D3DTSS_MINFILTER, D3DTFG_LINEAR);
}
