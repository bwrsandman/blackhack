#ifndef BLACKHACK_SETUPBUTTON_H
#define BLACKHACK_SETUPBUTTON_H

#include "SetupControl.h"

struct SetupButton {
  struct SetupControl super;
  bool pressed;
  int field_0x240;
};
static_assert(sizeof(struct SetupButton) == 0x244, "struct size is wrong");

// win1.41 004097a0 mac 100c7c90 SetupButton::Draw(bool, bool)
void __fastcall Draw__11SetupButtonFbb(struct SetupButton *this, const void* edx, bool hovered, bool selected);
// win1.41 004098b0 mac 103dd710 SetupButton::SetupButton(int, int, int, int, int, wchar_t *, int)
struct SetupButton* __fastcall ct__10SetupButtonFiiiiiPwi(
    struct SetupButton* this, const void* edx, int id, int x, int y, int width,
    int height, const wchar_t* label, int param_8);
// win1.41 00409900 mac 101104d0 SetupButton::MouseDown(int, int, bool)
void __fastcall MouseDown__10SetupButtonFiib(struct SetupButton* this, const void* edx, int x, int y, bool param_3);
// win1.41 00409910 mac 10172660 SetupButton::MouseUp(int, int, bool)
void __fastcall MouseUp__10SetupButtonFiib(struct SetupButton* this, const void* edx, int x, int y, bool param_3);
// win1.41 00409920 mac 1034a2d0 SetupButton::KeyDown(int, int)
void __fastcall KeyDown__11SetupButtonFii(struct SetupButton* this, const void* edx, enum LHKey key, enum LHKeyMod mod);
// win1.41 00409940 mac 10594240 SetupButton::~SetupButton(void)
struct SetupButton* __fastcall dt__11SetupButtonFb(struct SetupButton* this, const void* edx, bool param_1);

// win1.41 008ab2b4 mac none SetupButton virtual table
static struct vt__SetupControl_t vt__SetupButton = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__11SetupButtonFbb,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
    .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__10SetupButtonFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__11SetupButtonFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__11SetupButtonFb,
};

#endif // BLACKHACK_SETUPBUTTON_H
