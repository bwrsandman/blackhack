#include "SetupPicture.h"
#include "Setup.h"
#include "SetupThing.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

#include <logger.h>

#include <LH3DTech.h>
#include <LHMatrix.h>
#include <LHSys.h>

void __fastcall MouseDown__12SetupPictureFiib(
    struct SetupPicture* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  MouseDown__10SetupButtonFiib(&this->super, edx, x, y, param_3);

  this->hovered_picture_index = -1;

  if (!param_3) {
    return;
  }

  if (this->draggable) {
    this->dragging = true;
  }
  if (!this->clickable) {
    return;
  }
  this->zoomer.field_0x20 = this->zoomer.speed_m1;
  this->zoomer.speed = 0.0f;
  this->zoomer.time_m1 = 0.0f;
  this->zoomer.field_0x1c = this->zoomer.current_value;
  this->zoomer.destination = 1.0f;
  this->zoomer.time = 0.5f;

  struct LHMatrix local_60;
  local_60.m[0] = 1.0f / (128.0f * 3.0f);
  local_60.m[1] = 1.0f / (16.0f * 3.0f);
  local_60.m[2] = 1.0f / 8.0f;
  local_60.m[3] = 1.0f / (16.0f * 3.0f);
  local_60.m[4] = 1.0f / 8.0f;
  local_60.m[5] = 1.0f / 2.0f;
  local_60.m[6] = 1.0f / 8.0f;
  local_60.m[7] = 1.0f / 2.0f;
  local_60.m[8] = 1.0f;
  local_60.m[9] = 0.0f;
  local_60.m[10] = 0.0f;
  local_60.m[11] = 0.0f;

  struct LHMatrix local_30;
  SetInverse__8LHMatrixFRC8LHMatrix(&local_30, NULL, &local_60);

  float fVar1 = this->zoomer.destination - this->zoomer.field_0x1c -
                this->zoomer.field_0x20 * this->zoomer.time;
  float fVar2 = this->zoomer.speed - this->zoomer.field_0x20;

  this->zoomer.field_0x24.x =
      local_30.m[2] * fVar1 + local_30.m[5] * fVar2 + local_30.m[11];
  this->zoomer.field_0x24.y =
      local_30.m[4] * fVar2 + local_30.m[1] * fVar1 + local_30.m[10];
  this->zoomer.field_0x24.z =
      local_30.m[0] * fVar1 + local_30.m[3] * fVar2 + local_30.m[9];
}

void __fastcall MouseUp__12SetupPictureFiib(
    struct SetupPicture* this, const void* edx, int x, int y, bool param_3) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d, %s) from %p", this, x, y,
      param_3 ? "true" : "false", puEBP[1]);

  MouseUp__10SetupButtonFiib(&this->super, edx, x, y, param_3);

  if (!param_3) {
    return;
  }

  if (this->draggable) {
    if (this->dragging) {
      if (this->super.super.setup_box->field_0xb0 != NULL) {
        this->super.super.setup_box->field_0xb0(
            11, this->super.super.setup_box, &this->super.super, x, y);
      }
    }
    this->dragging = false;
  }

  if (!this->clickable) {
    return;
  }

  if (this->hovered_picture_index > -1) {
    this->picture_index = this->hovered_picture_index % this->num_pictures;
  }
  this->zoomer.field_0x20 = this->zoomer.speed_m1;
  this->zoomer.field_0x1c = this->zoomer.current_value;
  this->zoomer.destination = 0.0f;
  this->zoomer.speed = 0.0f;
  this->zoomer.time = 1.0f;
  this->zoomer.time_m1 = 0.0f;

  struct LHMatrix LStack96;
  LStack96.m[0] = 1.0f / 24.0f;
  LStack96.m[1] = 1.0f / 6.0f;
  LStack96.m[2] = 1.0f / 2.0f;
  LStack96.m[3] = 1.0f / 6.0f;
  LStack96.m[4] = 1.0f / 2.0f;
  LStack96.m[5] = 1.0f;
  LStack96.m[6] = 1.0f / 2.0f;
  LStack96.m[7] = 1.0f;
  LStack96.m[8] = 1.0f;
  LStack96.m[9] = 0.0f;
  LStack96.m[10] = 0.0f;
  LStack96.m[11] = 0.0f;

  struct LHMatrix LStack48;
  SetInverse__8LHMatrixFRC8LHMatrix(&LStack48, NULL, &LStack96);

  float fVar3 = this->zoomer.destination - this->zoomer.field_0x1c -
                this->zoomer.field_0x20 * this->zoomer.time;
  float fVar4 = this->zoomer.speed - this->zoomer.field_0x20;
  this->zoomer.field_0x24.x =
      LStack48.m[2] * fVar3 + LStack48.m[5] * fVar4 + LStack48.m[11];
  this->zoomer.field_0x24.y =
      LStack48.m[4] * fVar4 + LStack48.m[1] * fVar3 + LStack48.m[10];
  this->zoomer.field_0x24.z =
      LStack48.m[0] * fVar3 + LStack48.m[3] * fVar4 + LStack48.m[9];
}

void __fastcall KeyDown__12SetupPictureFii(struct SetupPicture* this,
    const void* edx, enum LHKey key, enum LHKeyMod mod) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, key=0x%08x, mod=0x%08x) from %p", this, key, mod,
      puEBP[1]);

  if (this->super.super.setup_box != NULL) {
    LOG_DEBUG("%p", this->super.super.setup_box->vftptr->field_0x0);
    this->super.super.setup_box->vftptr->field_0x0(
        this->super.super.setup_box, edx, key, mod);
  }
}

void __fastcall Click__12SetupPictureFii(
    struct SetupPicture* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);
}

void __fastcall Drag__12SetupPictureFii(
    struct SetupPicture* this, const void* edx, int x, int y) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, x=%d, y=%d) from %p", this, x, y, puEBP[1]);
}

void __fastcall Draw__12SetupPictureFbb(
    struct SetupPicture* this, const void* edx, bool hovered, bool selected) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, hovered=%s, selected=%s) from %p", this,
      hovered ? "true" : "false", selected ? "true" : "false", puEBP[1]);

  struct LH3DColor clear_color = {
    .b = 0x00,
    .g = 0x00,
    .r = 0x00,
    .a = 0x00,
  };

  this->zoomer.time_m1 += GetDeltaTime__8LH3DTechFv() / 1000.0f;
  if (this->zoomer.time_m1 < this->zoomer.time) {
    float fVar2 = this->zoomer.time_m1 * this->zoomer.time_m1 * 0.5f;
    float x_4 = fVar2 * this->zoomer.time_m1 / 3.0f;
    this->zoomer.speed_m1 = x_4 * this->zoomer.field_0x24.z +
                            fVar2 * this->zoomer.field_0x24.y +
                            this->zoomer.time_m1 * this->zoomer.field_0x24.x +
                            this->zoomer.field_0x20;
    this->zoomer.current_value =
        this->zoomer.field_0x20 * this->zoomer.time_m1 +
        fVar2 * this->zoomer.field_0x24.x + x_4 * this->zoomer.field_0x24.y +
        fVar2 * fVar2 / 6.0f * this->zoomer.field_0x24.z +
        this->zoomer.field_0x1c;
  }
  else {
    this->zoomer.current_value = this->zoomer.destination;
    this->zoomer.speed_m1 = this->zoomer.speed;
    this->zoomer.time_m2 = 0.0f;
    this->zoomer.time_m1 = this->zoomer.time;
  }

  int picture_index = this->hovered_picture_index < 0
                          ? this->picture_index
                          : this->hovered_picture_index;


  float u_min_main =
      (float)(picture_index % this->num_rows) / (float)this->num_rows +
      1.0f / 512.0f;
  float v_min_main =
      (float)(picture_index / this->num_rows) / (float)this->num_rows +
      1.0f / 512.0f;
  float uv_extent = 1.0f / (float)this->num_rows - 2.0f / 512.0f;

  struct LH3DColor color;
  if (*(uint32_t*)&this->tint != 0x0) {
    color = this->tint;
  }
  else if (selected == false) {
    color = LH3DColor_ARRAY_00c4ccd8_0;
  }
  else {
    color = LH3DColor_00c4ccf8;
  }

  // Shadow
  if (this->material != NULL && *(uint32_t*)&this->tint == 0x0) {
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
        this->super.super.rect.p0.x + 2, this->super.super.rect.p0.y + 2,
        this->super.super.rect.p1.x + 2, this->super.super.rect.p1.y + 2,
        u_min_main, v_min_main, u_min_main + uv_extent, v_min_main + uv_extent,
        this->material, &LH3DColor_ARRAY_00c4ccd8_1, true, -0xa000, 0xa000,
        false, 100.0f);
  }

  struct LHCoord mouse;
  ct__7LHCoordFRC7LHCoord(&mouse, edx, &Get__5LHSysFv()->mouse.coord_0xbc);
  unadjust__10SetupThingFRiRi(&mouse.x, &mouse.y);

  uint8_t prev_alpha = *GetDrawAlpha__10SetupThingFv();
  if (this->material != NULL) {
    const struct LH3DColor* p_color;
    if (hovered == false) {
      p_color = &color;
    }
    else {
      p_color = &LH3DColor_ARRAY_00c4ccd8_3;
    }

    // Actual Picture
    DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
        this->super.super.rect.p0.x, this->super.super.rect.p0.y,
        this->super.super.rect.p1.x, this->super.super.rect.p1.y, u_min_main,
        v_min_main, u_min_main + uv_extent, v_min_main + uv_extent,
        this->material, p_color, true, -0xa000, 0xa000, false, 100.0f);

    // FIXME: This store is likely redundant
    prev_alpha = *GetDrawAlpha__10SetupThingFv();

    const uint8_t fan_progress =
        (uint8_t)(max(0.0f, min(this->zoomer.current_value * 255.0f, 255.0f)));
    if (fan_progress > 5) {
      int r_1 = (this->super.super.rect.p1.y - this->super.super.rect.p0.y) / 2;
      int quarter_fan_length = (int)((float)r_1 * 3.0f / 4.0f);
      int r_2 = r_1 + quarter_fan_length * 4;
      int r_3 = r_2 + 3;
      int r_4 = r_1 - 3;
      int inner_selection_radius = r_1 + quarter_fan_length;
      int outer_selection_radius = r_1 + quarter_fan_length * 3;

      struct LHCoord center = {
        .x = (this->super.super.rect.p0.x + this->super.super.rect.p1.x) / 2,
        .y = (this->super.super.rect.p0.y + this->super.super.rect.p1.y) / 2,
      };

      struct LH3DColor fan_color = { .a = (uint8_t)(fan_progress / 2) };

      // Draw disc as 32 quads in a fan
      for (uint32_t i = 0; i < 32; ++i) {
        float cos_0 = cosf((float)i * (float)M_PI / 16.0f);
        float sin_0 = sinf((float)i * (float)M_PI / 16.0f);
        float cos_1 = cosf((float)(i + 1) * (float)M_PI / 16.0f);
        float sin_1 = sinf((float)(i + 1) * (float)M_PI / 16.0f);

        // Inner Circle
        struct LHCoord p_1 = {
          .x = (int)(sin_0 * (float)r_1 + (float)center.x),
          .y = (int)(cos_0 * (float)r_1 + (float)center.y),
        };
        struct LHCoord p_2 = {
          .x = (int)(sin_1 * (float)r_1 + (float)center.x),
          .y = (int)(cos_1 * (float)r_1 + (float)center.y),
        };

        // Outer Circle
        struct LHCoord p_3 = {
          .x = (int)(sin_1 * (float)r_2 + (float)center.x),
          .y = (int)(cos_1 * (float)r_2 + (float)center.y),
        };
        struct LHCoord p_4 = {
          .x = (int)(sin_0 * (float)r_2 + (float)center.x),
          .y = (int)(cos_0 * (float)r_2 + (float)center.y),
        };

        // Outer Outer Circle
        struct LHCoord p_5 = {
          .x = (int)(sin_0 * (float)r_3 + (float)center.x),
          .y = (int)(cos_0 * (float)r_3 + (float)center.y),
        };
        struct LHCoord p_6 = {
          .x = (int)(sin_1 * (float)r_3 + (float)center.x),
          .y = (int)(cos_1 * (float)r_3 + (float)center.y),
        };

        // Inner Inner Circle
        struct LHCoord p_7 = {
          .x = (int)(sin_0 * (float)r_4 + (float)center.x),
          .y = (int)(cos_0 * (float)r_4 + (float)center.y),
        };
        struct LHCoord p_8 = {
          .x = (int)(sin_1 * (float)r_4 + (float)center.x),
          .y = (int)(cos_1 * (float)r_4 + (float)center.y),
        };

        // Main Fan
        DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl(p_1.x, p_1.y, p_2.x, p_2.y,
            p_3.x, p_3.y, p_4.x, p_4.y, fan_color, fan_color, fan_color,
            fan_color, false, true);

        // Fan Outer Shadow
        DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl(p_5.x, p_5.y, p_6.x, p_6.y,
            p_3.x, p_3.y, p_4.x, p_4.y, clear_color, clear_color, fan_color,
            fan_color, false, true);

        // Fan Inner Shadow
        DrawQuad__10SetupThingFiiiiiiiiUlUlUlUlUlUl(p_7.x, p_7.y, p_8.x, p_8.y,
            p_2.x, p_2.y, p_1.x, p_1.y, clear_color, clear_color, fan_color,
            fan_color, false, true);

        prev_alpha = *GetDrawAlpha__10SetupThingFv();
      }

      *GetDrawAlpha__10SetupThingFv() =
          (uint8_t)(((uint32_t)*GetDrawAlpha__10SetupThingFv() *
                        (uint32_t)fan_progress) >>
                    8u);

      struct LHCoord m2c = {
        .x = center.x - mouse.x,
        .y = center.y - mouse.y,
      };

      if (r_3 * r_3 < m2c.x * m2c.x + m2c.y * m2c.y) {
        this->hovered_picture_index = -1;
      }

      float local_84 = (float)(0xff - fan_progress) * 0.00616f;
      int iVar5 = quarter_fan_length * (int)fan_progress >> 8;

      // Inner selection
      int count = this->num_pictures / 3 + 1;
      for (int i = 0; i < count; ++i) {
        float u_min =
            (float)(i % this->num_rows) / (float)this->num_rows + 1.0f / 512.0f;
        float v_min =
            (float)(i / this->num_rows) / (float)this->num_rows + 1.0f / 512.0f;

        float theta = (float)i * 2.0f * (float)M_PI / (float)count + local_84;

        struct LHCoord box_center = {
          .x = (int)((float)center.x +
                     sinf(theta) * (float)inner_selection_radius),
          .y = (int)((float)center.y -
                     cosf(theta) * (float)inner_selection_radius),
        };

        struct LHCoord m2b = {
          .x = box_center.x - mouse.x,
          .y = box_center.y - mouse.y,
        };

        bool highlighted = m2b.x * m2b.x + m2b.y * m2b.y < iVar5 * iVar5;

        DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
            box_center.x - iVar5 + 2, box_center.y - iVar5 + 2,
            box_center.x + iVar5 + 2, box_center.y + iVar5 + 2, u_min, v_min,
            u_min + uv_extent, v_min + uv_extent, this->material,
            &LH3DColor_ARRAY_00c4ccd8_1, true, -0xa000, 0xa000, false, 100.0f);

        if (highlighted) {
          p_color = &LH3DColor_ARRAY_00c4ccd8_3;
        }
        else {
          p_color = &LH3DColor_00c4ccf8;
        }

        DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
            box_center.x - iVar5, box_center.y - iVar5, box_center.x + iVar5,
            box_center.y + iVar5, u_min, v_min, u_min + uv_extent,
            v_min + uv_extent, this->material, p_color, true, -0xa000, 0xa000,
            false, 100.0f);

        if (highlighted && fan_progress > 0xfd) {
          this->hovered_picture_index = i;
        }
      }

      // Outer selection
      for (int i = count; i < this->num_pictures; ++i) {
        float u_min =
            (float)(i % this->num_rows) / (float)this->num_rows + 1.0f / 512.0f;
        float v_min =
            (float)(i / this->num_rows) / (float)this->num_rows + 1.0f / 512.0f;

        float theta = (float)(i - count) * 2.0f * (float)M_PI /
                          (float)(this->num_pictures - count) -
                      local_84;

        struct LHCoord box_center = {
          .x = (int)((float)center.x +
                     sinf(theta) * (float)outer_selection_radius),
          .y = (int)((float)center.y -
                     cosf(theta) * (float)outer_selection_radius),
        };

        struct LHCoord m2b = {
          .x = box_center.x - mouse.x,
          .y = box_center.y - mouse.y,
        };

        bool highlighted = m2b.x * m2b.x + m2b.y * m2b.y < iVar5 * iVar5;

        DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
            box_center.x - iVar5 + 2, box_center.y - iVar5 + 2,
            box_center.x + iVar5 + 2, box_center.y + iVar5 + 2, u_min, v_min,
            u_min + uv_extent, v_min + uv_extent, this->material,
            &LH3DColor_ARRAY_00c4ccd8_1, true, -0xa000, 0xa000, false, 100.0f);

        if (highlighted) {
          p_color = &LH3DColor_ARRAY_00c4ccd8_3;
        }
        else {
          p_color = &LH3DColor_00c4ccf8;
        }

        DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
            box_center.x - iVar5, box_center.y - iVar5, box_center.x + iVar5,
            box_center.y + iVar5, u_min, v_min, u_min + uv_extent,
            v_min + uv_extent, this->material, p_color, true, -0xa000, 0xa000,
            false, 100.0f);
        if (highlighted && fan_progress > 0xfd) {
          this->hovered_picture_index = i;
        }
      }
    }
  }

  *GetDrawAlpha__10SetupThingFv() = prev_alpha;

  if (this->dragging) {
    if (this->material != NULL && *(uint32_t*)&this->tint == 0x0) {
      DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
          mouse.x - 32 - 2, mouse.y - 32 + 2,
          mouse.x - 32 + this->super.super.rect.p1.x -
              this->super.super.rect.p0.x - 2,
          mouse.y - 32 + this->super.super.rect.p1.y -
              this->super.super.rect.p0.y + 2,
          u_min_main, v_min_main, u_min_main + uv_extent,
          v_min_main + uv_extent, this->material, &LH3DColor_ARRAY_00c4ccd8_1,
          true, -0xa000, 0xa000, false, 100.0f);
    }
    if (this->material != NULL) {
      struct LH3DColor inv_color = {
        .b = (uint8_t)(color.b ^ 0xFFu),
        .g = (uint8_t)(color.g ^ 0xFFu),
        .r = (uint8_t)(color.r ^ 0xFFu),
        .a = color.a,
      };

      struct LH3DColor* p_color;
      if (this->super.super.field_0x1c < 0) {
        p_color = &color;
      }
      else if ((GetTickCount() / 400) % 2) {
        p_color = &inv_color;
      }
      else {
        p_color = &color;
      }

      DrawBox__10SetupThingFiiiiffffP12LH3DMaterialP9LH3DColoriiibf(
          mouse.x - 32, mouse.y - 32,
          mouse.x - 32 + this->super.super.rect.p1.x -
              this->super.super.rect.p0.x,
          mouse.y - 32 + this->super.super.rect.p1.y -
              this->super.super.rect.p0.y,
          u_min_main, v_min_main, u_min_main + uv_extent,
          v_min_main + uv_extent, this->material, p_color, true, -0xa000,
          0xa000, false, 100.0f);
    }
    if (this->super.super.setup_box != NULL) {
      SetFocusControl__8SetupBoxFP12SetupControl(
          this->super.super.setup_box, edx, &this->super.super);
    }
  }
}

struct SetupPicture* __fastcall ct__12SetupPictureFiiiP12LH3DMaterialiibib(
    struct SetupPicture* this, const void* edx, int id, int x, int y,
    struct LH3DMaterial* material, int picture_index, int num_rows,
    bool clickable, int size, bool draggable) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(
      __FUNCTION__ "(%p, id=%d, x=%d, y=%d, material=%p, picture_index=%d, "
                   "num_rows=%d, clickable=%s, size=%d, draggable=%s from %p",
      this, id, x, y, material, picture_index, num_rows,
      clickable ? "true" : "false", size, draggable ? "true" : "false",
      puEBP[1]);

  struct LH3DColor clear_color = {
    .b = 0x00,
    .g = 0x00,
    .r = 0x00,
    .a = 0x00,
  };

  ct__10SetupButtonFiiiiiPwi(&this->super, edx, id, x, y, size, size,
      Get__10SetupThingFv()->WCHAR_00c4cd30, 0);
  this->super.super.vftptr = &vt__SetupPicture;

  SetFocus__12SetupPictureFb(this, edx, false);

  this->super.pressed = false;
  this->material = material;
  this->tint = clear_color;
  this->draggable = draggable;
  this->dragging = false;
  this->picture_index = picture_index;
  this->num_rows = num_rows;
  this->num_pictures = num_rows * num_rows;
  this->clickable = clickable;

  return this;
}

struct SetupPicture* __fastcall dt__12SetupPictureFb(
    struct SetupPicture* this, const void* edx, bool param_1) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, %s) from %p", this, param_1 ? "true" : "false",
      puEBP[1]);

  dt__11SetupButtonFb(&this->super, edx, param_1);

  return this;
}

void __fastcall SetFocus__12SetupPictureFb(
    struct SetupPicture* this, const void* edx, bool focus) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, focus=\"%s\") from 0x%p", this,
      focus ? "true" : "false", puEBP[1]);

  if (focus) {
    return;
  }

  this->hovered_picture_index = -1;

  ct__6ZoomerFv(&this->zoomer);
  this->zoomer.destination = 0.0f;
  this->zoomer.current_value = 0.0f;
  this->zoomer.field_0x1c = 0.0f;
  this->zoomer.time = 0.0f;
  this->zoomer.time_m1 = 0.0f;
  this->zoomer.field_0x24.z = 0.0f;
  this->zoomer.field_0x24.y = 0.0f;
  this->zoomer.time_m2 = 0.0f;
  this->zoomer.field_0x24.x = 0.0f;
  this->zoomer.speed_m1 = 0.0f;
  this->zoomer.field_0x20 = 0.0f;
  this->zoomer.speed = 0.0f;

  SetDestinationWithSpeedAndTime__6ZoomerFfff(
      &this->zoomer, edx, 0.0f, 0.0f, 0.0f);
}
