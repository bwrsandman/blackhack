#ifndef BLACKHACK_SETUPSTATICTEXT_H
#define BLACKHACK_SETUPSTATICTEXT_H

#include "SetupControl.h"

/// Since the constructor is inlined, it is difficult to override the virtual
/// table. One solution is to override the original virtual table pointers

struct SetupStaticText {
  struct SetupControl super;
  enum TEXTJUSTIFY text_justify;
  int display_text_size;
};
static_assert(sizeof(struct SetupStaticText) == 0x244, "struct size is wrong");

// win1.41 inlined mac 10327f40 SetupStaticText::SetupStaticText(int, int, int, int, int, wchar_t *, TEXTJUSTIFY)
struct SetupStaticText* __fastcall ct__15SetupStaticTextFiiiiiPw11TEXTJUSTIFY(
    struct SetupStaticText* this, const void* edx, int id, int x, int y,
    int width, int height, const wchar_t* label, enum TEXTJUSTIFY text_justify);
// win1.41 00409430 mac 105436e0 SetupStaticText::Draw(bool, bool)
void __fastcall Draw__15SetupStaticTextFbb(struct SetupStaticText* this, const void* edx, bool hovered, bool selected);
// win1.41 00411670 mac 100cb300 SetupStaticText::~SetupStaticText(void)
struct SetupStaticText* __fastcall dt__15SetupStaticTextFb(struct SetupStaticText* this, const void* edx, bool param_1);

// win1.41 008ab5c0 mac none SetupStaticText virtual table
static struct vt__SetupControl_t vt__SetupStaticText = {
    .SetToolTipUl = SetToolTip__12SetupControlFUl,
    .SetToolTipPw = SetToolTip__12SetupControlFPw,
    .Hide = Hide__12SetupControlFb,
    .SetFocus = SetFocus__12SetupControlFb,
    .HitTest = HitTest__12SetupControlFii,
    .Draw = (vt__SetupControl_Draw_t*)Draw__15SetupStaticTextFbb,
    .Drag = Drag__12SetupControlFii,
    .MouseDown = MouseDown__12SetupControlFiib,
    .MouseUp = MouseUp__12SetupControlFiib,
    .Click = Click__12SetupControlFii,
    .KeyDown = KeyDown__12SetupControlFii,
    .Char = Char__12SetupControlFi,
    .dtor = (vt__SetupControl_dtor_t*)dt__15SetupStaticTextFb,
};

#endif // BLACKHACK_SETUPSTATICTEXT_H
