#ifndef BLACKHACK__SETUPHSBARGRAPH_H
#define BLACKHACK__SETUPHSBARGRAPH_H

#include "SetupVBarGraph.h"

/// Since the constructor is inlined, it is difficult to override the virtual
/// table. One solution is to override the original virtual table pointers

struct SetupHSBarGraph {
  struct SetupVBarGraph super;
};
static_assert(sizeof(struct SetupHSBarGraph) == 0x284, "Struct is of wrong size");

// win1.41 inlined SetupHSBarGraph::SetupHSBarGraph(int, int, int, int, int, wchar_t *)
struct SetupHSBarGraph * __fastcall ct__15SetupHSBarGraphFiiiiiPw(struct SetupHSBarGraph *this, const void* edx, int id, int x, int y, int width, int height, const wchar_t *label);
// win1.41 0040d3c0 mac 100df500 SetupHSBarGraph::Draw(bool, bool)
void __fastcall Draw__15SetupHSBarGraphFbb(struct SetupHSBarGraph* this, const void* edx, bool hovered, bool selected);
// win1.41 0040d9a0 mac 10130720 SetupHSBarGraph::SetScale(float)
void __fastcall SetScale__15SetupHSBarGraphFf(struct SetupHSBarGraph *this, const void* edx, float scale);
// win1.41 0056d960 mac 100cd050 SetupHSBarGraph::~SetupHSBarGraph(void)
struct SetupHSBarGraph* __fastcall dt__15SetupHSBarGraphFb(struct SetupHSBarGraph* this, const void* edx, bool param_1);

// win1.41 008ffee4 mac none SetupHSBarGraph virtual table
static struct vt__SetupVBarGraph_t vt__SetupHSBarGraph = {
    .super = {
        .SetToolTipUl = SetToolTip__12SetupControlFUl,
        .SetToolTipPw = SetToolTip__12SetupControlFPw,
        .Hide = Hide__12SetupControlFb,
        .SetFocus = SetFocus__12SetupControlFb,
        .HitTest = HitTest__12SetupControlFii,
        .Draw = (vt__SetupControl_Draw_t*)Draw__15SetupHSBarGraphFbb,
        .Drag = Drag__12SetupControlFii,
        .MouseDown = (vt__SetupControl_Mouse_t*)MouseDown__10SetupButtonFiib,
        .MouseUp = (vt__SetupControl_Mouse_t*)MouseUp__10SetupButtonFiib,
        .Click = Click__12SetupControlFii,
        .KeyDown = (vt__SetupControl_KeyDown_t*)KeyDown__14SetupVBarGraphFii,
        .Char = Char__12SetupControlFi,
        .dtor = (vt__SetupControl_dtor_t*)dt__15SetupHSBarGraphFb,
    },
    .Reset = Reset__14SetupVBarGraphFv,
    .SetScale = (vt_SetupVBarGraph_SetScale_t*)SetScale__15SetupHSBarGraphFf,
    .AddBar = AddBar__14SetupVBarGraphFRC8VBarData,
    .SetBar = SetBar__14SetupVBarGraphFiRC8VBarData,
    .GetBar = GetBar__14SetupVBarGraphFiR8VBarData,
};

#endif // BLACKHACK_SETUPHSBARGRAPH_H
