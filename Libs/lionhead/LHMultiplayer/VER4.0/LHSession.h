#ifndef BLACKHACK_LHSESSION_H
#define BLACKHACK_LHSESSION_H

#include <stdint.h>

struct LHSession {
  uint8_t field_0x0;
  uint8_t field_0x1;
  uint8_t field_0x2;
  uint8_t field_0x3;
  int field_0x4;
  uint8_t field_0x8;
  uint8_t field_0x9;
  uint8_t field_0xa;
  uint8_t field_0xb;
  uint8_t field_0xc;
  uint8_t field_0xd;
  uint8_t field_0xe;
  uint8_t field_0xf;
  uint8_t field_0x10;
  uint8_t field_0x11;
  uint8_t field_0x12;
  uint8_t field_0x13;
  uint8_t field_0x14;
  uint8_t field_0x15;
  uint8_t field_0x16;
  uint8_t field_0x17;
  uint8_t field_0x18;
  uint8_t field_0x19;
  uint8_t field_0x1a;
  uint8_t field_0x1b;
  uint8_t field_0x1c;
  uint8_t field_0x1d;
  uint8_t field_0x1e;
  uint8_t field_0x1f;
  uint8_t field_0x20;
  uint8_t field_0x21;
  uint8_t field_0x22;
  uint8_t field_0x23;
  uint8_t field_0x24;
  uint8_t field_0x25;
  uint8_t field_0x26;
  uint8_t field_0x27;
  uint8_t field_0x28;
  uint8_t field_0x29;
  uint8_t field_0x2a;
  uint8_t field_0x2b;
  uint8_t field_0x2c;
  uint8_t field_0x2d;
  uint8_t field_0x2e;
  uint8_t field_0x2f;
  uint8_t field_0x30;
  uint8_t field_0x31;
  uint8_t field_0x32;
  uint8_t field_0x33;
  uint8_t field_0x34;
  uint8_t field_0x35;
  uint8_t field_0x36;
  uint8_t field_0x37;
  uint8_t field_0x38;
  uint8_t field_0x39;
  uint8_t field_0x3a;
  uint8_t field_0x3b;
  uint8_t field_0x3c;
  uint8_t field_0x3d;
  uint8_t field_0x3e;
  uint8_t field_0x3f;
  uint8_t field_0x40;
  uint8_t field_0x41;
  uint8_t field_0x42;
  uint8_t field_0x43;
  uint8_t field_0x44;
  uint8_t field_0x45;
  uint8_t field_0x46;
  uint8_t field_0x47;
  uint8_t field_0x48;
  uint8_t field_0x49;
  uint8_t field_0x4a;
  uint8_t field_0x4b;
  uint8_t field_0x4c;
  uint8_t field_0x4d;
  uint8_t field_0x4e;
  uint8_t field_0x4f;
  uint8_t field_0x50;
  uint8_t field_0x51;
  uint8_t field_0x52;
  uint8_t field_0x53;
  uint8_t field_0x54;
  uint8_t field_0x55;
  uint8_t field_0x56;
  uint8_t field_0x57;
  uint8_t field_0x58;
  uint8_t field_0x59;
  uint8_t field_0x5a;
  uint8_t field_0x5b;
  uint8_t field_0x5c;
  uint8_t field_0x5d;
  uint8_t field_0x5e;
  uint8_t field_0x5f;
  uint8_t field_0x60;
  uint8_t field_0x61;
  uint8_t field_0x62;
  uint8_t field_0x63;
  uint8_t field_0x64;
  uint8_t field_0x65;
  uint8_t field_0x66;
  uint8_t field_0x67;
  uint8_t field_0x68;
  uint8_t field_0x69;
  uint8_t field_0x6a;
  uint8_t field_0x6b;
  uint8_t field_0x6c;
  uint8_t field_0x6d;
  uint8_t field_0x6e;
  uint8_t field_0x6f;
  uint8_t field_0x70;
  uint8_t field_0x71;
  uint8_t field_0x72;
  uint8_t field_0x73;
  uint8_t field_0x74;
  uint8_t field_0x75;
  uint8_t field_0x76;
  uint8_t field_0x77;
  uint8_t field_0x78;
  uint8_t field_0x79;
  uint8_t field_0x7a;
  uint8_t field_0x7b;
  uint8_t field_0x7c;
  uint8_t field_0x7d;
  uint8_t field_0x7e;
  uint8_t field_0x7f;
  uint8_t field_0x80;
  uint8_t field_0x81;
  uint8_t field_0x82;
  uint8_t field_0x83;
  uint8_t field_0x84;
  uint8_t field_0x85;
  uint8_t field_0x86;
  uint8_t field_0x87;
  uint8_t field_0x88;
  uint8_t field_0x89;
  uint8_t field_0x8a;
  uint8_t field_0x8b;
  int field_0x8c;
  uint8_t field_0x90;
  uint8_t field_0x91;
  uint8_t field_0x92;
  uint8_t field_0x93;
  uint8_t field_0x94;
  uint8_t field_0x95;
  uint8_t field_0x96;
  uint8_t field_0x97;
  uint8_t field_0x98;
  uint8_t field_0x99;
  uint8_t field_0x9a;
  uint8_t field_0x9b;
  uint8_t field_0x9c;
  uint8_t field_0x9d;
  uint8_t field_0x9e;
  uint8_t field_0x9f;
  int field_0xa0;
  uint8_t field_0xa4;
  uint8_t field_0xa5;
  uint8_t field_0xa6;
  uint8_t field_0xa7;
  uint8_t field_0xa8;
  uint8_t field_0xa9;
  uint8_t field_0xaa;
  uint8_t field_0xab;
  uint8_t field_0xac;
  uint8_t field_0xad;
  uint8_t field_0xae;
  uint8_t field_0xaf;
};

// win1.41 1001dab0 mac 10036a3c LHSession::IsSinglePlayer
int __fastcall IsSinglePlayer__9LHSessionFv(struct LHSession *this);

#endif // BLACKHACK_LHSESSION_H
