#ifndef BLACKHACK_UTILS_H
#define BLACKHACK_UTILS_H

#include <stdlib.h>

inline float clampf(float a, float minimum, float maximum) {
  return min(max(a, minimum), maximum);
}

inline float saturatef(float a) {
  return clampf(a, 0.0f, 1.0f);
}

inline int clampi(int a, int minimum, int maximum) {
  return min(max(a, minimum), maximum);
}

#endif // BLACKHACK_UTILS_H
