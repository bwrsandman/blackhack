#ifndef BLACKHACK_LH3DTEXTURE_H
#define BLACKHACK_LH3DTEXTURE_H

#include <stdint.h>

enum TextureFormat_ {
  TextureFormat_0x0 = 0,
};

struct TextureFormat {
  enum TextureFormat_ format;
};

struct LH3DTexture {
  uint32_t field_0x0;
  uint32_t field_0x4;
  uint32_t field_0x8;
  struct LH3DTexture* next;
  uint32_t field_0x10;
  struct TextureFormat format;
  uint32_t id;
};

// win1.41 006186b0 LH3DTexture::Create
struct LH3DTexture* __cdecl Create__11LH3DTextureFPvUlUlP13TextureFormat(void*, unsigned long, unsigned long, struct TextureFormat*);
// win1.41 00838480 mac 100c9060 LH3DTexture::GetThisTexture
struct LH3DTexture* GetThisTexture__11LH3DTextureFUl(unsigned long id);

#endif // BLACKHACK_LH3DTEXTURE_H
