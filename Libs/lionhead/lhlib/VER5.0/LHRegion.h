#ifndef BLACKHACK_LHREGION_H
#define BLACKHACK_LHREGION_H

struct LHRegion {
  int x, y, w, h;
};

#endif // BLACKHACK_LHREGION_H
