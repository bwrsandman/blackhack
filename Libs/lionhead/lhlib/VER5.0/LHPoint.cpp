#include "LHPoint.h"
#include <math.h>

float(__cdecl* pFUN_0086fc30)(float param_1, float param_2) = 0x0086fc30;

struct LHPoint2D* __fastcall __as__7Point2DFRC7Point2D(struct LHPoint2D* this, void* edx, const struct LHPoint2D* other) {
  this->x = other->x;
  this->y = other->y;
  return this;
}

struct LHPoint2D* __fastcall __pl__7Point2DCFRC7Point2D(struct LHPoint2D *this, void* edx, const struct LHPoint2D* lhs, const struct LHPoint2D* rhs) {
  this->x = lhs->x + rhs->x;
  this->y = lhs->y + rhs->y;
  return this;
}

struct LHPoint2D* __fastcall __mi__7Point2DCFRC7Point2D(struct LHPoint2D* this, void* edx, const struct LHPoint2D* lhs, const struct LHPoint2D* rhs) {
  this->x = lhs->x - rhs->x;
  this->y = lhs->y - rhs->y;
  return this;
}

float __fastcall GetHeading__7Point2DCFv(const struct LHPoint2D* param_1) {
  float norm2 = GetSizeSq__7Point2DCFv(param_1);
  if (norm2 > 0.000001f) {
    return (*pFUN_0086fc30)(-param_1->y, param_1->x);
  }
  return 0.0f;
}

void __fastcall SetSize__7Point2DFf(struct LHPoint2D* this, void* edx, float size) {
  size = size / sqrtf(GetSizeSq__7Point2DCFv(this));
  this->x *= size;
  this->y *= size;
}

float __fastcall GetSizeSq__7Point2DCFv(const struct LHPoint2D* this) {
  return this->x * this->x + this->y * this->y;
}

float __fastcall GetRange__7Point2DCFRC7Point2D(
    const struct LHPoint2D* this, void* edx, const struct LHPoint2D* param_1) {
  struct LHPoint2D diff;
  __mi__7Point2DCFRC7Point2D(&diff, edx, this, param_1);
  return sqrtf(GetSizeSq__7Point2DCFv(&diff));
}

struct LHPoint* __fastcall __ct__7LHPointFv(struct LHPoint* this) {
  return this;
}

struct LHPoint* __fastcall __ct__7LHPointFfff(
    struct LHPoint* this, float x, float y, float z) {
  this->x = x;
  this->y = y;
  this->z = z;
  return this;
}

struct LHPoint* __fastcall __ct__7LHPointFRC7LHPoint(
    struct LHPoint* this, struct LHPoint* other) {
  this->x = other->x;
  this->y = other->y;
  this->z = other->z;
  return this;
}

void __fastcall SetNull__7LHPointFv(struct LHPoint* this) {
  this->x = 0.0f;
  this->y = 0.0f;
  this->z = 0.0f;
}

void __fastcall FastNormalize__7LHPointFv(struct LHPoint *this) {
  float norm = sqrtf(this->x * this->x + this->y * this->y + this->z * this->z);
  this->x *= norm;
  this->y *= norm;
  this->z *= norm;
}
