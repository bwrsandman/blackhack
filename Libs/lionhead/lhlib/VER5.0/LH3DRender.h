#ifndef BLACKHACK_LH3DRENDER_H
#define BLACKHACK_LH3DRENDER_H

#include <stdbool.h>
#include <stdint.h>

#include <d3d.h>
#include <d3dtypes.h>

void __cdecl RenderLoadingFrame__Fb(bool param_1);

// win1.41 00412940 mac 10046ae0 LH3DRender::SetRenderState(D3DRENDERSTATETYPE, unsigned long)
int __cdecl SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(D3DRENDERSTATETYPE type, uint32_t value);
int __cdecl GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(D3DRENDERSTATETYPE type, uint32_t* value);

// win1.41 0082a500 mac 10017228 DrawAndClip(D3DPRIMITIVETYPE, unsigned long, Vertex3D *, unsigned long, unsigned short *, unsigned long)
void __cdecl DrawAndClip__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl(
    D3DPRIMITIVETYPE primitive_type, uint32_t fvf, D3DTLVERTEX *vertices,
    uint32_t vertex_count, uint16_t *indices, uint32_t index_count);
// win1.41 0082a5b0 mac 1004fe98 DrawAndClip2D(D3DPRIMITIVETYPE, unsigned long, Vertex3D *, unsigned long, unsigned short *, unsigned long)
void __cdecl DrawAndClip2D__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl(
    D3DPRIMITIVETYPE primitive_type, uint32_t fvf, D3DTLVERTEX *vertices,
    uint32_t vertex_count, uint16_t *indices, uint32_t index_count);

// win1.41 0082b9c0 mac 1002dbb0 LH3DRender::SetTextureStageState(unsigned long, D3DTEXTURESTAGESTATETYPE, unsigned long)
int __cdecl SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(uint32_t index, D3DTEXTURESTAGESTATETYPE type, uint32_t value);

// win1.41 0082ff10 mac 10046bbc LH3DRender::SetD3DTillingOn(int)
void __cdecl SetD3DTillingOn__10LH3DRenderFi(uint32_t index);
// win1.41 0082ff50 mac 10046c4c LH3DRender::SetD3DTillingOff(int)
void __cdecl SetD3DTillingOff__10LH3DRenderFi(uint32_t index);

#endif // BLACKHACK_LH3DRENDER_H
