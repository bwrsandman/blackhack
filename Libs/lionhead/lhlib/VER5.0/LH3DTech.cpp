#include "LH3DTech.h"

#include "../../../../globals/globals.h"

#include <stdio.h>

void Report3D__FPCce(char const *fmt, ...) {
  //char buf[2048];

  void **puEBP = NULL;
  __asm { mov puEBP, ebp };

  char fmt2[2048];
  snprintf(fmt2, sizeof(fmt2), __FUNCTION__ " from 0x%p: %s", puEBP[1], fmt);

  va_list args;
  va_start(args, fmt);
  vprintf(fmt2, args);
  va_end(args);

  //OutputDebugStringA(buf);
}

uint32_t GetDeltaTime__8LH3DTechFv(void) {
  return *globals()->g_delta_time__8LH3DTech;
}
