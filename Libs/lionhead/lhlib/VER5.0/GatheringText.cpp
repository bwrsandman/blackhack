#include "GatheringText.h"

#include <logger.h>

#include <globals.h>

float(__fastcall* pGetStringWidth__13GatheringTextFPwif)(
    struct GatheringText* this, const void* edx, const wchar_t* str, int len,
    float text_size) = 0x00831130;
float(__fastcall* pDrawText__13GatheringTextFPwfffffffffP9LH3DColoriii)(
    struct GatheringText* this, const void* edx, const wchar_t* text,
    float param_2, float param_3, float param_4, float param_5, float param_6,
    float param_7, float param_8, float param_9, float param_10,
    const struct LH3DColor* param_11, int param_12, int param_13,
    int param_14) = 0x008315b0;
void(
    __fastcall* pDrawTextRaw__13GatheringTextFPwiffffP9LH3DColoriP9LH3DColorff)(
    struct GatheringText* this, const void* edx, const wchar_t* text,
    int param_2, float x, float y, float param_5, float param_6,
    const struct LH3DColor* param_7, int param_8,
    const struct LH3DColor* param_9, float param_10,
    float param_11) = 0x00832c60;

struct GatheringText* GetGameFont__13GatheringTextFv(void) {
  return *globals()->GatheringText__gamefont;
}

float __fastcall GetStringWidth__13GatheringTextFPwif(
    struct GatheringText* this, const void* edx, const wchar_t* str, int len,
    float text_size) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, str=\"%S\", len=%d, text_size=%f) from %p", this,
      str, len, text_size, puEBP[1]);

  if (len < 1) {
    return 0.0f;
  }

  return (*pGetStringWidth__13GatheringTextFPwif)(
      this, edx, str, len, text_size);
}

float __fastcall DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
    struct GatheringText* this, const void* edx, const wchar_t* text,
    float param_2, float param_3, float param_4, float param_5, float param_6,
    float param_7, float param_8, float param_9, float param_10,
    const struct LH3DColor* p_color, int param_12, int param_13, int param_14) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, text=\"%S\", %f, %f, %f, %f, %f, %f, %f, %f, "
                         "%f, p_color=%p, %d, %d, %d) from %p",
      this, text, param_2, param_3, param_4, param_5, param_6, param_7, param_8,
      param_9, param_10, p_color, param_12, param_13, param_14, puEBP[1]);

  return (*pDrawText__13GatheringTextFPwfffffffffP9LH3DColoriii)(this, edx,
      text, param_2, param_3, param_4, param_5, param_6, param_7, param_8,
      param_9, param_10, p_color, param_12, param_13, param_14);
}

void __fastcall DrawTextRaw__13GatheringTextFPwiffffP9LH3DColoriP9LH3DColorff(
    struct GatheringText* this, const void* edx, const wchar_t* text,
    int param_2, float x, float y, float param_5, float param_6,
    struct LH3DColor* param_7, int param_8, struct LH3DColor* param_9,
    float param_10, float param_11) {

  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, text=\"%S\", %d, x=%f, y=%f, %f, %f, %p, %d, "
                         "%p, %f, %f) from %p",
      this, text, param_2, x, y, param_5, param_6, param_7, param_8, param_9,
      param_10, param_11, puEBP[1]);

  (*pDrawTextRaw__13GatheringTextFPwiffffP9LH3DColoriP9LH3DColorff)(this, edx,
      text, param_2, x, y, param_5, param_6, param_7, param_8, param_9,
      param_10, param_11);
}
