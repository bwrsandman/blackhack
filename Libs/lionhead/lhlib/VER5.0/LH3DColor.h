#ifndef BLACKHACK_LH3DCOLOR_H
#define BLACKHACK_LH3DCOLOR_H

#include <stdint.h>

struct LH3DColor {
  uint8_t b;
  uint8_t g;
  uint8_t r;
  uint8_t a;
};

// win1.41 inlined mac 10083c40 LH3DColor::LH3DColor(uint)
void ct__9LH3DColorFUl(struct LH3DColor *this, const void* edx, uint32_t color);
// win1.41 inlined mac 100530b0 LH3DColor::LH3DColor(LH3DColor const &)
void ct__9LH3DColorFRC9LH3DColor(struct LH3DColor *this, const void* edx, const struct LH3DColor *other);
// win1.41 inlined mac 1047dc00 LH3DColor::operator=(LH3DColor const &)
void as__9LH3DColorFRC9LH3DColor(struct LH3DColor *this, const void* edx, const struct LH3DColor *other);

#endif // BLACKHACK_LH3DCOLOR_H
