#include "LHScreen.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <logger.h>

#include <globals.h>

#include "LHSys.h"

void __stdcall TurnOffMenu() {
  void** puEBP = NULL;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(void) from 0x%p", puEBP[1]);
  SetMenu(*globals()->DAT_00e8c0f4, NULL);
}

void __fastcall SetFullscreenMode__8LHScreenFi(struct LHScreen* this, const void* edx, int mode) {
  void** puEBP = NULL;
  __asm { mov puEBP, ebp }
  LOG_TRACE(
      __FUNCTION__ "(mode=%d) from 0x%p", mode, puEBP[1]);
  this->fullscreen_mode = 1;

  if (mode) {
    // *(undefined4*)LHAssertIgnoreAllAsserts_exref = 1;
    // this->fullscreen_mode = 0;
    SetWindowLongA(Get__5LHSysFv()->screen.window, GWL_STYLE, WS_POPUP);
    SetWindowLongA(Get__5LHSysFv()->screen.window, GWL_EXSTYLE, WS_EX_TOPMOST);
    TurnOffMenu();
  }
  else {
    // this->fullscreen_mode = 1;
  }
}
