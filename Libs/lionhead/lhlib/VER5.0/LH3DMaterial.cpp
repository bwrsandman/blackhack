#include "LH3DMaterial.h"

struct LH3DMaterial* (__cdecl* pCreateMaterial__10LH3DRenderFQ212LH3DMaterial10RenderModeP11LH3DTexture)(
    enum LH3DMaterial__RenderMode render_mode, struct LH3DTexture* texture) = 0x0082fd30;

struct LH3DMaterial* __cdecl CreateMaterial__10LH3DRenderFQ212LH3DMaterial10RenderModeP11LH3DTexture(
enum LH3DMaterial__RenderMode render_mode, struct LH3DTexture* texture) {
  return (*pCreateMaterial__10LH3DRenderFQ212LH3DMaterial10RenderModeP11LH3DTexture)(render_mode, texture);
}