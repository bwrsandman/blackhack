#include "LH3DTexture.h"

#include <logger.h>

#include <globals.h>
#include "LH3DTech.h"

struct LH3DTexture* (__cdecl* pCreate__11LH3DTextureFPvUlUlP13TextureFormat)(void* param_1, unsigned long param_2, unsigned long param_3, struct TextureFormat* param_4) = 0x008379e0;

struct LH3DTexture* __cdecl Create__11LH3DTextureFPvUlUlP13TextureFormat(void* param_1, unsigned long param_2, unsigned long param_3, struct TextureFormat* param_4) {
  return (*pCreate__11LH3DTextureFPvUlUlP13TextureFormat)(
      param_1, param_2, param_3, param_4);
}

struct LH3DTexture* GetThisTexture__11LH3DTextureFUl(unsigned long id) {
  void** puEBP = NULL;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%lu) from 0x%p", id, puEBP[1]);

  struct LH3DTexture* result = NULL;
  for (struct LH3DTexture* i = *globals()->LH3DTexture.first; i != NULL;
       i = i->next) {
    if (i->id == id) {
      result = i;
      break;
    }
  }
  if (result == NULL) {
    Report3D__FPCce("#### GetThisTexture: can't found this texture %x\n", id);
    return *globals()->LH3DTexture.first;
  }
  if (result->id != id) {
    Report3D__FPCce(
        "#### GetThisTexture: can't found this texture %x bad id\n", id);
  }
  return result;
}
