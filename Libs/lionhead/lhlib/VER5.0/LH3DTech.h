#ifndef BLACKHACK_LH3DTECH_H
#define BLACKHACK_LH3DTECH_H

#include <stdint.h>

// win1.41 0081bbd0 mac 100be300 Report3D(char const *,...)
void Report3D__FPCce(char const* fmt, ...);

// win1.41 inlined mac 10093230 LH3DTech::GetDeltaTime(void)
uint32_t GetDeltaTime__8LH3DTechFv(void);

#endif // BLACKHACK_LH3DTECH_H
