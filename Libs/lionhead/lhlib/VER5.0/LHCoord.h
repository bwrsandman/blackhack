#ifndef BLACKHACK_LH_COORD_H
#define BLACKHACK_LH_COORD_H

struct LHCoord {
  int x;
  int y;
};

// win1.41 inlined mac 1006ebc0
void __fastcall ct__7LHCoordFRC7LHCoord(struct LHCoord * this, const void* edx, const struct LHCoord* other);

#endif // BLACKHACK_LH_COORD_H
