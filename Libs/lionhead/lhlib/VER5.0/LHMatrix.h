#ifndef BLACKHACK_LHMATRIX_H
#define BLACKHACK_LHMATRIX_H

#include "LHPoint.h"

struct LHMatrix {
  float m[12];
};

struct LHMatrix* IdentityMatrix__LHMatrix(struct LHMatrix* this);
struct LHMatrix* TranslationMatrix__LHMatrix(struct LHMatrix* this, struct LHPoint* translation);
// win1.41 007fb290 mac 1004f05c LHMatrix::SetInverse(LHMatrix const &)
void __fastcall SetInverse__8LHMatrixFRC8LHMatrix(
    struct LHMatrix* l, const void* edx, const struct LHMatrix* r);

#endif // BLACKHACK_LHMATRIX_H
