#include "LHSys.h"

#include <logger.h>

#include <globals.h>

void(__fastcall* pActivate__Q24slim5TbIMEFPv)(struct LHSys_field_0x70c4_t* this,
                                              const void* edx, HWND param_1) = 0x007f3d00;
void(__fastcall* pUnActivate__Q24slim5TbIMEFv)(
    struct LHSys_field_0x70c4_t* this) = 0x007f3d10;

void __fastcall Activate__Q24slim5TbIMEFPv(
    struct LHSys_field_0x70c4_t* this, const void* edx, HWND param_1) {
  LOG_TRACE(__FUNCTION__ "(%p, %p)", this, param_1);
  (*pActivate__Q24slim5TbIMEFPv)(this, edx, param_1);
}

void __fastcall UnActivate__Q24slim5TbIMEFv(struct LHSys_field_0x70c4_t* this) {
  LOG_TRACE(__FUNCTION__ "(%p)", this);
  (*pUnActivate__Q24slim5TbIMEFv)(this);
}

wchar_t* __fastcall Composition_Get__Q24slim5TbIMEFv(
    struct LHSys_field_0x70c4_t* this) {
  LOG_TRACE(__FUNCTION__ "(%p)", this);
  return this->field_0x0->field_0x8;
}

uint32_t __fastcall CandidateList_GetSelectIdx__Q24slim5TbIMEFv(struct LHSys_field_0x70c4_t *this)
{
  if (this->field_0x0->field_0x958) {
    return this->field_0x0->select_idx;
  }
  return 0;
}

void (__fastcall* pFUN_007f5d20)(
    struct LHSys_field_0x70c4_t_field_0x0_t *this, const void* edx, uint32_t param_1, uint32_t param_2) = 0x007f5d20;

void __fastcall FUN_007f5d20(
    struct LHSys_field_0x70c4_t_field_0x0_t *this, const void* edx, uint32_t param_1, uint32_t param_2)

{
  (*pFUN_007f5d20)(this, edx, param_1, param_2);
}

void __fastcall
CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi
    (struct LHSys_field_0x70c4_t *this, const void* edx, uint32_t param_1, uint32_t param_2, uint32_t idx)

{
  if ((!this->field_0x0->field_0x958 || this->field_0x0->field_0x93c != param_1) ||
      (idx != this->field_0x0->select_idx)) {
    FUN_007f5d20(this->field_0x0, edx, param_1, param_2);
  }
}

struct LHSys* __fastcall Get__5LHSysFv(void) {
  return globals()->LHSys;
}
