#ifndef BLACKHACK_LHPOINT_H
#define BLACKHACK_LHPOINT_H

struct LHPoint2D {
  float x;
  float y;
};

struct LHPoint {
  float x;
  float y;
  float z;
};

struct LHPoint2D* __fastcall __as__7Point2DFRC7Point2D(struct LHPoint2D* this, void* edx, const struct LHPoint2D* other);
struct LHPoint2D* __fastcall __pl__7Point2DCFRC7Point2D(struct LHPoint2D *this, void* edx, const struct LHPoint2D* lhs, const struct LHPoint2D* rhs);
struct LHPoint2D* __fastcall __mi__7Point2DCFRC7Point2D(struct LHPoint2D *this, void* edx, const struct LHPoint2D *lhs, const struct LHPoint2D *rhs);
// win1.41 0086fd00 Point2D::GetHeading(void) const
float __fastcall GetHeading__7Point2DCFv(const struct LHPoint2D* param_1);
// win1.41 0086fd70 Point2D::SetSize(float)
void __fastcall SetSize__7Point2DFf(struct LHPoint2D* this, void* edx, float size);
// win1.41 0086fda0 Point2D::GetSizeSq(void) const
float __fastcall GetSizeSq__7Point2DCFv(const struct LHPoint2D* this);
// win1.41 0086fdc0 Point2D::GetRange(Point2D const &) const
float __fastcall GetRange__7Point2DCFRC7Point2D(const struct LHPoint2D* this, void* edx, const struct LHPoint2D* param_1);

struct LHPoint* __fastcall __ct__7LHPointFv(struct LHPoint* this_);
struct LHPoint* __fastcall __ct__7LHPointFfff(struct LHPoint* this, float x, float y, float z);
struct LHPoint* __fastcall __ct__7LHPointFRC7LHPoint(struct LHPoint* this, struct LHPoint* other);
void __fastcall SetNull__7LHPointFv(struct LHPoint* this);
// win1.41 0054e910 LHPoint::FastNormalize(void)
void __fastcall FastNormalize__7LHPointFv(struct LHPoint *this);

#endif // BLACKHACK_LHPOINT_H
