#ifndef BLACKHACK_LH3DMATERIAL_H
#define BLACKHACK_LH3DMATERIAL_H

#include <stdbool.h>

#include "LH3DColor.h"
#include "LH3DTexture.h"

enum LH3DMaterial__RenderMode {
  RENDER_MODE_0x6 = 0x6,
  RENDER_MODE_0x9 = 0x9,
};

struct LH3DMaterial {
  enum LH3DMaterial__RenderMode render_mode;
  uint8_t field_0x4;
  uint8_t cull_mode;
  struct LH3DTexture* texture;
  struct LH3DColor color;
};

struct LH3DMaterial * __cdecl
CreateMaterial__10LH3DRenderFQ212LH3DMaterial10RenderModeP11LH3DTexture
(enum LH3DMaterial__RenderMode render_mode, struct LH3DTexture *texture);

#endif // BLACKHACK_LH3DMATERIAL_H
