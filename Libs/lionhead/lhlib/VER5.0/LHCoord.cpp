#include "LHCoord.h"

void __fastcall ct__7LHCoordFRC7LHCoord(struct LHCoord * this, const void* edx, const struct LHCoord* other) {
  this->x = other->x;
  this->y = other->y;
}
