#include "LHMatrix.h"
#include <globals.h>

#include <math.h>

struct LHMatrix* IdentityMatrix__LHMatrix(struct LHMatrix* this) {
  this->m[0] = 1.0f;
  this->m[1] = 0.0f;
  this->m[2] = 0.0f;

  this->m[3] = 0.0f;
  this->m[4] = 1.0f;
  this->m[5] = 0.0f;

  this->m[6] = 0.0f;
  this->m[7] = 0.0f;
  this->m[8] = 1.0f;

  this->m[9] = 0.0f;
  this->m[10] = 0.0f;
  this->m[11] = 0.0f;

  return this;
}

struct LHMatrix* TranslationMatrix__LHMatrix(
    struct LHMatrix* this, struct LHPoint* translation) {
  this->m[0] = 1.0f;
  this->m[1] = 0.0f;
  this->m[2] = 0.0f;

  this->m[3] = 0.0f;
  this->m[4] = 1.0f;
  this->m[5] = 0.0f;

  this->m[6] = 0.0f;
  this->m[7] = 0.0f;
  this->m[8] = 1.0f;

  this->m[9] = translation->x;
  this->m[10] = translation->y;
  this->m[11] = translation->z;

  return this;
}

void __fastcall SetInverse__8LHMatrixFRC8LHMatrix(
    struct LHMatrix* l, const void* edx, const struct LHMatrix* r) {
  float fVar3 = (r->m[8] * r->m[4] - r->m[7] * r->m[5]) * r->m[0] +
                (r->m[2] * r->m[7] - r->m[8] * r->m[1]) * r->m[3] +
                (r->m[5] * r->m[1] - r->m[2] * r->m[4]) * r->m[6];

  float fVar2;
  if (fabsf(fVar3) < *globals()->DAT_00c371d4) {
    fVar2 = *globals()->DAT_00c371d4;
    if (fVar3 < 0.0f) {
      fVar2 *= -1.0f;
    }
  }
  else {
    fVar2 = fVar3;
  }

  l->m[0] = (r->m[8] * r->m[4] - r->m[7] * r->m[5]) / fVar2;
  l->m[1] = (r->m[2] * r->m[7] - r->m[8] * r->m[1]) / fVar2;
  l->m[2] = (r->m[5] * r->m[1] - r->m[2] * r->m[4]) / fVar2;
  l->m[3] = (r->m[6] * r->m[5] - r->m[8] * r->m[3]) / fVar2;
  l->m[4] = (r->m[8] * r->m[0] - r->m[6] * r->m[2]) / fVar2;
  l->m[5] = (r->m[2] * r->m[3] - r->m[0] * r->m[5]) / fVar2;
  l->m[6] = (r->m[7] * r->m[3] - r->m[6] * r->m[4]) / fVar2;
  l->m[7] = (r->m[6] * r->m[1] - r->m[0] * r->m[7]) / fVar2;
  l->m[8] = (r->m[0] * r->m[4] - r->m[3] * r->m[1]) / fVar2;
  l->m[9] = -(r->m[9] * l->m[0] + r->m[11] * l->m[6] + r->m[10] * l->m[3]);
  l->m[10] = -(l->m[4] * r->m[10] + r->m[9] * l->m[1] + l->m[7] * r->m[11]);
  l->m[11] = -(l->m[5] * r->m[10] + l->m[8] * r->m[11] + r->m[9] * l->m[2]);
}
