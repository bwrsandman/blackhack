#ifndef BLACKHACK_LHSCREEN_H
#define BLACKHACK_LHSCREEN_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <ddraw.h>

#include "LHRegion.h"

struct LHScreen {
  uint32_t field_0x0;
  uint32_t field_0x4;
  uint16_t field_0x8;
  uint16_t field_0xa;
  uint32_t field_0xc;
  uint32_t field_0x10;
  uint8_t field_0x14;
  uint32_t field_0x18;
  uint32_t field_0x1c;
  uint32_t field_0x20;
  uint32_t field_0x24;
  HWND window;
  IDirectDraw7* ddraw;
  uint32_t field_0x30;
  IDirectDrawSurface7 * surface;
  uint8_t field_0x38[0x10];
  struct LHRegion region;
  POINT client_to_screen;
  IDirectDrawClipper* clipper;
  bool fullscreen_mode;
  uint8_t field_0x65[22];
  uint8_t field_0x7b;
  uint8_t field_0x7c;
  uint8_t field_0x7d;
  uint8_t field_0x7e;
  uint8_t field_0x7f;
  uint8_t field_0x80;
  uint8_t field_0x81[307];
};
static_assert(sizeof(struct LHScreen) == 0x1b4, "struct size is wrong");

// win1.41 007db8e0 mac 1015eae0
void __stdcall TurnOffMenu(void);
// win1.41 007dd0d0 mac 1014e420 LHScreen::SetFullscreenMode(int)
void __fastcall SetFullscreenMode__8LHScreenFi(struct LHScreen* this, const void* edx, int mode);

#endif //BLACKHACK_LHSCREEN_H
