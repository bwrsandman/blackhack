#ifndef BLACKHACK_MEMORY_H
#define BLACKHACK_MEMORY_H

#include <stddef.h>
#include <stdint.h>

// win1.41 007db790 mac 106203fc operator new(unsigned long)
void* __cdecl __nw__FUl(size_t size, const char* file_name, uint32_t line);
// win1.41 007aee98 mac 1061fbd4 operator delete(void *)
void __cdecl __dl__FPv(void* address);

#endif // BLACKHACK_MEMORY_H
