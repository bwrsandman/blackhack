#include "LH3DColor.h"

#include <logger.h>

void ct__9LH3DColorFUl(
    struct LH3DColor* this, const void* edx, uint32_t color) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, 0x%08X) from 0x%p", this, color, puEBP[1]);

  *this = *(struct LH3DColor*)&color;
}

void ct__9LH3DColorFRC9LH3DColor(struct LH3DColor *this, const void* edx, const struct LH3DColor *other) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, 0x%08X) from 0x%p", this, *(uint32_t*)&other,
            puEBP[1]);
  *this = *other;
}

void as__9LH3DColorFRC9LH3DColor(
    struct LH3DColor* this, const void* edx, const struct LH3DColor* other) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, 0x%08X) from 0x%p", this, *(uint32_t*)&other,
      puEBP[1]);
  *this = *other;
}
