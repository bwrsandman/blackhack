#ifndef BLACKHACK_LHSYS_H
#define BLACKHACK_LHSYS_H

#include <stdint.h>

#include <assert.h>

#include "LHScreen.h"
#include "LHMouse.h"

struct LHSys_field_0x70c4_t_field_0x0_t {
  uint32_t field_0x0;
  uint32_t field_0x4;
  wchar_t field_0x8[0x100];
  uint8_t field_0x208[0x618];
  HWND field_0x820;
  uint8_t field_0x824;
  HIMC field_0x828;
  uint32_t field_0x82c;
  uint32_t field_0x830;
  uint8_t field_0x834[0x100];
  uint32_t field_0x934;
  uint32_t field_0x938;
  uint32_t field_0x93c;
  uint32_t select_idx;
  uint8_t field_0x944;
  uint32_t field_0x948;
  uint32_t field_0x94c;
  uint32_t field_0x950;
  uint32_t field_0x954;
  bool field_0x958;
  uint32_t field_0x95c;
};
static_assert(sizeof(struct LHSys_field_0x70c4_t_field_0x0_t) == 0x960, "struct size is wrong");

struct LHSys_field_0x70c4_t {
  struct LHSys_field_0x70c4_t_field_0x0_t* field_0x0;
};
static_assert(sizeof(struct LHSys_field_0x70c4_t) == 0x4, "struct size is wrong");

// win1.41 007f3d00 mac 101704a0 slim::TbIME::Activate(void *)
void __fastcall Activate__Q24slim5TbIMEFPv(struct LHSys_field_0x70c4_t* this, const void* edx, HWND param_1);
// win1.41 007f3d10 mac 10170460 slim::TbIME::UnActivate(void)
void __fastcall UnActivate__Q24slim5TbIMEFv(struct LHSys_field_0x70c4_t* this);
// win1.41 007f3d50 mac 1061ec14
wchar_t* __fastcall Composition_Get__Q24slim5TbIMEFv(struct LHSys_field_0x70c4_t* this);
// win1.41 007f3dc0 mac 1061e884
uint32_t __fastcall CandidateList_GetSelectIdx__Q24slim5TbIMEFv(struct LHSys_field_0x70c4_t *this);
// win1.41 007f3de0 mac 1061e89c
void __fastcall CandidateList_SetViewWindow__Q24slim5TbIMEFUiUiUi(
    struct LHSys_field_0x70c4_t *this, const void* edx, uint32_t param_1, uint32_t param_2, uint32_t idx);

struct LHSystem {
  HINSTANCE instance;
  uint32_t field_0x4;
  uint32_t field_0x8;
  int terminating;
};
static_assert(sizeof(struct LHSystem) == 0x10, "struct size is wrong");

struct LHSpriteList {
  uint32_t field_0x0;
  uint16_t* field_0x4;
  int field_0x8;
};
static_assert(sizeof(struct LHSpriteList) == 0xc, "struct size is wrong");

struct LHText {
  struct LHSpriteList sprite_list;
  uint16_t field_0xc;
  uint8_t field_0xe;
  uint32_t field_0x10;
  uint32_t field_0x14;
  uint32_t field_0x18;
  int field_0x1c;
  uint8_t field_0x20;
};
static_assert(sizeof(struct LHText) == 0x24, "struct size is wrong");

struct LHSys {
  struct LHSystem system;
  struct LHScreen screen;
  struct LHMouse mouse;
  uint8_t field_0x2ac[0x6d98];
  struct LHText text;
  uint8_t field_0x7068[0x4c];
  HWND field_0x70b4;
  uint32_t field_0x70b8;
  uint32_t field_0x70bc;
  uint32_t field_0x70c0;
  struct LHSys_field_0x70c4_t * field_0x70c4;
  uint8_t field_0x70c8[0x10];
};
static_assert(sizeof(struct LHSys) == 0x70d8, "struct size is wrong");

// Inlined LHSys::Get(void)
struct LHSys* __fastcall Get__5LHSysFv(void);

#endif // BLACKHACK_LHSYS_H
