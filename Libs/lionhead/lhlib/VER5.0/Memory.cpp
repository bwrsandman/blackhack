#include "Memory.h"
#include <logger.h>

void* (__cdecl* p__nw__FUl)(size_t size, const char* file_name, uint32_t line) = 0x007db790;
void (__cdecl* p__dl__FPv)(void* param_1) = 0x007aee98;

void* __cdecl __nw__FUl(size_t size, const char* file_name, uint32_t line) {
  LOG_TRACE("operator new(size=0x%zx) from %s:%u", size, file_name, line);
  return (*p__nw__FUl)(size, file_name, line);
}

void __cdecl __dl__FPv(void* address) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE("operator delete(address=0x%p) from %p", address, puEBP[1]);
  (*p__dl__FPv)(address);
}
