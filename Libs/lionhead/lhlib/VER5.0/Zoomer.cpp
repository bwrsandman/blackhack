#include "Zoomer.h"

#include <logger.h>

#include "LHMatrix.h"

struct Zoomer* __fastcall ct__6ZoomerFv(struct Zoomer* this) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p) from %p", this, puEBP[1]);
}


void __fastcall SetDestinationWithSpeedAndTime__6ZoomerFfff(struct Zoomer* this,
    const void* edx, float destination, float speed, float time) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(%p, destination=%f, speed=%f, time=%f) from %p",
      this, destination, speed, time, puEBP[1]);

  if (time < 0.001f) {
    this->destination = destination;
    this->current_value = destination;
    this->field_0x1c = destination;
    this->time = 0.0f;
    this->time_m1 = 0.0f;
    this->field_0x24.x = 0.0f;
    this->field_0x24.y = 0.0f;
    this->field_0x24.z = 0.0f;
    this->time_m2 = 0.0f;
    this->speed_m1 = 0.0f;
    this->field_0x20 = 0.0f;
    this->speed = 0.0f;
  }
  else {
    this->field_0x20 = this->speed_m1;
    this->field_0x1c = this->current_value;
    this->destination = destination;
    this->speed = speed;
    this->time = time;
    this->time_m1 = 0.0f;
    float iVar1 = this->time * this->time * 0.5f;

    struct LHMatrix local_60;
    local_60.m[0] = iVar1 * iVar1 / 6.0f;
    local_60.m[1] = iVar1 * this->time / 3.0f;
    local_60.m[2] = iVar1;
    local_60.m[3] = iVar1 * this->time / 3.0f;
    local_60.m[4] = iVar1;
    local_60.m[5] = this->time;
    local_60.m[6] = iVar1;
    local_60.m[7] = this->time;
    local_60.m[8] = 1.0f;
    local_60.m[9] = 0.0f;
    local_60.m[10] = 0.0f;
    local_60.m[11] = 0.0f;

    struct LHMatrix inv;
    SetInverse__8LHMatrixFRC8LHMatrix(&inv, NULL, &local_60);

    float fVar1 =
        this->destination - this->field_0x1c - this->time * this->field_0x20;
    float fVar2 = this->speed - this->field_0x20;
    this->field_0x24.x = inv.m[2] * fVar1 + inv.m[5] * fVar2 + inv.m[11];
    this->field_0x24.y = inv.m[4] * fVar2 + inv.m[1] * fVar1 + inv.m[10];
    this->field_0x24.z = inv.m[0] * fVar1 + inv.m[3] * fVar2 + inv.m[9];
  }
}
