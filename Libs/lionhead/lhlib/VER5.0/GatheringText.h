#ifndef BLACKHACK_GATHERINGTEXT_H
#define BLACKHACK_GATHERINGTEXT_H

#include <wchar.h>

struct GatheringText;

// Inlined GatheringText::Get00eccd08(void)
struct GatheringText* GetGameFont__13GatheringTextFv(void);

// win1.41 00831130 mac 0033360 GatheringText::GetStringWidth(wchar_t *, int, float)
float __fastcall GetStringWidth__13GatheringTextFPwif(
    struct GatheringText* this, const void* edx, const wchar_t* str, int len,
    float text_size);

// win1.41 008315b0 mac 1061ca0c GatheringText::DrawText(wchar_t *, float, float, float, float, float, float, float, float, float, LH3DColor *, int, int, int)
float __fastcall DrawText__13GatheringTextFPwfffffffffP9LH3DColoriii(
    struct GatheringText* this, const void* edx, const wchar_t* text, float param_2,
    float param_3, float param_4, float param_5, float param_6, float param_7,
    float param_8, float param_9, float param_10, const struct LH3DColor* p_color,
    int param_12, int param_13, int param_14);

// win1.41 00832c60 mac 10be2db8 GatheringText::DrawTextRaw(wchar_t *, int, float, float, float, float, LH3DColor *, int, LH3DColor *, float, float)
void __fastcall DrawTextRaw__13GatheringTextFPwiffffP9LH3DColoriP9LH3DColorff(
    struct GatheringText *this, const void* edx, const wchar_t *text,
    int param_2, float x, float y, float param_5, float param_6,
    struct LH3DColor *param_7, int param_8, struct LH3DColor *param_9,
    float param_10, float param_11);

#endif // BLACKHACK_GATHERINGTEXT_H
