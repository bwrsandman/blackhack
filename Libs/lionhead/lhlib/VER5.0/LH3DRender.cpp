#include "LH3DRender.h"

#include <assert.h>
#include <logger.h>
#include <math.h>

#include <globals.h>

static const char* D3DRENDERSTATETYPE_strs[] = {
  [1] = "D3DRENDERSTATE_TEXTUREHANDLE",
  [2] = "D3DRENDERSTATE_ANTIALIAS",
  [3] = "D3DRENDERSTATE_TEXTUREADDRESS",
  [4] = "D3DRENDERSTATE_TEXTUREPERSPECTIVE",
  [5] = "D3DRENDERSTATE_WRAPU",
  [6] = "D3DRENDERSTATE_WRAPV",
  [7] = "D3DRENDERSTATE_ZENABLE",
  [8] = "D3DRENDERSTATE_FILLMODE",
  [9] = "D3DRENDERSTATE_SHADEMODE",
  [10] = "D3DRENDERSTATE_LINEPATTERN",
  [11] = "D3DRENDERSTATE_MONOENABLE",
  [12] = "D3DRENDERSTATE_ROP2",
  [13] = "D3DRENDERSTATE_PLANEMASK",
  [14] = "D3DRENDERSTATE_ZWRITEENABLE",
  [15] = "D3DRENDERSTATE_ALPHATESTENABLE",
  [16] = "D3DRENDERSTATE_LASTPIXEL",
  [17] = "D3DRENDERSTATE_TEXTUREMAG",
  [18] = "D3DRENDERSTATE_TEXTUREMIN",
  [19] = "D3DRENDERSTATE_SRCBLEND",
  [20] = "D3DRENDERSTATE_DESTBLEND",
  [21] = "D3DRENDERSTATE_TEXTUREMAPBLEND",
  [22] = "D3DRENDERSTATE_CULLMODE",
  [23] = "D3DRENDERSTATE_ZFUNC",
  [24] = "D3DRENDERSTATE_ALPHAREF",
  [25] = "D3DRENDERSTATE_ALPHAFUNC",
  [26] = "D3DRENDERSTATE_DITHERENABLE",
  [27] = "D3DRENDERSTATE_ALPHABLENDENABLE",
  [28] = "D3DRENDERSTATE_FOGENABLE",
  [29] = "D3DRENDERSTATE_SPECULARENABLE",
  [30] = "D3DRENDERSTATE_ZVISIBLE",
  [31] = "D3DRENDERSTATE_SUBPIXEL",
  [32] = "D3DRENDERSTATE_SUBPIXELX",
  [33] = "D3DRENDERSTATE_STIPPLEDALPHA",
  [34] = "D3DRENDERSTATE_FOGCOLOR",
  [35] = "D3DRENDERSTATE_FOGTABLEMODE",
  [36] = "D3DRENDERSTATE_FOGTABLESTART",
  [37] = "D3DRENDERSTATE_FOGTABLEEND",
  [38] = "D3DRENDERSTATE_FOGTABLEDENSITY",
  [36] = "D3DRENDERSTATE_FOGSTART",
  [37] = "D3DRENDERSTATE_FOGEND",
  [38] = "D3DRENDERSTATE_FOGDENSITY",
  [39] = "D3DRENDERSTATE_STIPPLEENABLE",
  [40] = "D3DRENDERSTATE_EDGEANTIALIAS",
  [41] = "D3DRENDERSTATE_COLORKEYENABLE",
  [43] = "D3DRENDERSTATE_BORDERCOLOR",
  [44] = "D3DRENDERSTATE_TEXTUREADDRESSU",
  [45] = "D3DRENDERSTATE_TEXTUREADDRESSV",
  [46] = "D3DRENDERSTATE_MIPMAPLODBIAS",
  [47] = "D3DRENDERSTATE_ZBIAS",
  [48] = "D3DRENDERSTATE_RANGEFOGENABLE",
  [49] = "D3DRENDERSTATE_ANISOTROPY",
  [50] = "D3DRENDERSTATE_FLUSHBATCH",
  [51] = "D3DRENDERSTATE_TRANSLUCENTSORTINDEPENDENT",
  [52] = "D3DRENDERSTATE_STENCILENABLE",
  [53] = "D3DRENDERSTATE_STENCILFAIL",
  [54] = "D3DRENDERSTATE_STENCILZFAIL",
  [55] = "D3DRENDERSTATE_STENCILPASS",
  [56] = "D3DRENDERSTATE_STENCILFUNC",
  [57] = "D3DRENDERSTATE_STENCILREF",
  [58] = "D3DRENDERSTATE_STENCILMASK",
  [59] = "D3DRENDERSTATE_STENCILWRITEMASK",
  [60] = "D3DRENDERSTATE_TEXTUREFACTOR",
  [64] = "D3DRENDERSTATE_STIPPLEPATTERN00",
  [65] = "D3DRENDERSTATE_STIPPLEPATTERN01",
  [66] = "D3DRENDERSTATE_STIPPLEPATTERN02",
  [67] = "D3DRENDERSTATE_STIPPLEPATTERN03",
  [68] = "D3DRENDERSTATE_STIPPLEPATTERN04",
  [69] = "D3DRENDERSTATE_STIPPLEPATTERN05",
  [70] = "D3DRENDERSTATE_STIPPLEPATTERN06",
  [71] = "D3DRENDERSTATE_STIPPLEPATTERN07",
  [72] = "D3DRENDERSTATE_STIPPLEPATTERN08",
  [73] = "D3DRENDERSTATE_STIPPLEPATTERN09",
  [74] = "D3DRENDERSTATE_STIPPLEPATTERN10",
  [75] = "D3DRENDERSTATE_STIPPLEPATTERN11",
  [76] = "D3DRENDERSTATE_STIPPLEPATTERN12",
  [77] = "D3DRENDERSTATE_STIPPLEPATTERN13",
  [78] = "D3DRENDERSTATE_STIPPLEPATTERN14",
  [79] = "D3DRENDERSTATE_STIPPLEPATTERN15",
  [80] = "D3DRENDERSTATE_STIPPLEPATTERN16",
  [81] = "D3DRENDERSTATE_STIPPLEPATTERN17",
  [82] = "D3DRENDERSTATE_STIPPLEPATTERN18",
  [83] = "D3DRENDERSTATE_STIPPLEPATTERN19",
  [84] = "D3DRENDERSTATE_STIPPLEPATTERN20",
  [85] = "D3DRENDERSTATE_STIPPLEPATTERN21",
  [86] = "D3DRENDERSTATE_STIPPLEPATTERN22",
  [87] = "D3DRENDERSTATE_STIPPLEPATTERN23",
  [88] = "D3DRENDERSTATE_STIPPLEPATTERN24",
  [89] = "D3DRENDERSTATE_STIPPLEPATTERN25",
  [90] = "D3DRENDERSTATE_STIPPLEPATTERN26",
  [91] = "D3DRENDERSTATE_STIPPLEPATTERN27",
  [92] = "D3DRENDERSTATE_STIPPLEPATTERN28",
  [93] = "D3DRENDERSTATE_STIPPLEPATTERN29",
  [94] = "D3DRENDERSTATE_STIPPLEPATTERN30",
  [95] = "D3DRENDERSTATE_STIPPLEPATTERN31",
  [128] = "D3DRENDERSTATE_WRAP0",
  [129] = "D3DRENDERSTATE_WRAP1",
  [130] = "D3DRENDERSTATE_WRAP2",
  [131] = "D3DRENDERSTATE_WRAP3",
  [132] = "D3DRENDERSTATE_WRAP4",
  [133] = "D3DRENDERSTATE_WRAP5",
  [134] = "D3DRENDERSTATE_WRAP6",
  [135] = "D3DRENDERSTATE_WRAP7",
  [136] = "D3DRENDERSTATE_CLIPPING",
  [137] = "D3DRENDERSTATE_LIGHTING",
  [138] = "D3DRENDERSTATE_EXTENTS",
  [139] = "D3DRENDERSTATE_AMBIENT",
  [140] = "D3DRENDERSTATE_FOGVERTEXMODE",
  [141] = "D3DRENDERSTATE_COLORVERTEX",
  [142] = "D3DRENDERSTATE_LOCALVIEWER",
  [143] = "D3DRENDERSTATE_NORMALIZENORMALS",
  [144] = "D3DRENDERSTATE_COLORKEYBLENDENABLE",
  [145] = "D3DRENDERSTATE_DIFFUSEMATERIALSOURCE",
  [146] = "D3DRENDERSTATE_SPECULARMATERIALSOURCE",
  [147] = "D3DRENDERSTATE_AMBIENTMATERIALSOURCE",
  [148] = "D3DRENDERSTATE_EMISSIVEMATERIALSOURCE",
  [151] = "D3DRENDERSTATE_VERTEXBLEND",
  [152] = "D3DRENDERSTATE_CLIPPLANEENABLE",
};

static const char* D3DTEXTURESTAGESTATETYPE_strs[] = {
  [1] = "D3DTSS_COLOROP",
  [2] = "D3DTSS_COLORARG1",
  [3] = "D3DTSS_COLORARG2",
  [4] = "D3DTSS_ALPHAOP",
  [5] = "D3DTSS_ALPHAARG1",
  [6] = "D3DTSS_ALPHAARG2",
  [7] = "D3DTSS_BUMPENVMAT00",
  [8] = "D3DTSS_BUMPENVMAT01",
  [9] = "D3DTSS_BUMPENVMAT10",
  [10] = "D3DTSS_BUMPENVMAT11",
  [11] = "D3DTSS_TEXCOORDINDEX",
  [12] = "D3DTSS_ADDRESS",
  [13] = "D3DTSS_ADDRESSU",
  [14] = "D3DTSS_ADDRESSV",
  [15] = "D3DTSS_BORDERCOLOR",
  [16] = "D3DTSS_MAGFILTER",
  [17] = "D3DTSS_MINFILTER",
  [18] = "D3DTSS_MIPFILTER",
  [19] = "D3DTSS_MIPMAPLODBIAS",
  [20] = "D3DTSS_MAXMIPLEVEL",
  [21] = "D3DTSS_MAXANISOTROPY",
  [22] = "D3DTSS_BUMPENVLSCALE",
  [23] = "D3DTSS_BUMPENVLOFFSET",
  [24] = "D3DTSS_TEXTURETRANSFORMFLAGS",
};

static const char* D3DPRIMITIVETYPE_strs[] = {
  [1] = "D3DPT_POINTLIST",
  [2] = "D3DPT_LINELIST",
  [3] = "D3DPT_LINESTRIP",
  [4] = "D3DPT_TRIANGLELIST",
  [5] = "D3DPT_TRIANGLESTRIP",
  [6] = "D3DPT_TRIANGLEFAN",
};

void(__cdecl* pRenderLoadingFrame__Fb)(bool param_1) = 0x005f4e40;

void __cdecl RenderLoadingFrame__Fb(bool param_1) {
  (*pRenderLoadingFrame__Fb)(param_1);
}

int __cdecl SetRenderState__10LH3DRenderF18D3DRENDERSTATETYPEUl(
    D3DRENDERSTATETYPE type, uint32_t value) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  const char* type_str = D3DRENDERSTATETYPE_strs[type];
  if (type_str == NULL) {
    assert(false);
    type_str = "***BAD TYPE***";
  }

  LOG_TRACE(__FUNCTION__ "(type=%s, value=0x%08X) from %p", type_str, value,
      puEBP[1]);

  HRESULT result = S_OK;

  if ((*globals()->UINT_ARRAY_00eca1f0)[type] != value) {
    result =
        (*globals()->Direct3DDevice7)
            ->lpVtbl->SetRenderState(*globals()->Direct3DDevice7, type, value);
    if (result != S_OK) {
      value = 0xffffffff;
    }
    (*globals()->UINT_ARRAY_00eca1f0)[type] = value;
  }
  return result;
}

int __cdecl GetRenderState__10LH3DRenderF18D3DRENDERSTATETYPERUl(
    D3DRENDERSTATETYPE type, uint32_t* value) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  const char* type_str = D3DRENDERSTATETYPE_strs[type];
  if (type_str == NULL) {
    assert(false);
    type_str = "***BAD TYPE***";
  }

  LOG_TRACE(
      __FUNCTION__ "(type=%s, value=0x%p) from %p", type_str, value, puEBP[1]);

  HRESULT result = S_OK;

  if ((*globals()->UINT_ARRAY_00eca1f0)[type] == 0xffffffff) {
    result = (*globals()->Direct3DDevice7)
                 ->lpVtbl->GetRenderState(*globals()->Direct3DDevice7,
                     D3DRENDERSTATE_ZWRITEENABLE, (LPDWORD)value);
    if (result == S_OK) {
      (*globals()->UINT_ARRAY_00eca1f0)[type] = *value;
    }
    else {
      (*globals()->UINT_ARRAY_00eca1f0)[type] = 0xffffffff;
    }
  }
  else {
    *value = (*globals()->UINT_ARRAY_00eca1f0)[type];
  }
  return result;
}

void __cdecl DrawAndClip__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl(
    D3DPRIMITIVETYPE primitive_type, uint32_t fvf, D3DTLVERTEX* vertices,
    uint32_t vertex_count, uint16_t* indices, uint32_t index_count) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  const char* primitive_type_str = D3DPRIMITIVETYPE_strs[primitive_type];
  if (primitive_type_str == NULL) {
    assert(false);
    primitive_type_str = "***BAD TYPE***";
  }

  LOG_TRACE(
      __FUNCTION__ "(primitive_type=%s, fvf=%u, vertices=0x%p, "
                   "vertex_count=%u, indices=0x%p, index_count=%u) from %p",
      primitive_type_str, fvf, vertices, vertex_count, indices, index_count,
      puEBP[1]);

  for (uint32_t i = 0; i < vertex_count; ++i) {
    if (vertices[i].sx < 0.0f) {
      vertices[i].sx = 0.0f;
    }
    else if (vertices[i].sx > globals()->DrawAndClip_00c2ab00->clip_x_max) {
      vertices[i].sx = globals()->DrawAndClip_00c2ab00->clip_x_max;
    }

    if (vertices[i].sy < 0.0f) {
      vertices[i].sy = 0.0f;
    }
    else if (vertices[i].sy > globals()->DrawAndClip_00c2ab00->clip_y_max) {
      vertices[i].sy = globals()->DrawAndClip_00c2ab00->clip_y_max;
    }
  }

  (*globals()->Direct3DDevice7)
      ->lpVtbl->DrawIndexedPrimitive(*globals()->Direct3DDevice7, primitive_type,
          fvf, vertices, vertex_count, indices, index_count, 0);
}

void __cdecl DrawAndClip2D__F16D3DPRIMITIVETYPEUlP8Vertex3DUlPUsUl(
    D3DPRIMITIVETYPE primitive_type, uint32_t fvf, D3DTLVERTEX* vertices,
    uint32_t vertex_count, uint16_t* indices, uint32_t index_count) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  const char* primitive_type_str = D3DPRIMITIVETYPE_strs[primitive_type];
  if (primitive_type_str == NULL) {
    assert(false);
    primitive_type_str = "***BAD TYPE***";
  }

  LOG_TRACE(
      __FUNCTION__ "(primitive_type=%s, fvf=%u, vertices=0x%p, "
                   "vertex_count=%u, indices=0x%p, index_count=%u) from %p",
      primitive_type_str, fvf, vertices, vertex_count, indices, index_count,
      puEBP[1]);

  uint32_t uVar5 = 0xffffffff;

  for (uint32_t i = 0; i < vertex_count; ++i) {
    if (vertices[i].sx < 0.0f || vertices[i].sy < 0.0f ||
        vertices[i].sx > globals()->DrawAndClip_00c2ab00->clip_x_max ||
        vertices[i].sy > globals()->DrawAndClip_00c2ab00->clip_y_max) {
      uint32_t uVar3 = i & 0xfffffffc;

      float du;
      float dv;
      float local_1c;
      float local_20;
      float local_24;
      float local_28;
      float local_2c;
      float local_30;
      if (uVar5 != uVar3 && (int)uVar3 < (int)(vertex_count - 2)) {
        float dx_0 = vertices[uVar3].sx - vertices[uVar3 + 1].sx;
        float dy_0 = vertices[uVar3].sy - vertices[uVar3 + 1].sy;
        float du_0 = vertices[uVar3].tu - vertices[uVar3 + 1].tu;
        float dv_0 = vertices[uVar3].tv - vertices[uVar3 + 1].tv;

        float dx_1 = vertices[uVar3 + 2].sx - vertices[uVar3 + 1].sx;
        float dy_1 = vertices[uVar3 + 2].sy - vertices[uVar3 + 1].sy;
        float du_1 = vertices[uVar3 + 2].tu - vertices[uVar3 + 1].tu;
        float dv_1 = vertices[uVar3 + 2].tv - vertices[uVar3 + 1].tv;

        local_30 = dy_0 * du_1 - dy_1 * du_0;
        local_2c = dx_1 * du_0 - dx_0 * du_1;
        local_28 = dx_0 * dy_1 - dx_1 * dy_0;
        du = local_30 * vertices[uVar3].sx + local_2c * vertices[uVar3].sy +
             local_28 * vertices[uVar3].tu;

        local_24 = dy_0 * dv_1 - dy_1 * dv_0;
        local_20 = dx_1 * dv_0 - dx_0 * dv_1;
        local_1c = dx_0 * dy_1 - dx_1 * dy_0;
        dv = local_24 * vertices[uVar3].sx + local_20 * vertices[uVar3].sy +
             local_1c * vertices[uVar3].tv;

        uVar5 = uVar3;
      }

      if (vertices[i].sx < 0.0f) {
        vertices[i].sx = 0.0f;
      }
      else if (vertices[i].sx > globals()->DrawAndClip_00c2ab00->clip_x_max) {
        vertices[i].sx = globals()->DrawAndClip_00c2ab00->clip_x_max;
      }

      if (vertices[i].sy < 0.0f) {
        vertices[i].sy = 0.0f;
      }
      else if (vertices[i].sy > globals()->DrawAndClip_00c2ab00->clip_y_max) {
        vertices[i].sy = globals()->DrawAndClip_00c2ab00->clip_y_max;
      }

      if (fabsf(local_28) > 0.00001f) {
        vertices[i].tu =
            ((du - local_30 * vertices[i].sx) - local_2c * vertices[i].sy) /
            local_28;
      }

      if (fabsf(local_1c) > 0.00001f) {
        vertices[i].tv =
            ((dv - local_24 * vertices[i].sx) - local_20 * vertices[i].sy) /
            local_1c;
      }
    }
  }

  (*globals()->Direct3DDevice7)
      ->lpVtbl->DrawIndexedPrimitive(*globals()->Direct3DDevice7, primitive_type,
          fvf, vertices, vertex_count, indices, index_count, 0);
}

int __cdecl SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
    uint32_t index, D3DTEXTURESTAGESTATETYPE type, uint32_t value) {
  void** puEBP;
  __asm { mov puEBP, ebp }

  const char* type_str = D3DTEXTURESTAGESTATETYPE_strs[type];
  if (type_str == NULL) {
    assert(false);
    type_str = "***BAD TYPE***";
  }

  LOG_TRACE(__FUNCTION__ "(index=%u, type=%s, value=%u) from %p", index,
      type_str, value, puEBP[1]);

  int iVar1;

  if ((*globals()->UINT_ARRAY_00ec81f0)[index * 0x100 + type] != value) {
    iVar1 = (*globals()->Direct3DDevice7)
                ->lpVtbl->SetTextureStageState(
                    *globals()->Direct3DDevice7, index, type, value);
    if (iVar1 != 0) {
      value = 0xffffffff;
    }
    (*globals()->UINT_ARRAY_00ec81f0)[index * 0x100 + type] = value;
    return iVar1;
  }
  return 0;
}

void __cdecl SetD3DTillingOn__10LH3DRenderFi(uint32_t index) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(index=%u) from %p", index, puEBP[1]);

  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      index, D3DTSS_ADDRESS, D3DTADDRESS_WRAP);
}

void __cdecl SetD3DTillingOff__10LH3DRenderFi(uint32_t index) {
  void** puEBP;
  __asm { mov puEBP, ebp }
  LOG_TRACE(__FUNCTION__ "(index=%u) from %p", index, puEBP[1]);

  SetTextureStageState__10LH3DRenderFUl24D3DTEXTURESTAGESTATETYPEUl(
      index, D3DTSS_ADDRESS, D3DTADDRESS_CLAMP);
}
