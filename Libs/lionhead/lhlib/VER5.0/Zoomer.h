#ifndef BLACKHACK_ZOOMER_H
#define BLACKHACK_ZOOMER_H

#include "LHPoint.h"

struct Zoomer {
  float current_value;
  float destination;
  float speed;
  float speed_m1;
  float time_m2;
  float time_m1;
  float time;
  float field_0x1c;
  float field_0x20;
  struct LHPoint field_0x24;
};
static_assert(sizeof(struct Zoomer) == 0x30, "Struct is of wrong size");

// win1.41 inlined mac 1056a120 Zoomer::Zoomer()
struct Zoomer * __fastcall ct__6ZoomerFv(struct Zoomer *this);
// win1.41 00407d60 mac 1004ee60 Zoomer::SetDestinationWithSpeedAndTime(float, float, float)
void __fastcall SetDestinationWithSpeedAndTime__6ZoomerFfff(struct Zoomer *this, const void* edx, float destination, float speed, float time);

struct Zoomer3d {
  struct Zoomer z;
  struct Zoomer x;
  struct Zoomer y;
};

#endif //BLACKHACK_ZOOMER_H
