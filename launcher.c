#include "launcher.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <detours.h>
#include <logger.h>
#include <picohash.h>

struct args_t {
  const char* dll_path;
  const char* exe_path;
  bool break_on_start;
  struct blackhack_launcher_payload_t payload;
};

bool parse_options(int argc, char* argv[], struct args_t* args) {
  bool found_exe = false;
  bool found_dll = false;
  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      if (argv[i][1] == '-') {
        if (strcmp(argv[i], "--vs-jit-attach") == 0) {
          args->payload.vs_jit_attach = true;
        }
        else if (strcmp(argv[i], "--break-on-start") == 0) {
          args->break_on_start = true;
        }
        else if (strcmp(argv[i], "--log-level") == 0 && i + 1 < argc) {
          i++;
          if (strcmp(argv[i], "TRACE") == 0) {
            args->payload.log_level = LogLevel_TRACE;
          }
          else if (strcmp(argv[i], "DEBUG") == 0) {
            args->payload.log_level = LogLevel_DEBUG;
          }
          else if (strcmp(argv[i], "INFO") == 0) {
            args->payload.log_level = LogLevel_INFO;
          }
          else if (strcmp(argv[i], "WARN") == 0) {
            args->payload.log_level = LogLevel_WARN;
          }
          else if (strcmp(argv[i], "ERROR") == 0) {
            args->payload.log_level = LogLevel_ERROR;
          }
          else if (strcmp(argv[i], "FATAL") == 0) {
            args->payload.log_level = LogLevel_FATAL;
          }
          else {
            return false;
          }
        }
        else {
          return false;
        }
      }
    }
    else if (args->dll_path == NULL) {
      args->dll_path = argv[i];
      found_dll = true;
    }
    else if (args->exe_path == NULL) {
      args->exe_path = argv[i];
      found_exe = true;
    }
    else {
      return false;
    }
  }

  // Can't break if not attached
  args->break_on_start &= args->payload.vs_jit_attach;

  return found_dll && found_exe;
}

bool check_version(const char* exe_path, uint32_t* version) {
  picohash_ctx_t ctx;
  char digest[PICOHASH_MD5_DIGEST_LENGTH];

  FILE* exe = fopen(exe_path, "rb");
  if (!exe) {
    return NULL;
  }
  fseek(exe, 0, SEEK_END);
  unsigned long len = (unsigned long)ftell(exe);
  fseek(exe, 0, SEEK_SET);
  char* exe_contents = malloc(len);
  fread_s(exe_contents, len, len, 1, exe);

  picohash_init_md5(&ctx);
  picohash_update(&ctx, exe_contents, len);
  picohash_final(&ctx, digest);

  fclose(exe);
  free(exe_contents);

  if (memcmp(digest, runblack_version_win_1_41_17_sum, sizeof(digest)) == 0) {
    *version = runblack_version_win_1_41_17;
    return true;
  }

  return false;
}

int main(int argc, char* argv[]) {
  struct args_t args = {
    .dll_path = NULL,
    .exe_path = NULL,
    .break_on_start = false,
    .payload = {
        .vs_jit_attach = false,
        .log_level = LogLevel_DEBUG,
    },
  };
  if (!parse_options(argc, argv, &args)) {
    printf("Usage: %s [--vs-jit-attach] [--break-on-start] [--log-level LEVEL] blackhack.dll "
           "runblack.exe\n",
        argv[0]);
    return EXIT_FAILURE;
  }

  if (!check_version(args.exe_path, &args.payload.version)) {
    printf("Unsupported version of runblack (md5 mismatch): %s\n", args.exe_path);
    return EXIT_FAILURE;
  }

  uint32_t create_flags = CREATE_DEFAULT_ERROR_MODE | CREATE_SUSPENDED;

  STARTUPINFOA si = { 0 };
  PROCESS_INFORMATION pi = { 0 };
  if (!DetourCreateProcessWithDllsA(args.exe_path, NULL, NULL, NULL, true,
          create_flags, NULL, NULL, &si, &pi, 1, &args.dll_path, NULL)) {
    printf("Usage: %s DetourCreateProcessWithDllsA failed: 0x%08x\n", argv[0],
        (uint32_t)GetLastError());
    return EXIT_FAILURE;
  }

  BOOL ok = DetourCopyPayloadToProcess(
      pi.hProcess, &blackhack_guid, &args.payload, sizeof(args.payload));
  assert(ok);

  // FIXME: Bit of a race condition here
  if (args.break_on_start) {
    DebugBreakProcess(pi.hProcess);
  }
  ResumeThread(pi.hThread);
  WaitForSingleObject(pi.hProcess, INFINITE);

  unsigned long result = 0;
  if (!GetExitCodeProcess(pi.hProcess, &result)) {
    printf("%s: GetExitCodeProcess failed: 0x%08x\n", argv[0],
        (uint32_t)GetLastError());
    return EXIT_FAILURE;
  }

  return (int)result;
}
