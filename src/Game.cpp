#include "Game.h"

#include <globals.h>

struct GGame* Get__5GGameFv(void) {
  return globals()->pp_game;
}

bool NeedsBiggerText__5GGameFv(struct GGame* this) {
  if (this->field_0x250080 == 6) {
    return true;
  }
  if (this->field_0x250080 == 10) {
    return true;
  }
  if (this->field_0x250080 == 11) {
    return true;
  }
  if (this->field_0x250080 == 13) {
    return true;
  }
  if (this->field_0x250080 == 14) {
    return true;
  }
  return false;
}
