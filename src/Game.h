#ifndef BLACKHACK_GAME_H
#define BLACKHACK_GAME_H

#include <assert.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

struct GGame {
  uint8_t field_0x0[0x250080];
  uint32_t field_0x250080;
};
static_assert(offsetof(struct GGame, field_0x250080) == 0x250080, "member at wrong offset");

// inlined Game::Get(void)
struct GGame* Get__5GGameFv(void);
// inlined Game::NeedsBiggerText(void)
bool NeedsBiggerText__5GGameFv(struct GGame* this);

#endif // BLACKHACK_GAME_H
